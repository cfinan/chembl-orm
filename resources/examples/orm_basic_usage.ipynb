{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4c5fcef4",
   "metadata": {},
   "source": [
    "# Using the ChEMBL ORM\n",
    "In this notebook there are some code examples illustrating how to use the object relational mapper to query the ChEMBL database. You do not have to use this, some people may choose to query the database directly. However, it is useful if you want your queries to be largly agnostic of the database backend.\n",
    "\n",
    "We will use the SQLite ChEMBL distribution that can be downloaded from [ChEMBL downloads](https://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/latest/chembl_33_sqlite.tar.gz). This is a `tar.gz`, after downloaded, do `tar -xzvf <tar file>` and in the extracted directory there should be a database file names `chembl_<version>.db`. This is the SQLite database file and contains a full copy of ChEMBL. There are other database distributions on the [download site](https://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/latest/), this code should work with those as well, the only thing you will have to change is the connection URL in the call to `create_engine`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "d94a5fae",
   "metadata": {},
   "outputs": [],
   "source": [
    "from chembl_orm import orm as o\n",
    "from sqlalchemy import create_engine, and_\n",
    "from sqlalchemy.orm import sessionmaker"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6502231a",
   "metadata": {},
   "source": [
    "## Get the database and connect\n",
    "First we will define the location of the database, so we can open it using SQLAlchemy, your location will be different."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "da772b78",
   "metadata": {},
   "outputs": [],
   "source": [
    "db_path = \"/data/chembl/chembl_33.db\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9fbcd20a",
   "metadata": {},
   "source": [
    "Now we will use SQLAlchemy to open the database, see the SQLAlchemy [docs](https://docs.sqlalchemy.org/en/13/orm/session_basics.html) for details. At present the chembl-orm package uses SQLAlchemy 1.3 but will be updated in future."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "9d7b7ecb",
   "metadata": {},
   "outputs": [],
   "source": [
    "engine = create_engine(f\"sqlite:///{db_path}\")\n",
    "\n",
    "# create a configured \"Session\" class\n",
    "Session = sessionmaker(bind=engine)\n",
    "\n",
    "# create a Session\n",
    "session = Session()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64fbbdfd",
   "metadata": {},
   "source": [
    "## Some basic queries"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "45e4c060",
   "metadata": {},
   "source": [
    "Now you are good to go. With the SQLAlchemy ORM, you can issue queries joining tables, or use it similar to an API, traversing relationships between objects.\n",
    "\n",
    "You can also perform simple count operations on query objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "c3b14f23",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2399743"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Count the number of molecules in the database, the real version has over 1M concepts\n",
    "session.query(o.MoleculeDictionary).count()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "2616aea4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "15398"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Count the number of targets\n",
    "session.query(o.TargetDictionary).count()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3175ae15",
   "metadata": {},
   "source": [
    "Have a look at some descriptions, this will return target dictionary objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "213adfff",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<TargetDictionary(tid=1, target_type=SINGLE PROTEIN, pref_name=Maltase-glucoamylase, tax_id=9606, organism=Homo sapiens, chembl_id=CHEMBL2074, species_group_flag=False)>\n",
      "<TargetDictionary(tid=2, target_type=SINGLE PROTEIN, pref_name=Sulfonylurea receptor 2, tax_id=9606, organism=Homo sapiens, chembl_id=CHEMBL1971, species_group_flag=False)>\n"
     ]
    }
   ],
   "source": [
    "# using limit to reduct the number of results to 2\n",
    "q = session.query(o.TargetDictionary).limit(2)\n",
    "\n",
    "for row in q:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11fae318",
   "metadata": {},
   "source": [
    "You do not have to return all of the columns, i.e. a whole object representing the table. You can ask for specific column names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "f1987336",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CHEMBL2074 Maltase-glucoamylase\n",
      "CHEMBL1971 Sulfonylurea receptor 2\n"
     ]
    }
   ],
   "source": [
    "for row in session.query(o.TargetDictionary.chembl_id, o.TargetDictionary.pref_name).limit(2):\n",
    "    print(row.chembl_id, row.pref_name)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de401cbf",
   "metadata": {},
   "source": [
    "You can add filters, which are like the where clause in an SQL query. Here we are limiting the targets to human targets AND protein complexes. This also demonstrates how to format the query code as the queries get longer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "f1683f6d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "('CHEMBL612409', 'Anti-estrogen binding site (AEBS)', 'PROTEIN COMPLEX', 'Homo sapiens')\n",
      "('CHEMBL1907588', 'Acetylcholine receptor; alpha1/beta1/delta/gamma', 'PROTEIN COMPLEX', 'Homo sapiens')\n"
     ]
    }
   ],
   "source": [
    "sql = session.query(\n",
    "    o.TargetDictionary.chembl_id,\n",
    "    o.TargetDictionary.pref_name,\n",
    "    o.TargetDictionary.target_type,\n",
    "    o.TargetDictionary.organism\n",
    ").filter(\n",
    "    and_(\n",
    "        o.TargetDictionary.target_type == 'PROTEIN COMPLEX',\n",
    "        o.TargetDictionary.organism == 'Homo sapiens',\n",
    "    )\n",
    ").limit(2)\n",
    "\n",
    "# We will store the ChEMBL IDs and use them below\n",
    "chembl_ids = []\n",
    "for row in sql:\n",
    "    chembl_ids.append(row.chembl_id)\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12c88e82",
   "metadata": {},
   "source": [
    "We can add one or more joins, here we are looking at the individual componenets of our targets. In this case they are individual proteins and we have got the SwissProt identifier for them. Obviously with this you need to know the database schema, to know which columns to join."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "3b9b992d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "('CHEMBL1907588', 'Acetylcholine receptor; alpha1/beta1/delta/gamma', 'PROTEIN COMPLEX', 'Homo sapiens', 'SWISS-PROT', 'P02708')\n",
      "('CHEMBL1907588', 'Acetylcholine receptor; alpha1/beta1/delta/gamma', 'PROTEIN COMPLEX', 'Homo sapiens', 'SWISS-PROT', 'P07510')\n",
      "('CHEMBL1907588', 'Acetylcholine receptor; alpha1/beta1/delta/gamma', 'PROTEIN COMPLEX', 'Homo sapiens', 'SWISS-PROT', 'P11230')\n",
      "('CHEMBL1907588', 'Acetylcholine receptor; alpha1/beta1/delta/gamma', 'PROTEIN COMPLEX', 'Homo sapiens', 'SWISS-PROT', 'Q07001')\n",
      "('CHEMBL612409', 'Anti-estrogen binding site (AEBS)', 'PROTEIN COMPLEX', 'Homo sapiens', 'SWISS-PROT', 'Q15125')\n",
      "('CHEMBL612409', 'Anti-estrogen binding site (AEBS)', 'PROTEIN COMPLEX', 'Homo sapiens', 'SWISS-PROT', 'Q9UBM7')\n"
     ]
    }
   ],
   "source": [
    "sql = session.query(\n",
    "    o.TargetDictionary.chembl_id,\n",
    "    o.TargetDictionary.pref_name,\n",
    "    o.TargetDictionary.target_type,\n",
    "    o.TargetDictionary.organism,\n",
    "    o.ComponentSequences.db_source,\n",
    "    o.ComponentSequences.accession\n",
    ").join(\n",
    "    o.TargetComponents, \n",
    "    o.TargetComponents.tid == o.TargetDictionary.tid\n",
    ").join(\n",
    "    o.ComponentSequences,\n",
    "    o.ComponentSequences.component_id == o.TargetComponents.component_id\n",
    ").filter(o.TargetDictionary.chembl_id.in_(chembl_ids))\n",
    "\n",
    "# This will be used for the demonstration query below\n",
    "for row in sql:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "95577bbc",
   "metadata": {},
   "source": [
    "We can also use the objects themselves to do a similar thing. So the ORM classes act in a similar way to a dedicated API. Here we are traversing to the relationships for the specific ChEMBL Ids that we stored earlier and we are gathering the same information. Here we do not need to know as much about the schema, as we are relying on the pre-defined relationships within the ORM."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "addf81ae",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "=== ChEMBL ID: CHEMBL1907588 ===\n",
      " * SWISS-PROT > P02708\n",
      " * SWISS-PROT > P07510\n",
      " * SWISS-PROT > P11230\n",
      " * SWISS-PROT > Q07001\n",
      "=== ChEMBL ID: CHEMBL612409 ===\n",
      " * SWISS-PROT > Q15125\n",
      " * SWISS-PROT > Q9UBM7\n"
     ]
    }
   ],
   "source": [
    "sql = session.query(\n",
    "    o.TargetDictionary\n",
    ").filter(\n",
    "    o.TargetDictionary.chembl_id.in_(chembl_ids)\n",
    ")\n",
    "\n",
    "for row in sql:\n",
    "    print(f\"=== ChEMBL ID: {row.chembl_id} ===\")\n",
    "    for comp in row.target_components:\n",
    "        print(f\" * {comp.component_sequences.db_source} > {comp.component_sequences.accession}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d988f90",
   "metadata": {},
   "source": [
    "## Summary\n",
    "So this is just to give you an idea of how to connect and issue basic queries ising the ORM classes defined in the chembl-orm package. When I get chance, I will add some more informative queries in here."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.15"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
