# Example code

This contains any example Jupyter notebooks that users can use to learn the API. Please note, the notebooks are also built into the documentation, so should not contain __any__ identifiable data. Use dummy or synthetic data if you want to illustrate examples with identifiable data.
