column	name	data_type	description
1	table_name	string	The current name of the database table, must not contain any spaces.
2	new_table_name	string	A new name for the database table, must not contain any spaces.
3	class_name	string	A class name by which the ORM class will be named, usually camel case, must not contain any spaces.
