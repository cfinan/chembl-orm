column	name	data_type	description
1	table_name	string	The current name of the database table, must not contain any spaces. If the table name has been renamed in the table-info file, then this table name must be the new name.
2	column_name	string	The name of the column.
3	dtype	string	The column data dtype. Currently supported data types are. ``int``, ``numeric``, ``big_int``, ``small_int``, ``text``, ``varchar``, ``date``, ``datetime``, float``.
4	max_len	integer	The maximum string length in a column.
5	is_primary_key	boolean	Is the column a primary key column.
6	is_index	boolean	Is the column indexed.
7	is_nullable	boolean	Can the column values be undefined.
8	is_unique	boolean	Should the column values be unique.
9	column_doc	string	A description for the column.
