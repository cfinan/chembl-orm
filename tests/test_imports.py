"""Test that all the modules can be imported.
"""
import pytest
import importlib
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "import_loc",
    (
        'chembl_orm.common',
        'chembl_orm.parse_schema',
        'chembl_orm.orm',
        'chembl_orm.queries',
        'chembl_orm.index',
    )
)
def test_package_import(import_loc):
    """Test that the modules can be imported.
    """
    importlib.import_module(import_loc, package=None)
