"""For creating and querying index tables based in the ``drug_indication``
table. These can be used for querying against the drug indications for
non-exact matches.

This indexes both the EFO terms and the MeSH terms. Each time the script is
run, it will drop and re-build the indexes.
"""
from chembl_orm import (
    __version__,
    __name__ as pkg_name,
    orm,
    common
)
from umls_tools.admin import index
from sqlalchemy_config import config as cfg
from pyaddons import log
import argparse
import os
import sys
import csv
# import pprint as pp

# deal with long fields in csv
csv.field_size_limit(sys.maxsize)

_PROG_NAME = "chembl-index"
"""The name of the program that doubles as the logger name (`str`)
"""
_DESC = __doc__
"""The program description given to argparse (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point. For API access see
    ``chembl_orm.index.build_chembl_index``
    """
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Get a sessionmaker to create sessions to interact with the database
    sm = cfg.get_new_sessionmaker(
        args.dburl,
        conn_prefix=common.DEFAULT_PREFIX,
        config_arg=args.config,
        config_env=None,
        config_default=common.DEFAULT_CONFIG,
        exists=True
    )

    load_verbose = log.progress_verbose(verbose=args.verbose)

    try:
        logger.info("building a ChEMBL index, this may take some time...")
        nterms, ntokens, mterm_len, mtoken_len = build_chembl_index(
            sm, tmpdir=args.tmp, verbose=load_verbose,
            commit_every=args.commit_every, chunksize=args.chunksize
        )
        logger.info(f"# terms: {nterms}")
        logger.info(f"# tokens: {ntokens}")
        logger.info(f"max term length: {mterm_len}")
        logger.info(f"max token_length: {mtoken_len}")
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Use argparse to parse the command line arguments

    Returns
    -------
    parser : `argparse.ArgumentParser`
        The arguments expected on the command line.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'dburl',
        nargs='?',
        type=str,
        help="An SQLAlchemy connection URL or filename if using SQLite. If you"
        " do not want to put full connection parameters on the cmd-line use "
        "the config file (--config) and config section (--config-section) to"
        " supply the parameters"
    )
    parser.add_argument(
        '-c', '--config',
        type=str,
        default="~/{0}".format(
            os.path.basename(common.DEFAULT_CONFIG)
        ),
        help="The location of the config file"
    )
    parser.add_argument(
        '-T', '--tmp',
        type=str,
        help="The location of tmp, if not provided will "
        "use the system tmp"
    )
    parser.add_argument(
        '--commit-every', type=int, default=10000,
        help="The commit to the database after every "
        "--commit-every rows"
    )
    parser.add_argument(
        '--chunksize', type=int, default=100000,
        help="The max rows to keep in memory when sorting"
    )
    parser.add_argument(
        '-v', '--verbose', default=0, action="count",
        help="give more output, ``--vv`` turns on progress monitoring"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Use argparse to parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The arguments expected on the command line.

    Returns
    -------
    args : `ArgumentParser.args`
        The arguments from parsing the cmd line args.
    """
    args = parser.parse_args()
    # Because the default is ~/ for Sphinx docs
    args.config = os.path.expanduser(args.config)
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_chembl_index(sm, tmpdir=None, verbose=False, commit_every=10000,
                       chunksize=100000, max_files=16):
    """Build an index data for all the ChEMBL EFO and MeSH indications.

    Parameters
    ----------
    sm : `sqlalchemy.Sessionmaker`
        The SQLAlchemy session maker.
    tmpdir : `str`, optional, default: `NoneType`
        The temp directory to use for the merge sort. If not provided then the
        default temp location is used.
    verbose : `bool`, optional, default: `False`
        Monitor progress.
    commit_every : `int`, optional, default: `10000`
        Commit to the database every 10000 rows.
    lang : `str`, optional, default: `ENG`
        Restrict the building of the index to a specific language.
    chunksize : `int` optional, default: `100000`
        The number of rows to hold in memory during the chunking phase of an
        external merge sort. More rows will use more memory but will be
        faster.
    max_files : `int`, optional, default: `16`
        The maximum number of tempfiles to open at a single time during the
        merge phase of an external merge sort.

    Notes
    -----
    This will create two index tables ``term_index_lookup`` and
    ``term_index_map``, these are represented by the orm classes
    ``chembl_orm.orm.TermIndexLookup`` and ``chembl_orm.orm.TermIndexMap``,
    respectively. These tables contain tokens from the drug indication terms.

    Note that the index tables are dropped and re-created upon each call of
    this function.

    See also
    --------
    chembl_orm.orm.TermIndexLookup
    chembl_orm.orm.TermIndexMap
    chembl_orm.queries.ChemblQuery.get_drugs_for_indication
    """
    session = sm()

    try:
        q1 = session.query(
            orm.DrugIndication.drugind_id, orm.DrugIndication.efo_term
        ).filter(
            orm.DrugIndication.efo_term != None
        )

        q2 = session.query(
            orm.DrugIndication.drugind_id, orm.DrugIndication.mesh_heading
        )

        q = q1.union(q2).distinct()
        try:
            return index.build_index(
                session, q, orm, tmpdir=tmpdir, verbose=verbose,
                commit_every=commit_every, chunksize=chunksize
            )
        except (TypeError, ValueError) as e:
            if hasattr(e, "error_record"):
                raise e.__class__(
                    f"error in record: {e.error_record}"
                ) from e
            else:
                raise
    finally:
        session.close()
