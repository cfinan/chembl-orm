"""A utility for parsing ChEMBL schema documentation and producing a table info
and a column info file. The schema documentation file can be downloaded from
the ChEMBL `downloads page <https://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/latest/schema_documentation.txt>`_
"""
# Importing the version number (in __init__.py) and the package name
from chembl_orm import (
    __version__,
    __name__ as pkg_name
)
from pyaddons import (
    log,
)
import argparse
import sys
import os
import re
import csv
# import pprint as pp


_SCRIPT_NAME = "chembl-parse-schema"
"""The name of the script (`str`)
"""

# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""

_TABLE_INFO_HEADER = [
    'table_name', 'table_doc'
]
_COLUMN_INFO_HEADER = [
    'table_name', 'column_name', 'dtype', 'max_len', 'is_primary_key',
    'is_index', 'is_nullable', 'is_unique', 'column_doc'
]
_DELIMITER = "\t"

csv.field_size_limit(sys.maxsize)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    try:
        version, table_info_path, column_info_path = parse_chembl_schema(
            args.schema_doc, table_info_path=args.out_table_info,
            column_info_path=args.out_column_info
        )
        logger.info(f"ChEMBL version {version}")
        logger.info(f"table info written to: {table_info_path}")
        logger.info(f"column info written to: {column_info_path}")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'schema_doc', type=str,
        help="The schema documentation file."
    )
    parser.add_argument(
        '-t', '--out-table-info', type=str,
        help="The path to the table info output file. If not supplied then a "
        "default file name of ``<ChEMBL_version>_table_info.txt`` will be used"
        " and written to the current directory."
    )
    parser.add_argument(
        '-c', '--out-column-info', type=str,
        help="The path to the column info output file. If not supplied then a "
        "default file name of ``<ChEMBL_version>_column_info.txt`` will be"
        " used and written to the current directory."
    )
    parser.add_argument(
        '-v', '--verbose',  action="count", default=0,
        help="give more output, -vv will turn on progress monitoring"
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_chembl_schema(schema_file, table_info_path=None,
                        column_info_path=None):
    """Parse the ChEMBL schema file.

    Parameters
    ----------
    schema_file : `str`
        The path to the ChEMBL schema file.
    table_info_path : `str`, optional, default: `NoneType`
        The path to the table info output file. If not supplied then
        ``<ChEMBL_version>_table_info.txt`` will be used and written to the
        current directory.
    column_info_path : `str`, optional, default: `NoneType`
        The path to the column info output file. If not supplied then
        ``<ChEMBL_version>_column_info.txt`` will be used and written to the
        current directory.
    """
    with open(schema_file, 'rt') as infile:
        version = _read_version(infile)

        if table_info_path is None:
            table_info_path = os.path.join(
                os.getcwd(), f"chembl_{version}_table_info.txt"
            )
        if column_info_path is None:
            column_info_path = os.path.join(
                os.getcwd(), f"chembl_{version}_column_info.txt"
            )

        with open(table_info_path, 'wt') as tabinfo:
            tab_writer = csv.writer(
                tabinfo, delimiter=_DELIMITER, lineterminator=os.linesep
            )
            tab_writer.writerow(_TABLE_INFO_HEADER)
            with open(column_info_path, 'wt') as colinfo:
                col_writer = csv.DictWriter(
                    colinfo, _COLUMN_INFO_HEADER,
                    delimiter=_DELIMITER, lineterminator=os.linesep,
                    extrasaction='ignore'
                )
                col_writer.writeheader()

                while True:
                    try:
                        table_name, table_desc, columns = _read_table(infile)
                        if not table_desc.endswith('.'):
                            table_desc += '.'
                        tab_writer.writerow([table_name, table_desc])
                        for i in columns:
                            i['table_name'] = table_name
                            if not i['column_doc'].endswith('.'):
                                i['column_doc'] += '.'
                            col_writer.writerow(i)
                        # print(table_name)
                        # print(table_desc)
                        # pp.pprint(columns)
                    except StopIteration:
                        break

    return version, table_info_path, column_info_path


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _read_version(infile):
    """Read the version from the schema documentation file. The version should
    be the first line of the file.

    Parameters
    ----------
    infile : `File`
        The file object to work with.

    Returns
    -------
    version : `int`
        The ChEMBL version number.

    Raises
    ------
    ValueError
        If the version can't be detected.
    """
    version_line = _read_to_next_line(infile)

    version_match = re.match(
            r'^chembl_(\d+)\s+schema\s+documentation',
            version_line.strip(),
            re.IGNORECASE
    )

    if version_match:
        return int(version_match.group(1))
    else:
        raise ValueError("can't find version line")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _read_table(infile):
    """Read a table entry in the schema documentation.

    Parameters
    ----------
    infile : `File`
        The input file object.
    """
    line = _read_to_next_line(infile)

    table_name_match = re.match(r'^([A-Z_]+):$', line.strip())
    if not table_name_match:
        raise ValueError("can't find table name")

    table_name = table_name_match.group(1).lower()

    table_desc = ""
    line = _read_to_next_line(infile)
    while line != "":
        table_desc += line
        line = infile.readline().strip()

    columns = _read_column_table(infile)
    return table_name, table_desc, columns


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _read_column_table(infile):
    """Read the column table from a table entry. This assumes that the table
    header is the next data line to be read in.

    Parameters
    ----------
    infile : `File`
        The input file object located just before the table header.

    Returns
    -------
    all_columns : `list` of `dict`
        All the columns in the table defintion.
    """
    table_header = [
        ('KEYS', _set_column_keys),
        ('COLUMN_NAME', _set_column_name),
        ('DATA_TYPE', _set_data_type),
        ('NULLABLE', _set_nullable),
        ('COMMENT', _set_comment)
    ]
    header = _read_to_next_line(infile)
    # header = re.sub(r'\s{3,}', '\t', header.rstrip('\n')).split('\t')
    header = _split_line(header, len(table_header))

    if header != [i for i, j in table_header]:
        raise ValueError(f"wrong table header: {','.join(header)}")

    all_columns = []
    line = infile.readline()
    while line.strip() != "":
        columns = dict()
        fields = _split_line(line, len(table_header))
        try:
            idx = 0
            for col, ifunc in table_header:
                columns = {**columns, **ifunc(fields[idx].strip())}
                idx += 1
            all_columns.append(columns)
            line = infile.readline()
        except (ValueError, IndexError) as e:
            raise ValueError(f"problem with line: {line}") from e
    return all_columns


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _split_line(line, nfields, fixed_width=20):
    """Split a fixed field width delimited line into a set number of fields.

    This is a fiddly format to handle as if the text is too long then the line
    shifts, so it is fixed width bu not allways. Also, missing data is just
    empty slots.

    Parameters
    ----------
    line : `str`
        The line to split.
    nfields : `int`
        The number of fields to split the line into.
    fixed_width : `int`, optional, default: `20`
        The max width of the separate between fields.

    Returns
    -------
    fields : `list` of `str`
        The extracted fields, list shuld have the length of ``nfields``.
    """
    fields = []
    it = 0
    idx = 0
    stop_fields = nfields - 1

    # The text of the current field
    field = ""
    while it < nfields:
        # Add fixed widths worth of characters to the string, these may be
        # actual text or spaces of the gap between the fields
        field += line[idx:idx+fixed_width]

        # If we have not yet reached the last field
        if it < stop_fields and re.search(r'^[^\s]$', field[-1:]):
            pass
        elif it == stop_fields and \
             re.search(r'[^\s]', line[idx+fixed_width:idx+fixed_width+2]) and \
             re.search(r'[^\s]', field[-2:]):
            pass
        else:
            it += 1
            fields.append(field.strip())
            field = ""
        idx += fixed_width
    return fields


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_column_keys(key_data):
    """Set the key columns.

    Parameters
    ----------
    key_data : `str`
        The comma delimited key data.

    Returns
    -------
    key_cols : `dict`
        The keys for the column.
    """
    key_cols = dict(index=0, primary_key=0, unique=0, foreign_key=0)

    key_data = [i.strip() for i in key_data.split(',')]

    if len(key_data) == 0:
        return key_cols

    if 'FK' in key_data:
        key_cols['is_foreign_key'] = 1

    if 'PK' in key_data:
        key_cols['is_primary_key'] = 1

    if 'UK' in key_data:
        key_cols['is_unique'] = 1
        key_cols['is_index'] = 1

    return key_cols


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_column_name(field_data):
    """Set the is column name field for the column.

    Parameters
    ----------
    field_data : `str`
        The data containing the is column name field.

    Returns
    -------
    column_name : `dict`
        A dict with the ``column_name`` key set.

    Raises
    ------
    ValueError
        If the column has no name.
    """
    if field_data == "":
        raise ValueError("column has no name")
    return dict(column_name=field_data.strip().lower())


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_data_type(field_data):
    """Set the is data type and max length fields for the column.

    Parameters
    ----------
    field_data : `str`
        The data containing the is data type field.

    Returns
    -------
    data_dtype : `dict`
        A dict with the ``dtype`` and ``max_len`` keys set.

    Raises
    ------
    ValueError
        If the column has no data type, or it can't be identified/is unknown.
    """
    field_data = field_data.strip()
    if field_data == "":
        raise ValueError("column has no dtype")
    dtype = None

    dtype_match = re.match(r'^([A-Z0-9_]+)(?:\((\d+)\))?$', field_data)

    try:
        sql_dtype = dtype_match.group(1)
    except AttributeError as e:
        raise ValueError(f"Can't find datatype: {field_data}") from e
    try:
        max_len = int(dtype_match.group(2))
    except (IndexError, TypeError):
        max_len = 0

    if sql_dtype in ['VARCHAR2', 'VARCHAR']:
        dtype = 'varchar'
    elif sql_dtype == 'NUMBER':
        dtype = 'numeric'
    elif sql_dtype == 'CLOB':
        dtype = 'text'
    elif sql_dtype == 'INTEGER':
        dtype = 'int'
    elif sql_dtype == 'DATE':
        dtype = 'date'
    else:
        raise ValueError(f"unknown data type: {sql_dtype} ({field_data})")

    return dict(dtype=dtype, max_len=max_len)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_nullable(field_data):
    """Set the is nullable field for the column.

    Parameters
    ----------
    field_data : `str`
        The data containing the is nullable field.

    Returns
    -------
    is_nullable : `dict`
        A dict with the ``is_nullable`` key set to 0/1 for null-ability.
    """
    nullable = 1
    field_data = field_data.strip()
    if field_data == "NOT NULL":
        nullable = 0
    return dict(is_nullable=nullable)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_comment(field_data):
    """Set the comment field for the column.

    Parameters
    ----------
    field_data : `str`
        The data containing the comment/description field.

    Returns
    -------
    description : `dict`
        A dict with the ``doc`` key set to the description text.
    """
    return dict(column_doc=field_data.strip())


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _read_to_next_line(infile):
    """Read through blank lines until the next defined line.

    Parameters
    ----------
    infile : `File`
        The file object for the input schema file.

    Returns
    -------
    line : `str`
        A non blank line.
    """
    line = infile.readline().strip()

    while line == "":
        # line = infile.readline().strip()
        line = next(infile).strip()
    return line


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
