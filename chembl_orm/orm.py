"""SQLAlchemy ORM module.
"""
try:
    # SQLAlchemy >= 1.4
    from sqlalchemy.orm import declarative_base
except ImportError:
    # SQLAlchemy <= 1.3
    from sqlalchemy.ext.declarative import declarative_base

from umls_tools import orm_mixin
from sqlalchemy import (
    Boolean,
    Column,
    Date,
    Float,
    ForeignKey,
    Integer,
    Sequence,
    String,
    Text,
    DateTime,
    SmallInteger
)
from sqlalchemy.orm import relationship as rel

Base = declarative_base()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def print_orm_obj(obj, exclude=[]):
    """Prints the contents of the SQL Alchemy ORM object.

    Parameters
    ----------
    obj : `sqlalchemy.ext.declarative.Base`
        The SQLAlchemy declarative base model object. This should have a
        __table__ attribute that contains the corresponding SQLAlchemy table
        object.
    exclude : `NoneType` or `list` or `str`, optional, default `NoneType`
        A list of attributes (these correspond to table columns) that you do
        not want to include in the string.

    Returns
    -------
    formatted_string : `str`
        A formatted string that can be printed. The string contains the
        attributes of the object and the data they contain.

    Notes
    -----
    This is designed to be called from an SQLAlchemy ORM model object and it
    in the __repr__ method. It simply frovides a formatted string that has the
    data in the object.
    """
    exclude = exclude or []

    attr_values = []
    for i in obj.__table__.columns:
        if i.name in exclude:
            continue
        val = getattr(obj, i.name)
        if val.__class__.__name__ != "method":
            attr_values.append("{0}={1}".format(i.name, val))

    # Turn outlist into a string and return
    return "<{0}({1})>".format(obj.__class__.__name__, ", ".join(attr_values))


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ActionType(Base):
    """A representation of the ``action_type`` table.

    Parameters
    ----------
    action_type : `str`
        Primary key. Type of action of the drug e.g., agonist, antagonist.
    description : `str`
        Description of how the action type is used.
    parent_type : `str`, optional, default: `NoneType`
        Higher-level grouping of action types e.g., positive vs negative
        action.
    activities : `chembl_orm.orm.Activities`, optional, default: `NoneType`
        Relationship with ``activities``.
    drug_mechanism : `chembl_orm.orm.DrugMechanism`, optional, default: \
    `NoneType`
        Relationship with ``drug_mechanism``.

    Notes
    -----
    Table storing the distinct list of action types used in the drug_mechanism
    table, together with a higher-level parent action type.
    """
    __tablename__ = "action_type"

    action_type = Column(
        String(50), Sequence("action_type_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key. Type of action of the drug e.g., agonist, "
        "antagonist."
    )
    description = Column(
        String(200), index=False, unique=False, nullable=False,
        doc="Description of how the action type is used."
    )
    parent_type = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Higher-level grouping of action types e.g., positive vs negative"
        " action."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activities = rel(
        "Activities", back_populates="action_type_rel",
        doc="Relationship back to ``activities``."
    )
    drug_mechanism = rel(
        "DrugMechanism", back_populates="action_type_rel",
        doc="Relationship back to ``drug_mechanism``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Activities(Base):
    """A representation of the ``activities`` table.

    Parameters
    ----------
    activity_id : `int`
        Unique ID for the activity row.
    assay_id : `int`
        Foreign key to the assays table (containing the assay description).
        Foreign key to ``assays.assay_id``.
    doc_id : `int`, optional, default: `NoneType`
        Foreign key to documents table (for quick lookup of publication details
        - can also link to documents through compound_records or assays table).
        Foreign key to ``docs.doc_id``.
    record_id : `int`
        Foreign key to the compound_records table (containing information on
        the compound tested). Foreign key to ``compound_records.record_id``.
    molregno : `int`, optional, default: `NoneType`
        Foreign key to compounds table (for quick lookup of compound structure
        - can also link to compounds through compound_records table). Foreign
        key to ``molecule_dictionary.molregno``.
    standard_relation : `str`, optional, default: `NoneType`
        Symbol constraining the activity value (e.g. >, <, =).
    standard_value : `float`, optional, default: `NoneType`
        Same as PUBLISHED_VALUE but transformed to common units: e.g. mM
        concentrations converted to nM.
    standard_units : `str`, optional, default: `NoneType`
        Selected 'Standard' units for data type: e.g. concentrations are in nM.
    standard_flag : `bool`, optional, default: `NoneType`
        Shows whether the standardised columns have been curated/set (1) or
        just default to the published data (0).
    standard_type : `str`, optional, default: `NoneType`
        Standardised version of the published_activity_type (e.g. IC50 rather
        than Ic-50/Ic50/ic50/ic-50).
    activity_comment : `str`, optional, default: `NoneType`
        Previously used to report non-numeric activities i.e. 'Slighty active',
        'Not determined'. STANDARD_TEXT_VALUE will be used for this in future,
        and this will be just for additional comments.
    data_validity_comment : `str`, optional, default: `NoneType`
        Comment reflecting whether the values for this activity measurement are
        likely to be correct - one of 'Manually validated' (checked original
        paper and value is correct), 'Potential author error' (value looks
        incorrect but is as reported in the original paper), 'Outside typical
        range' (value seems too high/low to be correct e.g., negative IC50
        value), 'Non standard unit type' (units look incorrect for this
        activity type). Foreign key to
        ``data_validity_lookup.data_validity_comment``.
    potential_duplicate : `bool`, optional, default: `NoneType`
        When set to 1, indicates that the value is likely to be a repeat
        citation of a value reported in a previous ChEMBL paper, rather than a
        new, independent measurement. Note: value of zero does not guarantee
        that the measurement is novel/independent though.
    pchembl_value : `float`, optional, default: `NoneType`
        Negative log of selected concentration-response activity values
        (IC50/EC50/XC50/AC50/Ki/Kd/Potency).
    bao_endpoint : `str`, optional, default: `NoneType`
        ID for the corresponding result type in BioAssay Ontology (based on
        standard_type). Foreign key to ``bioassay_ontology.bao_id``.
    uo_units : `str`, optional, default: `NoneType`
        ID for the corresponding unit in Unit Ontology (based on
        standard_units).
    qudt_units : `str`, optional, default: `NoneType`
        ID for the corresponding unit in QUDT Ontology (based on
        standard_units).
    toid : `int`, optional, default: `NoneType`
        The Test Occasion Identifier, used to group together related activity
        measurements.
    upper_value : `float`, optional, default: `NoneType`
        Where the activity is a range, this represents the highest value of the
        range (numerically), while the PUBLISHED_VALUE column represents the
        lower value.
    standard_upper_value : `float`, optional, default: `NoneType`
        Where the activity is a range, this represents the standardised version
        of the highest value of the range (with the lower value represented by
        STANDARD_VALUE).
    src_id : `int`, optional, default: `NoneType`
        Foreign key to source table, indicating the source of the activity
        value. Foreign key to ``source.src_id``.
    type : `str`
        Type of end-point measurement: e.g. IC50, LD50, %inhibition etc, as it
        appears in the original dataset.
    relation : `str`, optional, default: `NoneType`
        Symbol constraining the activity value (e.g. >, <, =), as it appears in
        the original dataset.
    value : `float`, optional, default: `NoneType`
        Datapoint value as it appears in the original dataset.
    units : `str`, optional, default: `NoneType`
        Units of measurement as they appear in the original dataset.
    text_value : `str`, optional, default: `NoneType`
        Additional information about the measurement.
    standard_text_value : `str`, optional, default: `NoneType`
        Standardized version of additional information about the measurement.
    action_type : `str`, optional, default: `NoneType`
        Foreign key to action_type table; specifies the effect of the compound
        on its target. Foreign key to ``action_type.action_type``.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.
    docs : `chembl_orm.orm.Docs`, optional, default: `NoneType`
        Relationship with ``docs``.
    compound_records : `chembl_orm.orm.CompoundRecords`, optional, default: \
    `NoneType`
        Relationship with ``compound_records``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.
    data_validity_lookup : `chembl_orm.orm.DataValidityLookup`, optional, \
    default: `NoneType`
        Relationship with ``data_validity_lookup``.
    bioassay_ontology : `chembl_orm.orm.BioassayOntology`, optional, default: \
    `NoneType`
        Relationship with ``bioassay_ontology``.
    source : `chembl_orm.orm.Source`, optional, default: `NoneType`
        Relationship with ``source``.
    action_type_rel : `chembl_orm.orm.ActionType`, optional, default: \
    `NoneType`
        Relationship with ``action_type``.
    activity_properties : `chembl_orm.orm.ActivityProperties`, optional, \
    default: `NoneType`
        Relationship with ``activity_properties``.
    activity_supp_map : `chembl_orm.orm.ActivitySuppMap`, optional, default: \
    `NoneType`
        Relationship with ``activity_supp_map``.
    ligand_eff : `chembl_orm.orm.LigandEff`, optional, default: `NoneType`
        Relationship with ``ligand_eff``.
    predicted_binding_domains : `chembl_orm.orm.PredictedBindingDomains`, \
    optional, default: `NoneType`
        Relationship with ``predicted_binding_domains``.

    Notes
    -----
    Activity 'values' or 'end points'  that are the results of an assay
    recorded in a scientific document. Each activity is described by a row.
    """
    __tablename__ = "activities"

    activity_id = Column(
        Integer, Sequence("activity_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Unique ID for the activity row."
    )
    assay_id = Column(
        Integer, ForeignKey("assays.assay_id"), index=True, unique=False,
        nullable=False,
        doc="Foreign key to the assays table (containing the assay "
        "description)."
    )
    doc_id = Column(
        Integer, ForeignKey("docs.doc_id"), index=True, unique=False,
        nullable=True,
        doc="Foreign key to documents table (for quick lookup of publication "
        "details - can also link to documents through compound_records or "
        "assays table)."
    )
    record_id = Column(
        Integer, ForeignKey("compound_records.record_id"), index=True,
        unique=False, nullable=False,
        doc="Foreign key to the compound_records table (containing "
        "information on the compound tested)."
    )
    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=False, nullable=True,
        doc="Foreign key to compounds table (for quick lookup of compound "
        "structure - can also link to compounds through compound_records "
        "table)."
    )
    standard_relation = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Symbol constraining the activity value (e.g. >, <, =)."
    )
    standard_value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Same as PUBLISHED_VALUE but transformed to common units: e.g. mM"
        " concentrations converted to nM."
    )
    standard_units = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Selected 'Standard' units for data type: e.g. concentrations are"
        " in nM."
    )
    standard_flag = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Shows whether the standardised columns have been curated/set (1)"
        " or just default to the published data (0)."
    )
    standard_type = Column(
        String(250), index=False, unique=False, nullable=True,
        doc="Standardised version of the published_activity_type (e.g. IC50 "
        "rather than Ic-50/Ic50/ic50/ic-50)."
    )
    activity_comment = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="Previously used to report non-numeric activities i.e. 'Slighty "
        "active', 'Not determined'. STANDARD_TEXT_VALUE will be used for this "
        "in future, and this will be just for additional comments."
    )
    data_validity_comment = Column(
        String(30), ForeignKey("data_validity_lookup.data_validity_comment"),
        index=True, unique=False, nullable=True,
        doc="Comment reflecting whether the values for this activity "
        "measurement are likely to be correct - one of 'Manually validated' "
        "(checked original paper and value is correct), 'Potential author "
        "error' (value looks incorrect but is as reported in the original "
        "paper), 'Outside typical range' (value seems too high/low to be "
        "correct e.g., negative IC50 value), 'Non standard unit type' (units "
        "look incorrect for this activity type)."
    )
    potential_duplicate = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="When set to 1, indicates that the value is likely to be a repeat"
        " citation of a value reported in a previous ChEMBL paper, rather than"
        " a new, independent measurement. Note: value of zero does not "
        "guarantee that the measurement is novel/independent though."
    )
    pchembl_value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Negative log of selected concentration-response activity values "
        "(IC50/EC50/XC50/AC50/Ki/Kd/Potency)."
    )
    bao_endpoint = Column(
        String(11), ForeignKey("bioassay_ontology.bao_id"), index=True,
        unique=False, nullable=True,
        doc="ID for the corresponding result type in BioAssay Ontology (based"
        " on standard_type)."
    )
    uo_units = Column(
        String(10), index=False, unique=False, nullable=True,
        doc="ID for the corresponding unit in Unit Ontology (based on "
        "standard_units)."
    )
    qudt_units = Column(
        String(70), index=False, unique=False, nullable=True,
        doc="ID for the corresponding unit in QUDT Ontology (based on "
        "standard_units)."
    )
    toid = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="The Test Occasion Identifier, used to group together related "
        "activity measurements."
    )
    upper_value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Where the activity is a range, this represents the highest value"
        " of the range (numerically), while the PUBLISHED_VALUE column "
        "represents the lower value."
    )
    standard_upper_value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Where the activity is a range, this represents the standardised "
        "version of the highest value of the range (with the lower value "
        "represented by STANDARD_VALUE)."
    )
    src_id = Column(
        Integer, ForeignKey("source.src_id"), index=True, unique=False,
        nullable=True,
        doc="Foreign key to source table, indicating the source of the "
        "activity value."
    )
    type = Column(
        String(250), index=False, unique=False, nullable=False,
        doc="Type of end-point measurement: e.g. IC50, LD50, %inhibition etc,"
        " as it appears in the original dataset."
    )
    relation = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Symbol constraining the activity value (e.g. >, <, =), as it "
        "appears in the original dataset."
    )
    value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Datapoint value as it appears in the original dataset."
    )
    units = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Units of measurement as they appear in the original dataset."
    )
    text_value = Column(
        String(1000), index=False, unique=False, nullable=True,
        doc="Additional information about the measurement."
    )
    standard_text_value = Column(
        String(1000), index=False, unique=False, nullable=True,
        doc="Standardized version of additional information about the "
        "measurement."
    )
    action_type = Column(
        String(50), ForeignKey("action_type.action_type"), index=True,
        unique=False, nullable=True,
        doc="Foreign key to action_type table; specifies the effect of the "
        "compound on its target."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assays = rel(
        "Assays", back_populates="activities",
        doc="Relationship back to ``assays``"
    )
    docs = rel(
        "Docs", back_populates="activities",
        doc="Relationship back to ``docs``"
    )
    compound_records = rel(
        "CompoundRecords", back_populates="activities",
        doc="Relationship back to ``compound_records``"
    )
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="activities",
        doc="Relationship back to ``molecule_dictionary``"
    )
    data_validity_lookup = rel(
        "DataValidityLookup", back_populates="activities",
        doc="Relationship back to ``data_validity_lookup``"
    )
    bioassay_ontology = rel(
        "BioassayOntology", back_populates="activities",
        doc="Relationship back to ``bioassay_ontology``"
    )
    source = rel(
        "Source", back_populates="activities",
        doc="Relationship back to ``source``"
    )
    action_type_rel = rel(
        "ActionType", back_populates="activities",
        doc="Relationship back to ``action_type``"
    )
    activity_properties = rel(
        "ActivityProperties", back_populates="activities",
        doc="Relationship back to ``activity_properties``."
    )
    activity_supp_map = rel(
        "ActivitySuppMap", back_populates="activities",
        doc="Relationship back to ``activity_supp_map``."
    )
    ligand_eff = rel(
        "LigandEff", back_populates="activities",
        doc="Relationship back to ``ligand_eff``."
    )
    predicted_binding_domains = rel(
        "PredictedBindingDomains", back_populates="activities",
        doc="Relationship back to ``predicted_binding_domains``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Assays(Base):
    """A representation of the ``assays`` table.

    Parameters
    ----------
    assay_id : `int`
        Unique ID for the assay.
    doc_id : `int`
        Foreign key to documents table. Foreign key to ``docs.doc_id``.
    description : `str`, optional, default: `NoneType`
        Description of the reported assay.
    assay_type : `str`, optional, default: `NoneType`
        Assay classification, e.g. B=Binding assay, A=ADME assay, F=Functional
        assay. Foreign key to ``assay_type.assay_type``.
    assay_test_type : `str`, optional, default: `NoneType`
        Type of assay system (i.e., in vivo or in vitro).
    assay_category : `str`, optional, default: `NoneType`
        screening, confirmatory (ie: dose-response), summary, panel or other.
    assay_organism : `str`, optional, default: `NoneType`
        Name of the organism for the assay system (e.g., the organism, tissue
        or cell line in which an assay was performed). May differ from the
        target organism (e.g., for a human protein expressed in non-human
        cells, or pathogen-infected human cells).
    assay_tax_id : `int`, optional, default: `NoneType`
        NCBI tax ID for the assay organism.
    assay_strain : `str`, optional, default: `NoneType`
        Name of specific strain of the assay organism used (where known).
    assay_tissue : `str`, optional, default: `NoneType`
        Name of tissue used in the assay system (e.g., for tissue-based assays)
        or from which the assay system was derived (e.g., for cell/subcellular
        fraction-based assays).
    assay_cell_type : `str`, optional, default: `NoneType`
        Name of cell type or cell line used in the assay system (e.g., for
        cell-based assays).
    assay_subcellular_fraction : `str`, optional, default: `NoneType`
        Name of subcellular fraction used in the assay system (e.g.,
        microsomes, mitochondria).
    tid : `int`, optional, default: `NoneType`
        Target identifier to which this assay has been mapped. Foreign key to
        target_dictionary. From ChEMBL_15 onwards, an assay will have only a
        single target assigned. Foreign key to ``target_dictionary.tid``.
    relationship_type : `str`, optional, default: `NoneType`
        Flag indicating of the relationship between the reported target in the
        source document and the assigned target from TARGET_DICTIONARY. Foreign
        key to RELATIONSHIP_TYPE table. Foreign key to
        ``relationship_type.relationship_type``.
    confidence_score : `int`, optional, default: `NoneType`
        Confidence score, indicating how accurately the assigned target(s)
        represents the actually assay target. Foreign key to CONFIDENCE_SCORE
        table. 0 means uncurated/unassigned, 1 = low confidence to 9 = high
        confidence. Foreign key to
        ``confidence_score_lookup.confidence_score``.
    curated_by : `str`, optional, default: `NoneType`
        Indicates the level of curation of the target assignment. Foreign key
        to curation_lookup table. Foreign key to
        ``curation_lookup.curated_by``.
    src_id : `int`
        Foreign key to source table. Foreign key to ``source.src_id``.
    src_assay_id : `str`, optional, default: `NoneType`
        Identifier for the assay in the source database/deposition (e.g.,
        pubchem AID).
    chembl_id : `str`
        ChEMBL identifier for this assay (for use on web interface etc). This
        is indexed. This is unique. Foreign key to
        ``chembl_id_lookup.chembl_id``.
    cell_id : `int`, optional, default: `NoneType`
        Foreign key to cell dictionary. The cell type or cell line used in the
        assay. Foreign key to ``cell_dictionary.cell_id``.
    bao_format : `str`, optional, default: `NoneType`
        ID for the corresponding format type in BioAssay Ontology (e.g., cell-
        based, biochemical, organism-based etc). Foreign key to
        ``bioassay_ontology.bao_id``.
    tissue_id : `int`, optional, default: `NoneType`
        ID for the corresponding tissue/anatomy in Uberon. Foreign key to
        tissue_dictionary. Foreign key to ``tissue_dictionary.tissue_id``.
    variant_id : `int`, optional, default: `NoneType`
        Foreign key to variant_sequences table. Indicates the mutant/variant
        version of the target used in the assay (where known/applicable).
        Foreign key to ``variant_sequences.variant_id``.
    aidx : `str`
        The Depositor Defined Assay Identifier.
    activities : `chembl_orm.orm.Activities`, optional, default: `NoneType`
        Relationship with ``activities``.
    docs : `chembl_orm.orm.Docs`, optional, default: `NoneType`
        Relationship with ``docs``.
    assay_type_rel : `chembl_orm.orm.AssayType`, optional, default: \
    `NoneType`
        Relationship with ``assay_type``.
    target_dictionary : `chembl_orm.orm.TargetDictionary`, optional, default: \
    `NoneType`
        Relationship with ``target_dictionary``.
    relationship_type_rel : `chembl_orm.orm.RelationshipType`, optional, \
    default: `NoneType`
        Relationship with ``relationship_type``.
    confidence_score_lookup : `chembl_orm.orm.ConfidenceScoreLookup`, \
    optional, default: `NoneType`
        Relationship with ``confidence_score_lookup``.
    curation_lookup : `chembl_orm.orm.CurationLookup`, optional, default: \
    `NoneType`
        Relationship with ``curation_lookup``.
    source : `chembl_orm.orm.Source`, optional, default: `NoneType`
        Relationship with ``source``.
    chembl_id_lookup : `chembl_orm.orm.ChemblIdLookup`, optional, default: \
    `NoneType`
        Relationship with ``chembl_id_lookup``.
    cell_dictionary : `chembl_orm.orm.CellDictionary`, optional, default: \
    `NoneType`
        Relationship with ``cell_dictionary``.
    bioassay_ontology : `chembl_orm.orm.BioassayOntology`, optional, default: \
    `NoneType`
        Relationship with ``bioassay_ontology``.
    tissue_dictionary : `chembl_orm.orm.TissueDictionary`, optional, default: \
    `NoneType`
        Relationship with ``tissue_dictionary``.
    variant_sequences : `chembl_orm.orm.VariantSequences`, optional, default: \
    `NoneType`
        Relationship with ``variant_sequences``.
    assay_class_map : `chembl_orm.orm.AssayClassMap`, optional, default: \
    `NoneType`
        Relationship with ``assay_class_map``.
    assay_parameters : `chembl_orm.orm.AssayParameters`, optional, default: \
    `NoneType`
        Relationship with ``assay_parameters``.

    Notes
    -----
    Table storing a list of the assays that are reported in each document.
    Similar assays from different publications will appear as distinct assays
    in this table.
    """
    __tablename__ = "assays"

    assay_id = Column(
        Integer, Sequence("assay_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Unique ID for the assay."
    )
    doc_id = Column(
        Integer, ForeignKey("docs.doc_id"), index=True, unique=False,
        nullable=False, doc="Foreign key to documents table."
    )
    description = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="Description of the reported assay."
    )
    assay_type = Column(
        String(1), ForeignKey("assay_type.assay_type"), index=True,
        unique=False, nullable=True,
        doc="Assay classification, e.g. B=Binding assay, A=ADME assay, "
        "F=Functional assay."
    )
    assay_test_type = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="Type of assay system (i.e., in vivo or in vitro)."
    )
    assay_category = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="screening, confirmatory (ie: dose-response), summary, panel or "
        "other."
    )
    assay_organism = Column(
        String(250), index=False, unique=False, nullable=True,
        doc="Name of the organism for the assay system (e.g., the organism, "
        "tissue or cell line in which an assay was performed). May differ from"
        " the target organism (e.g., for a human protein expressed in non-"
        "human cells, or pathogen-infected human cells)."
    )
    assay_tax_id = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="NCBI tax ID for the assay organism."
    )
    assay_strain = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Name of specific strain of the assay organism used (where known)."
    )
    assay_tissue = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Name of tissue used in the assay system (e.g., for tissue-based "
        "assays) or from which the assay system was derived (e.g., for "
        "cell/subcellular fraction-based assays)."
    )
    assay_cell_type = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Name of cell type or cell line used in the assay system (e.g., "
        "for cell-based assays)."
    )
    assay_subcellular_fraction = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Name of subcellular fraction used in the assay system (e.g., "
        "microsomes, mitochondria)."
    )
    tid = Column(
        Integer, ForeignKey("target_dictionary.tid"), index=True,
        unique=False, nullable=True,
        doc="Target identifier to which this assay has been mapped. Foreign "
        "key to target_dictionary. From ChEMBL_15 onwards, an assay will have "
        "only a single target assigned."
    )
    relationship_type = Column(
        String(1), ForeignKey("relationship_type.relationship_type"),
        index=True, unique=False, nullable=True,
        doc="Flag indicating of the relationship between the reported target "
        "in the source document and the assigned target from "
        "TARGET_DICTIONARY. Foreign key to RELATIONSHIP_TYPE table."
    )
    confidence_score = Column(
        Integer, ForeignKey("confidence_score_lookup.confidence_score"),
        index=True, unique=False, nullable=True,
        doc="Confidence score, indicating how accurately the assigned "
        "target(s) represents the actually assay target. Foreign key to "
        "CONFIDENCE_SCORE table. 0 means uncurated/unassigned, 1 = low "
        "confidence to 9 = high confidence."
    )
    curated_by = Column(
        String(32), ForeignKey("curation_lookup.curated_by"), index=True,
        unique=False, nullable=True,
        doc="Indicates the level of curation of the target assignment. "
        "Foreign key to curation_lookup table."
    )
    src_id = Column(
        Integer, ForeignKey("source.src_id"), index=True, unique=False,
        nullable=False, doc="Foreign key to source table."
    )
    src_assay_id = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Identifier for the assay in the source database/deposition "
        "(e.g., pubchem AID)."
    )
    chembl_id = Column(
        String(20), ForeignKey("chembl_id_lookup.chembl_id"), index=True,
        unique=True, nullable=False,
        doc="ChEMBL identifier for this assay (for use on web interface etc)."
    )
    cell_id = Column(
        Integer, ForeignKey("cell_dictionary.cell_id"), index=True,
        unique=False, nullable=True,
        doc="Foreign key to cell dictionary. The cell type or cell line used "
        "in the assay."
    )
    bao_format = Column(
        String(11), ForeignKey("bioassay_ontology.bao_id"), index=True,
        unique=False, nullable=True,
        doc="ID for the corresponding format type in BioAssay Ontology (e.g.,"
        " cell-based, biochemical, organism-based etc)."
    )
    tissue_id = Column(
        Integer, ForeignKey("tissue_dictionary.tissue_id"), index=True,
        unique=False, nullable=True,
        doc="ID for the corresponding tissue/anatomy in Uberon. Foreign key "
        "to tissue_dictionary."
    )
    variant_id = Column(
        Integer, ForeignKey("variant_sequences.variant_id"), index=True,
        unique=False, nullable=True,
        doc="Foreign key to variant_sequences table. Indicates the "
        "mutant/variant version of the target used in the assay (where "
        "known/applicable)."
    )
    aidx = Column(
        String(200), index=False, unique=False, nullable=False,
        doc="The Depositor Defined Assay Identifier."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activities = rel(
        "Activities", back_populates="assays",
        doc="Relationship back to ``activities``."
    )
    docs = rel(
        "Docs", back_populates="assays", doc="Relationship back to ``docs``"
    )
    assay_type_rel = rel(
        "AssayType", back_populates="assays",
        doc="Relationship back to ``assay_type``"
    )
    target_dictionary = rel(
        "TargetDictionary", back_populates="assays",
        doc="Relationship back to ``target_dictionary``"
    )
    relationship_type_rel = rel(
        "RelationshipType", back_populates="assays",
        doc="Relationship back to ``relationship_type``"
    )
    confidence_score_lookup = rel(
        "ConfidenceScoreLookup", back_populates="assays",
        doc="Relationship back to ``confidence_score_lookup``"
    )
    curation_lookup = rel(
        "CurationLookup", back_populates="assays",
        doc="Relationship back to ``curation_lookup``"
    )
    source = rel(
        "Source", back_populates="assays",
        doc="Relationship back to ``source``"
    )
    chembl_id_lookup = rel(
        "ChemblIdLookup", back_populates="assays",
        doc="Relationship back to ``chembl_id_lookup``"
    )
    cell_dictionary = rel(
        "CellDictionary", back_populates="assays",
        doc="Relationship back to ``cell_dictionary``"
    )
    bioassay_ontology = rel(
        "BioassayOntology", back_populates="assays",
        doc="Relationship back to ``bioassay_ontology``"
    )
    tissue_dictionary = rel(
        "TissueDictionary", back_populates="assays",
        doc="Relationship back to ``tissue_dictionary``"
    )
    variant_sequences = rel(
        "VariantSequences", back_populates="assays",
        doc="Relationship back to ``variant_sequences``"
    )
    assay_class_map = rel(
        "AssayClassMap", back_populates="assays",
        doc="Relationship back to ``assay_class_map``."
    )
    assay_parameters = rel(
        "AssayParameters", back_populates="assays",
        doc="Relationship back to ``assay_parameters``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class AssayType(Base):
    """A representation of the ``assay_type`` table.

    Parameters
    ----------
    assay_type : `str`
        Single character representing assay type.
    assay_desc : `str`, optional, default: `NoneType`
        Description of assay type.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.

    Notes
    -----
    Description of assay types (e.g., Binding, Functional, ADMET).
    """
    __tablename__ = "assay_type"

    assay_type = Column(
        String(1), Sequence("assay_type_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Single character representing assay type."
    )
    assay_desc = Column(
        String(250), index=False, unique=False, nullable=True,
        doc="Description of assay type."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assays = rel(
        "Assays", back_populates="assay_type_rel",
        doc="Relationship back to ``assays``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CellDictionary(Base):
    """A representation of the ``cell_dictionary`` table.

    Parameters
    ----------
    cell_id : `int`
        Primary key. Unique identifier for each cell line in the
        target_dictionary.
    cell_name : `str`
        Name of each cell line (as used in the target_dicitonary pref_name).
        This is indexed. This is unique.
    cell_description : `str`, optional, default: `NoneType`
        Longer description (where available) of the cell line.
    cell_source_tissue : `str`, optional, default: `NoneType`
        Tissue from which the cell line is derived, where known.
    cell_source_organism : `str`, optional, default: `NoneType`
        Name of organism from which the cell line is derived.
    cell_source_tax_id : `int`, optional, default: `NoneType`
        NCBI tax ID of the organism from which the cell line is derived. This
        is indexed. This is unique.
    clo_id : `str`, optional, default: `NoneType`
        ID for the corresponding cell line in Cell Line Ontology.
    efo_id : `str`, optional, default: `NoneType`
        ID for the corresponding cell line in Experimental Factory Ontology.
    cellosaurus_id : `str`, optional, default: `NoneType`
        ID for the corresponding cell line in Cellosaurus Ontology.
    cl_lincs_id : `str`, optional, default: `NoneType`
        Cell ID used in LINCS (Library of Integrated Network-based Cellular
        Signatures).
    chembl_id : `str`, optional, default: `NoneType`
        ChEMBL identifier for the cell (used in web interface etc). This is
        indexed. This is unique. Foreign key to ``chembl_id_lookup.chembl_id``.
    cell_ontology_id : `str`, optional, default: `NoneType`
        ID for the corresponding cell type in the Cell Ontology.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.
    chembl_id_lookup : `chembl_orm.orm.ChemblIdLookup`, optional, default: \
    `NoneType`
        Relationship with ``chembl_id_lookup``.

    Notes
    -----
    Table storing information for cell lines in the target_dictionary (e.g.,
    tissue/species origin). Cell_name should match pref_name in
    target_dictionary.
    """
    __tablename__ = "cell_dictionary"

    cell_id = Column(
        Integer, Sequence("cell_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key. Unique identifier for each cell line in the "
        "target_dictionary."
    )
    cell_name = Column(
        String(50), index=True, unique=True, nullable=False,
        doc="Name of each cell line (as used in the target_dicitonary "
        "pref_name)."
    )
    cell_description = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Longer description (where available) of the cell line."
    )
    cell_source_tissue = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Tissue from which the cell line is derived, where known."
    )
    cell_source_organism = Column(
        String(150), index=False, unique=False, nullable=True,
        doc="Name of organism from which the cell line is derived."
    )
    cell_source_tax_id = Column(
        Integer, index=True, unique=True, nullable=True,
        doc="NCBI tax ID of the organism from which the cell line is derived."
    )
    clo_id = Column(
        String(11), index=False, unique=False, nullable=True,
        doc="ID for the corresponding cell line in Cell Line Ontology."
    )
    efo_id = Column(
        String(12), index=False, unique=False, nullable=True,
        doc="ID for the corresponding cell line in Experimental Factory "
        "Ontology."
    )
    cellosaurus_id = Column(
        String(15), index=False, unique=False, nullable=True,
        doc="ID for the corresponding cell line in Cellosaurus Ontology."
    )
    cl_lincs_id = Column(
        String(8), index=False, unique=False, nullable=True,
        doc="Cell ID used in LINCS (Library of Integrated Network-based "
        "Cellular Signatures)."
    )
    chembl_id = Column(
        String(20), ForeignKey("chembl_id_lookup.chembl_id"), index=True,
        unique=True, nullable=True,
        doc="ChEMBL identifier for the cell (used in web interface etc)."
    )
    cell_ontology_id = Column(
        String(10), index=False, unique=False, nullable=True,
        doc="ID for the corresponding cell type in the Cell Ontology."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assays = rel(
        "Assays", back_populates="cell_dictionary",
        doc="Relationship back to ``assays``."
    )
    chembl_id_lookup = rel(
        "ChemblIdLookup", back_populates="cell_dictionary",
        doc="Relationship back to ``chembl_id_lookup``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ChemblIdLookup(Base):
    """A representation of the ``chembl_id_lookup`` table.

    Parameters
    ----------
    chembl_id : `str`
        ChEMBL identifier.
    entity_type : `str`
        Type of entity (e.g., COMPOUND, ASSAY, TARGET). This is indexed. This
        is unique.
    entity_id : `int`
        Primary key for that entity in corresponding table (e.g., molregno for
        compounds, tid for targets). This is indexed. This is unique.
    status : `str`
        Indicates whether the status of the entity within the database -
        ACTIVE, INACTIVE (downgraded), OBS (obsolete/removed).
    last_active : `int`, optional, default: `NoneType`
        indicates the last ChEMBL version where the CHEMBL_ID was active.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.
    cell_dictionary : `chembl_orm.orm.CellDictionary`, optional, default: \
    `NoneType`
        Relationship with ``cell_dictionary``.
    docs : `chembl_orm.orm.Docs`, optional, default: `NoneType`
        Relationship with ``docs``.
    target_dictionary : `chembl_orm.orm.TargetDictionary`, optional, default: \
    `NoneType`
        Relationship with ``target_dictionary``.
    tissue_dictionary : `chembl_orm.orm.TissueDictionary`, optional, default: \
    `NoneType`
        Relationship with ``tissue_dictionary``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.

    Notes
    -----
    Lookup table storing chembl identifiers for different entities in the
    database (assays, compounds, documents and targets).
    """
    __tablename__ = "chembl_id_lookup"

    chembl_id = Column(
        String(20), Sequence("chembl_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="ChEMBL identifier."
    )
    entity_type = Column(
        String(50), index=True, unique=True, nullable=False,
        doc="Type of entity (e.g., COMPOUND, ASSAY, TARGET)."
    )
    entity_id = Column(
        Integer, index=True, unique=True, nullable=False,
        doc="Primary key for that entity in corresponding table (e.g., "
        "molregno for compounds, tid for targets)."
    )
    status = Column(
        String(10), index=False, unique=False, nullable=False,
        doc="Indicates whether the status of the entity within the database -"
        " ACTIVE, INACTIVE (downgraded), OBS (obsolete/removed)."
    )
    last_active = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="indicates the last ChEMBL version where the CHEMBL_ID was active."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assays = rel(
        "Assays", back_populates="chembl_id_lookup",
        doc="Relationship back to ``assays``."
    )
    cell_dictionary = rel(
        "CellDictionary", back_populates="chembl_id_lookup",
        doc="Relationship back to ``cell_dictionary``."
    )
    docs = rel(
        "Docs", back_populates="chembl_id_lookup",
        doc="Relationship back to ``docs``."
    )
    target_dictionary = rel(
        "TargetDictionary", back_populates="chembl_id_lookup",
        doc="Relationship back to ``target_dictionary``."
    )
    tissue_dictionary = rel(
        "TissueDictionary", back_populates="chembl_id_lookup",
        doc="Relationship back to ``tissue_dictionary``."
    )
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="chembl_id_lookup",
        doc="Relationship back to ``molecule_dictionary``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ConfidenceScoreLookup(Base):
    """A representation of the ``confidence_score_lookup`` table.

    Parameters
    ----------
    confidence_score : `int`
        0-9 score showing level of confidence in assignment of the precise
        molecular target of the assay.
    description : `str`
        Description of the target types assigned with each score.
    target_mapping : `str`
        Short description of the target types assigned with each score.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.

    Notes
    -----
    Lookup table describing how assays confidence scores are assigned depending
    on the type of target(s) assigned to the assay and the level of confidence
    in their molecular identity.
    """
    __tablename__ = "confidence_score_lookup"

    confidence_score = Column(
        Integer, Sequence("confidence_score_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="0-9 score showing level of confidence in assignment of the "
        "precise molecular target of the assay."
    )
    description = Column(
        String(100), index=False, unique=False, nullable=False,
        doc="Description of the target types assigned with each score."
    )
    target_mapping = Column(
        String(30), index=False, unique=False, nullable=False,
        doc="Short description of the target types assigned with each score."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assays = rel(
        "Assays", back_populates="confidence_score_lookup",
        doc="Relationship back to ``assays``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CurationLookup(Base):
    """A representation of the ``curation_lookup`` table.

    Parameters
    ----------
    curated_by : `str`
        Short description of the level of curation.
    description : `str`
        Definition of terms in the curated_by field.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.

    Notes
    -----
    Lookup table for assays.curated_by column. Shows level of curation that has
    been applied to the assay to target mapping.
    """
    __tablename__ = "curation_lookup"

    curated_by = Column(
        String(32), Sequence("curated_by_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Short description of the level of curation."
    )
    description = Column(
        String(100), index=False, unique=False, nullable=False,
        doc="Definition of terms in the curated_by field."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assays = rel(
        "Assays", back_populates="curation_lookup",
        doc="Relationship back to ``assays``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Docs(Base):
    """A representation of the ``docs`` table.

    Parameters
    ----------
    doc_id : `int`
        Unique ID for the document.
    journal : `str`, optional, default: `NoneType`
        Abbreviated journal name for an article.
    year : `int`, optional, default: `NoneType`
        Year of journal article publication.
    volume : `str`, optional, default: `NoneType`
        Volume of journal article.
    issue : `str`, optional, default: `NoneType`
        Issue of journal article.
    first_page : `str`, optional, default: `NoneType`
        First page number of journal article.
    last_page : `str`, optional, default: `NoneType`
        Last page number of journal article.
    pubmed_id : `int`, optional, default: `NoneType`
        NIH pubmed record ID, where available.
    doi : `str`, optional, default: `NoneType`
        Digital object identifier for this reference.
    chembl_id : `str`
        ChEMBL identifier for this document (for use on web interface etc).
        This is indexed. This is unique. Foreign key to
        ``chembl_id_lookup.chembl_id``.
    title : `str`, optional, default: `NoneType`
        Document title (e.g., Publication title or description of dataset).
    doc_type : `str`
        Type of the document (e.g., Publication, Deposited dataset).
    authors : `str`, optional, default: `NoneType`
        For a deposited dataset, the authors carrying out the screening and/or
        submitting the dataset.
    abstract : `str`, optional, default: `NoneType`
        For a deposited dataset, a brief description of the dataset.
    patent_id : `str`, optional, default: `NoneType`
        Patent ID for this document.
    ridx : `str`
        The Depositor Defined Reference Identifier.
    src_id : `int`
        Foreign key to Source table, indicating the source of this document.
        Foreign key to ``source.src_id``.
    chembl_release_id : `int`, optional, default: `NoneType`
        Foreign key to chembl_release table. Foreign key to
        ``chembl_release.chembl_release_id``.
    contact : `str`, optional, default: `NoneType`
        Details of someone willing to be contacted over the dataset
        (ideally ORCID ID, up to 3) - length 200.
    activities : `chembl_orm.orm.Activities`, optional, default: `NoneType`
        Relationship with ``activities``.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.
    chembl_id_lookup : `chembl_orm.orm.ChemblIdLookup`, optional, default: \
    `NoneType`
        Relationship with ``chembl_id_lookup``.
    source : `chembl_orm.orm.Source`, optional, default: `NoneType`
        Relationship with ``source``.
    chembl_release : `chembl_orm.orm.ChemblRelease`, optional, default: \
    `NoneType`
        Relationship with ``chembl_release``.
    compound_records : `chembl_orm.orm.CompoundRecords`, optional, default: \
    `NoneType`
        Relationship with ``compound_records``.

    Notes
    -----
    Holds all scientific documents (journal articles or patents) from which
    assays have been extracted.
    """
    __tablename__ = "docs"

    doc_id = Column(
        Integer, Sequence("doc_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Unique ID for the document."
    )
    journal = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Abbreviated journal name for an article."
    )
    year = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Year of journal article publication."
    )
    volume = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Volume of journal article."
    )
    issue = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Issue of journal article."
    )
    first_page = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="First page number of journal article."
    )
    last_page = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Last page number of journal article."
    )
    pubmed_id = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="NIH pubmed record ID, where available."
    )
    doi = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Digital object identifier for this reference."
    )
    chembl_id = Column(
        String(20), ForeignKey("chembl_id_lookup.chembl_id"), index=True,
        unique=True, nullable=False,
        doc="ChEMBL identifier for this document (for use on web interface "
        "etc)."
    )
    title = Column(
        String(500), index=False, unique=False, nullable=True,
        doc="Document title (e.g., Publication title or description of "
        "dataset)."
    )
    doc_type = Column(
        String(50), index=False, unique=False, nullable=False,
        doc="Type of the document (e.g., Publication, Deposited dataset)."
    )
    authors = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="For a deposited dataset, the authors carrying out the screening "
        "and/or submitting the dataset."
    )
    abstract = Column(
        Text, index=False, unique=False, nullable=True,
        doc="For a deposited dataset, a brief description of the dataset."
    )
    patent_id = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="Patent ID for this document."
    )
    ridx = Column(
        String(200), index=False, unique=False, nullable=False,
        doc="The Depositor Defined Reference Identifier."
    )
    src_id = Column(
        Integer, ForeignKey("source.src_id"), index=True, unique=False,
        nullable=False,
        doc="Foreign key to Source table, indicating the source of this "
        "document."
    )
    chembl_release_id = Column(
        Integer, ForeignKey("chembl_release.chembl_release_id"), index=True,
        unique=False, nullable=True, doc="Foreign key to chembl_release table."
    )
    contact = Column(
        String(200), nullable=True,
        doc="Details of someone willing to be contacted over the dataset"
        " (ideally ORCID ID, up to 3)"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activities = rel(
        "Activities", back_populates="docs",
        doc="Relationship back to ``activities``."
    )
    assays = rel(
        "Assays", back_populates="docs", doc="Relationship back to ``assays``."
    )
    chembl_id_lookup = rel(
        "ChemblIdLookup", back_populates="docs",
        doc="Relationship back to ``chembl_id_lookup``"
    )
    source = rel(
        "Source", back_populates="docs", doc="Relationship back to ``source``"
    )
    chembl_release = rel(
        "ChemblRelease", back_populates="docs",
        doc="Relationship back to ``chembl_release``"
    )
    compound_records = rel(
        "CompoundRecords", back_populates="docs",
        doc="Relationship back to ``compound_records``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ChemblRelease(Base):
    """A representation of the ``chembl_release`` table.

    Parameters
    ----------
    chembl_release_id : `int`
        Primary key.
    chembl_release : `str`, optional, default: `NoneType`
        ChEMBL release name.
    creation_date : `datetime.datetime`, optional, default: `NoneType`
        ChEMBL release creation date.
    docs : `chembl_orm.orm.Docs`, optional, default: `NoneType`
        Relationship with ``docs``.

    Notes
    -----
    Table listing ChEMBL releases with their creation dates.
    """
    __tablename__ = "chembl_release"

    chembl_release_id = Column(
        Integer, Sequence("chembl_release_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    chembl_release = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="ChEMBL release name."
    )
    creation_date = Column(
        DateTime, index=False, unique=False, nullable=True,
        doc="ChEMBL release creation date."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    docs = rel(
        "Docs", back_populates="chembl_release",
        doc="Relationship back to ``docs``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Source(Base):
    """A representation of the ``source`` table.

    Parameters
    ----------
    src_id : `int`
        Identifier for each source (used in compound_records and assays
        tables).
    src_description : `str`, optional, default: `NoneType`
        Description of the data source.
    src_short_name : `str`, optional, default: `NoneType`
        A short name for each data source, for display purposes.
    activities : `chembl_orm.orm.Activities`, optional, default: `NoneType`
        Relationship with ``activities``.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.
    docs : `chembl_orm.orm.Docs`, optional, default: `NoneType`
        Relationship with ``docs``.
    compound_records : `chembl_orm.orm.CompoundRecords`, optional, default: \
    `NoneType`
        Relationship with ``compound_records``.

    Notes
    -----
    Table showing source from which ChEMBL data is derived (e.g., literature,
    deposited datasets, patents, drug or clinical candidate sources etc).
    """
    __tablename__ = "source"

    src_id = Column(
        Integer, Sequence("src_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Identifier for each source (used in compound_records and assays "
        "tables)."
    )
    src_description = Column(
        String(500), index=False, unique=False, nullable=True,
        doc="Description of the data source."
    )
    src_short_name = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="A short name for each data source, for display purposes."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activities = rel(
        "Activities", back_populates="source",
        doc="Relationship back to ``activities``."
    )
    assays = rel(
        "Assays", back_populates="source",
        doc="Relationship back to ``assays``."
    )
    docs = rel(
        "Docs", back_populates="source", doc="Relationship back to ``docs``."
    )
    compound_records = rel(
        "CompoundRecords", back_populates="source",
        doc="Relationship back to ``compound_records``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RelationshipType(Base):
    """A representation of the ``relationship_type`` table.

    Parameters
    ----------
    relationship_type : `str`
        Relationship_type flag used in the assays table.
    relationship_desc : `str`, optional, default: `NoneType`
        Description of relationship_type flags.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.

    Notes
    -----
    Lookup table for assays.relationship_type column, showing whether assays
    are mapped to targets of the correct identity and species ('Direct') or
    close homologues ('Homologue').
    """
    __tablename__ = "relationship_type"

    relationship_type = Column(
        String(1), Sequence("relationship_type_seq"), index=False,
        unique=False, nullable=False, primary_key=True,
        doc="Relationship_type flag used in the assays table."
    )
    relationship_desc = Column(
        String(250), index=False, unique=False, nullable=True,
        doc="Description of relationship_type flags."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assays = rel(
        "Assays", back_populates="relationship_type_rel",
        doc="Relationship back to ``assays``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TargetDictionary(Base):
    """A representation of the ``target_dictionary`` table.

    Parameters
    ----------
    tid : `int`
        Unique ID for the target.
    target_type : `str`, optional, default: `NoneType`
        Describes whether target is a protein, an organism, a tissue etc.
        Foreign key to TARGET_TYPE table. Foreign key to
        ``target_type.target_type``.
    pref_name : `str`
        Preferred target name: manually curated.
    tax_id : `int`, optional, default: `NoneType`
        NCBI taxonomy id of target.
    organism : `str`, optional, default: `NoneType`
        Source organism of molecuar target or tissue, or the target organism if
        compound activity is reported in an organism rather than a protein or
        tissue.
    chembl_id : `str`
        ChEMBL identifier for this target (for use on web interface etc). This
        is indexed. This is unique. Foreign key to
        ``chembl_id_lookup.chembl_id``.
    species_group_flag : `bool`
        Flag to indicate whether the target represents a group of species,
        rather than an individual species (e.g., 'Bacterial DHFR'). Where set
        to 1, indicates that any associated target components will be a
        representative, rather than a comprehensive set.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.
    target_type_rel : `chembl_orm.orm.TargetType`, optional, default: \
    `NoneType`
        Relationship with ``target_type``.
    chembl_id_lookup : `chembl_orm.orm.ChemblIdLookup`, optional, default: \
    `NoneType`
        Relationship with ``chembl_id_lookup``.
    binding_sites : `chembl_orm.orm.BindingSites`, optional, default: \
    `NoneType`
        Relationship with ``binding_sites``.
    drug_mechanism : `chembl_orm.orm.DrugMechanism`, optional, default: \
    `NoneType`
        Relationship with ``drug_mechanism``.
    metabolism : `chembl_orm.orm.Metabolism`, optional, default: `NoneType`
        Relationship with ``metabolism``.
    target_components : `chembl_orm.orm.TargetComponents`, optional, default: \
    `NoneType`
        Relationship with ``target_components``.
    target_relations : `chembl_orm.orm.TargetRelations`, optional, default: \
    `NoneType`
        Relationship with ``target_relations``.
    target_relations_2 : `chembl_orm.orm.TargetRelations`, optional, default: \
    `NoneType`
        Relationship with ``target_relations``.

    Notes
    -----
    Target Dictionary containing all curated targets for ChEMBL. Includes both
    protein targets and non-protein targets (e.g., organisms, tissues, cell
    lines).
    """
    __tablename__ = "target_dictionary"

    tid = Column(
        Integer, Sequence("tid_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Unique ID for the target."
    )
    target_type = Column(
        String(30), ForeignKey("target_type.target_type"), index=True,
        unique=False, nullable=True,
        doc="Describes whether target is a protein, an organism, a tissue "
        "etc. Foreign key to TARGET_TYPE table."
    )
    pref_name = Column(
        String(200), index=False, unique=False, nullable=False,
        doc="Preferred target name: manually curated."
    )
    tax_id = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="NCBI taxonomy id of target."
    )
    organism = Column(
        String(150), index=False, unique=False, nullable=True,
        doc="Source organism of molecuar target or tissue, or the target "
        "organism if compound activity is reported in an organism rather than "
        "a protein or tissue."
    )
    chembl_id = Column(
        String(20), ForeignKey("chembl_id_lookup.chembl_id"), index=True,
        unique=True, nullable=False,
        doc="ChEMBL identifier for this target (for use on web interface etc)."
    )
    species_group_flag = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Flag to indicate whether the target represents a group of "
        "species, rather than an individual species (e.g., 'Bacterial DHFR'). "
        "Where set to 1, indicates that any associated target components will "
        "be a representative, rather than a comprehensive set."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assays = rel(
        "Assays", back_populates="target_dictionary",
        doc="Relationship back to ``assays``."
    )
    target_type_rel = rel(
        "TargetType", back_populates="target_dictionary",
        doc="Relationship back to ``target_type``"
    )
    chembl_id_lookup = rel(
        "ChemblIdLookup", back_populates="target_dictionary",
        doc="Relationship back to ``chembl_id_lookup``"
    )
    binding_sites = rel(
        "BindingSites", back_populates="target_dictionary",
        doc="Relationship back to ``binding_sites``."
    )
    drug_mechanism = rel(
        "DrugMechanism", back_populates="target_dictionary",
        doc="Relationship back to ``drug_mechanism``."
    )
    metabolism = rel(
        "Metabolism", back_populates="target_dictionary",
        doc="Relationship back to ``metabolism``."
    )
    target_components = rel(
        "TargetComponents", back_populates="target_dictionary",
        doc="Relationship back to ``target_components``."
    )
    # TODO: FLAG for checking
    target_relations = rel(
        "TargetRelations", back_populates="target_dictionary",
        doc="Relationship back to ``target_relations``.",
        primaryjoin="TargetRelations.tid == TargetDictionary.tid"
    )
    # TODO: FLAG for checking
    target_relations_2 = rel(
        "TargetRelations", back_populates="target_dictionary_2",
        doc="Relationship back to ``target_relations``.",
        primaryjoin="TargetRelations.related_tid == TargetDictionary.tid"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TargetType(Base):
    """A representation of the ``target_type`` table.

    Parameters
    ----------
    target_type : `str`
        Target type (as used in target dictionary).
    target_desc : `str`, optional, default: `NoneType`
        Description of target type.
    parent_type : `str`, optional, default: `NoneType`
        Higher level classification of target_type, allowing grouping of e.g.,
        all 'PROTEIN' targets, all 'NON-MOLECULAR' targets etc.
    target_dictionary : `chembl_orm.orm.TargetDictionary`, optional, default: \
    `NoneType`
        Relationship with ``target_dictionary``.

    Notes
    -----
    Lookup table for target types used in the target dictionary.
    """
    __tablename__ = "target_type"

    target_type = Column(
        String(30), Sequence("target_type_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Target type (as used in target dictionary)."
    )
    target_desc = Column(
        String(250), index=False, unique=False, nullable=True,
        doc="Description of target type."
    )
    parent_type = Column(
        String(25), index=False, unique=False, nullable=True,
        doc="Higher level classification of target_type, allowing grouping of"
        " e.g., all 'PROTEIN' targets, all 'NON-MOLECULAR' targets etc."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    target_dictionary = rel(
        "TargetDictionary", back_populates="target_type_rel",
        doc="Relationship back to ``target_dictionary``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TissueDictionary(Base):
    """A representation of the ``tissue_dictionary`` table.

    Parameters
    ----------
    tissue_id : `int`
        Primary key, numeric ID for each tissue.
    uberon_id : `str`, optional, default: `NoneType`
        Uberon ontology identifier for this tissue. This is indexed. This is
        unique.
    pref_name : `str`
        Name for the tissue (in most cases Uberon name). This is indexed. This
        is unique.
    efo_id : `str`, optional, default: `NoneType`
        Experimental Factor Ontology identifier for the tissue. This is
        indexed. This is unique.
    chembl_id : `str`
        ChEMBL identifier for this tissue (for use on web interface etc). This
        is indexed. This is unique. Foreign key to
        ``chembl_id_lookup.chembl_id``.
    bto_id : `str`, optional, default: `NoneType`
        BRENDA Tissue Ontology identifier for the tissue.
    caloha_id : `str`, optional, default: `NoneType`
        Swiss Institute for Bioinformatics CALOHA Ontology identifier for the
        tissue.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.
    chembl_id_lookup : `chembl_orm.orm.ChemblIdLookup`, optional, default: \
    `NoneType`
        Relationship with ``chembl_id_lookup``.

    Notes
    -----
    Table storing information about tissues used in assays.
    """
    __tablename__ = "tissue_dictionary"

    tissue_id = Column(
        Integer, Sequence("tissue_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key, numeric ID for each tissue."
    )
    uberon_id = Column(
        String(15), index=True, unique=True, nullable=True,
        doc="Uberon ontology identifier for this tissue."
    )
    pref_name = Column(
        String(200), index=True, unique=True, nullable=False,
        doc="Name for the tissue (in most cases Uberon name)."
    )
    efo_id = Column(
        String(20), index=True, unique=True, nullable=True,
        doc="Experimental Factor Ontology identifier for the tissue."
    )
    chembl_id = Column(
        String(20), ForeignKey("chembl_id_lookup.chembl_id"), index=True,
        unique=True, nullable=False,
        doc="ChEMBL identifier for this tissue (for use on web interface etc)."
    )
    bto_id = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="BRENDA Tissue Ontology identifier for the tissue."
    )
    caloha_id = Column(
        String(7), index=False, unique=False, nullable=True,
        doc="Swiss Institute for Bioinformatics CALOHA Ontology identifier "
        "for the tissue."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assays = rel(
        "Assays", back_populates="tissue_dictionary",
        doc="Relationship back to ``assays``."
    )
    chembl_id_lookup = rel(
        "ChemblIdLookup", back_populates="tissue_dictionary",
        doc="Relationship back to ``chembl_id_lookup``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class VariantSequences(Base):
    """A representation of the ``variant_sequences`` table.

    Parameters
    ----------
    variant_id : `int`
        Primary key, numeric ID for each sequence variant.
    mutation : `str`, optional, default: `NoneType`
        Details of variant(s) used, with residue positions adjusted to match
        provided sequence. This is indexed. This is unique.
    accession : `str`, optional, default: `NoneType`
        UniProt accesion for the representative sequence used as the base
        sequence (without variation). This is indexed. This is unique.
    version : `int`, optional, default: `NoneType`
        Version of the UniProt sequence used as the base sequence.
    isoform : `int`, optional, default: `NoneType`
        Details of the UniProt isoform used as the base sequence where
        relevant.
    sequence : `str`, optional, default: `NoneType`
        Variant sequence formed by adjusting the UniProt base sequence with the
        specified mutations/variations.
    organism : `str`, optional, default: `NoneType`
        Organism from which the sequence was obtained.
    tax_id : `int`, optional, default: `NoneType`
        NCBI Tax ID for the organism from which the sequence was obtained.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.
    drug_mechanism : `chembl_orm.orm.DrugMechanism`, optional, default: \
    `NoneType`
        Relationship with ``drug_mechanism``.

    Notes
    -----
    Table storing information about mutant sequences and other variants used in
    assays. The sequence provided is a representative sequence incorporating
    the reported mutation/variant used in the assay - it is not necessarily the
    exact sequence used in the experiment.
    """
    __tablename__ = "variant_sequences"

    variant_id = Column(
        Integer, Sequence("variant_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key, numeric ID for each sequence variant."
    )
    mutation = Column(
        String(2000), index=True, unique=True, nullable=True,
        doc="Details of variant(s) used, with residue positions adjusted to "
        "match provided sequence."
    )
    accession = Column(
        String(25), index=True, unique=True, nullable=True,
        doc="UniProt accesion for the representative sequence used as the "
        "base sequence (without variation)."
    )
    version = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Version of the UniProt sequence used as the base sequence."
    )
    isoform = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Details of the UniProt isoform used as the base sequence where "
        "relevant."
    )
    sequence = Column(
        Text, index=False, unique=False, nullable=True,
        doc="Variant sequence formed by adjusting the UniProt base sequence "
        "with the specified mutations/variations."
    )
    organism = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Organism from which the sequence was obtained."
    )
    tax_id = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="NCBI Tax ID for the organism from which the sequence was "
        "obtained."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assays = rel(
        "Assays", back_populates="variant_sequences",
        doc="Relationship back to ``assays``."
    )
    drug_mechanism = rel(
        "DrugMechanism", back_populates="variant_sequences",
        doc="Relationship back to ``drug_mechanism``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BioassayOntology(Base):
    """A representation of the ``bioassay_ontology`` table.

    Parameters
    ----------
    bao_id : `str`
        Bioassay Ontology identifier (BAO version 2.0).
    label : `str`
        Bioassay Ontology label for the term (BAO version 2.0).
    activities : `chembl_orm.orm.Activities`, optional, default: `NoneType`
        Relationship with ``activities``.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.

    Notes
    -----
    Lookup table providing labels for Bioassay Ontology terms used in assays
    and activities tables.
    """
    __tablename__ = "bioassay_ontology"

    bao_id = Column(
        String(11), Sequence("bao_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Bioassay Ontology identifier (BAO version 2.0)."
    )
    label = Column(
        String(100), index=False, unique=False, nullable=False,
        doc="Bioassay Ontology label for the term (BAO version 2.0)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activities = rel(
        "Activities", back_populates="bioassay_ontology",
        doc="Relationship back to ``activities``."
    )
    assays = rel(
        "Assays", back_populates="bioassay_ontology",
        doc="Relationship back to ``assays``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MoleculeDictionary(Base):
    """A representation of the ``molecule_dictionary`` table.

    Parameters
    ----------
    molregno : `int`
        Internal Primary Key for the molecule.
    pref_name : `str`, optional, default: `NoneType`
        Preferred name for the molecule.
    chembl_id : `str`
        ChEMBL identifier for this compound (for use on web interface etc).
        This is indexed. This is unique. Foreign key to
        ``chembl_id_lookup.chembl_id``.
    max_phase : `int`, optional, default: `NoneType`
        Maximum phase of development reached for the compound across all
        indications (4 = Approved, 3 = Phase 3 Clinical Trials, 2 = Phase 2
        Clinical Trials, 1 = Phase 1 Clinical Trials, 0.5 = Early Phase 1
        Clinical Trials, -1 = Clinical Phase unknown for drug or clinical
        candidate drug ie where ChEMBL cannot assign a clinical phase, NULL =
        preclinical compounds with bioactivity data).
    therapeutic_flag : `bool`
        Indicates that a drug has a therapeutic application, as opposed to
        e.g., an imaging agent, additive etc (1 = yes, 0 = default value).
    dosed_ingredient : `bool`
        Indicates that the drug is dosed in this form, e.g., a particular salt
        (1 = yes, 0 = default value).
    structure_type : `str`
        Indicates whether the molecule has a small molecule structure or a
        protein sequence (MOL indicates an entry in the compound_structures
        table, SEQ indications an entry in the protein_therapeutics table, NONE
        indicates an entry in neither table, e.g., structure unknown).
    chebi_par_id : `int`, optional, default: `NoneType`
        Preferred ChEBI ID for the compound (where different from assigned).
    molecule_type : `str`, optional, default: `NoneType`
        Type of molecule (Small molecule, Protein, Antibody, Oligosaccharide,
        Oligonucleotide, Cell, Enzyme, Gene, Unknown).
    first_approval : `int`, optional, default: `NoneType`
        Earliest known approval year for the drug (NULL is the default value).
    oral : `bool`
        Indicates whether the drug is known to be administered orally (1 = yes,
        0 = default value).
    parenteral : `bool`
        Indicates whether the drug is known to be administered parenterally (1
        = yes, 0 = default value).
    topical : `bool`
        Indicates whether the drug is known to be administered topically (1 =
        yes, 0 = default value).
    black_box_warning : `bool`
        Indicates that the drug has a black box warning (1 = yes, 0 = default
        value).
    natural_product : `bool`, optional, default: `NoneType`
        Indicates whether the compound is a natural product as defined by
        COCONUT (https://coconut.naturalproducts.net/), the COlleCtion of Open
        Natural ProdUcTs. (1 = yes, 0 = default value).
    first_in_class : `int`
        Indicates whether this is known to be the first approved drug of its
        class (e.g., acting on a particular target). This is regardless of the
        indication, or the route of administration (1 = yes, 0 = no, -1 =
        preclinical compound ie not a drug).
    chirality : `int`
        Shows whether a drug is dosed as a racemic mixture (0), single
        stereoisomer (1), an achiral molecule (2), or has unknown chirality
        (-1).
    prodrug : `int`
        Indicates that the drug is a pro-drug. See active_molregno field in
        molecule hierarchy for the pharmacologically active molecule, where
        known (1 = yes, 0 = no, -1 = preclinical compound ie not a drug).
    inorganic_flag : `int`
        Indicates whether the molecule is inorganic i.e., containing only metal
        atoms and <2 carbon atoms (1 = yes, 0 = no, -1 = preclinical compound
        ie not a drug).
    usan_year : `int`, optional, default: `NoneType`
        The year in which the application for a USAN/INN name was granted.
        (NULL is the default value).
    availability_type : `int`, optional, default: `NoneType`
        The availability type for the drug (-2 = withdrawn, -1 = unknown, 0 =
        discontinued, 1 = prescription only, 2 = over the counter).
    usan_stem : `str`, optional, default: `NoneType`
        Where the drug or clinical candidate name can be matched, this
        indicates the USAN stem (NULL is the default value). Also described in
        the USAN_STEMS table.
    polymer_flag : `bool`, optional, default: `NoneType`
        Indicates whether a molecule is a small molecule polymer, e.g.,
        polistyrex (1 = yes, 0 = default value).
    usan_substem : `str`, optional, default: `NoneType`
        Where the drug or clinical candidate name can be matched, this
        indicates the USAN substem (NULL is the default value).
    usan_stem_definition : `str`, optional, default: `NoneType`
        Definition of the USAN stem (NULL is the default value).
    indication_class : `str`, optional, default: `NoneType`
        Indication class(es) assigned to a drug in the USP dictionary. TO BE
        DEPRECATED.
    withdrawn_flag : `bool`
        Indicates an approved drug has been withdrawn for toxicity reasons for
        all indications, for all populations at all doses in at least one
        country (not necessarily in the US). (1 = yes, 0 = default value).
    chemical_probe : `bool`
        Indicates whether the compound is a chemical probe as defined by
        chemicalprobes.org. (1 = yes, 0 = default value).
    orphan : `int`, optional, default: `0`
        Indicates orphan designation, i.e. intended for use against a rare
        condition (1 = yes, 0 = no, -1 = preclinical compound ie not a drug)
    activities : `chembl_orm.orm.Activities`, optional, default: `NoneType`
        Relationship with ``activities``.
    chembl_id_lookup : `chembl_orm.orm.ChemblIdLookup`, optional, default: \
    `NoneType`
        Relationship with ``chembl_id_lookup``.
    compound_records : `chembl_orm.orm.CompoundRecords`, optional, default: \
    `NoneType`
        Relationship with ``compound_records``.
    biotherapeutics : `chembl_orm.orm.Biotherapeutics`, optional, default: \
    `NoneType`
        Relationship with ``biotherapeutics``.
    compound_properties : `chembl_orm.orm.CompoundProperties`, optional, \
    default: `NoneType`
        Relationship with ``compound_properties``.
    compound_structural_alerts : `chembl_orm.orm.CompoundStructuralAlerts`, \
    optional, default: `NoneType`
        Relationship with ``compound_structural_alerts``.
    compound_structures : `chembl_orm.orm.CompoundStructures`, optional, \
    default: `NoneType`
        Relationship with ``compound_structures``.
    drug_indication : `chembl_orm.orm.DrugIndication`, optional, default: \
    `NoneType`
        Relationship with ``drug_indication``.
    drug_mechanism : `chembl_orm.orm.DrugMechanism`, optional, default: \
    `NoneType`
        Relationship with ``drug_mechanism``.
    formulations : `chembl_orm.orm.Formulations`, optional, default: \
    `NoneType`
        Relationship with ``formulations``.
    molecule_atc_classification : `chembl_orm.orm.MoleculeAtcClassification`, \
    optional, default: `NoneType`
        Relationship with ``molecule_atc_classification``.
    molecule_frac_classification : \
    `chembl_orm.orm.MoleculeFracClassification`, optional, default: \
    `NoneType`
        Relationship with ``molecule_frac_classification``.
    molecule_hierarchy : `chembl_orm.orm.MoleculeHierarchy`, optional, \
    default: `NoneType`
        Relationship with ``molecule_hierarchy``.
    molecule_hierarchy_2 : `chembl_orm.orm.MoleculeHierarchy`, optional, \
    default: `NoneType`
        Relationship with ``molecule_hierarchy``.
    molecule_hierarchy_3 : `chembl_orm.orm.MoleculeHierarchy`, optional, \
    default: `NoneType`
        Relationship with ``molecule_hierarchy``.
    molecule_hrac_classification : \
    `chembl_orm.orm.MoleculeHracClassification`, optional, default: \
    `NoneType`
        Relationship with ``molecule_hrac_classification``.
    molecule_irac_classification : \
    `chembl_orm.orm.MoleculeIracClassification`, optional, default: \
    `NoneType`
        Relationship with ``molecule_irac_classification``.
    molecule_synonyms : `chembl_orm.orm.MoleculeSynonyms`, optional, default: \
    `NoneType`
        Relationship with ``molecule_synonyms``.

    Notes
    -----
    Table storing a non-redundant list of curated compounds for ChEMBL
    (includes preclinical compounds, drugs and clinical candidate drugs) and
    some associated attributes.
    """
    __tablename__ = "molecule_dictionary"

    molregno = Column(
        Integer, Sequence("molregno_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Internal Primary Key for the molecule."
    )
    pref_name = Column(
        String(255), index=False, unique=False, nullable=True,
        doc="Preferred name for the molecule."
    )
    chembl_id = Column(
        String(20), ForeignKey("chembl_id_lookup.chembl_id"), index=True,
        unique=True, nullable=False,
        doc="ChEMBL identifier for this compound (for use on web interface "
        "etc)."
    )
    max_phase = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Maximum phase of development reached for the compound across all"
        " indications (4 = Approved, 3 = Phase 3 Clinical Trials, 2 = Phase 2 "
        "Clinical Trials, 1 = Phase 1 Clinical Trials, 0.5 = Early Phase 1 "
        "Clinical Trials, -1 = Clinical Phase unknown for drug or clinical "
        "candidate drug ie where ChEMBL cannot assign a clinical phase, NULL ="
        " preclinical compounds with bioactivity data)."
    )
    therapeutic_flag = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Indicates that a drug has a therapeutic application, as opposed "
        "to e.g., an imaging agent, additive etc (1 = yes, 0 = default "
        "value)."
    )
    dosed_ingredient = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Indicates that the drug is dosed in this form, e.g., a "
        "particular salt (1 = yes, 0 = default value)."
    )
    structure_type = Column(
        String(10), index=False, unique=False, nullable=False,
        doc="Indicates whether the molecule has a small molecule structure or"
        " a protein sequence (MOL indicates an entry in the "
        "compound_structures table, SEQ indications an entry in the "
        "protein_therapeutics table, NONE indicates an entry in neither table,"
        " e.g., structure unknown)."
    )
    chebi_par_id = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Preferred ChEBI ID for the compound (where different from "
        "assigned)."
    )
    molecule_type = Column(
        String(30), index=False, unique=False, nullable=True,
        doc="Type of molecule (Small molecule, Protein, Antibody, "
        "Oligosaccharide, Oligonucleotide, Cell, Enzyme, Gene, Unknown)."
    )
    first_approval = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Earliest known approval year for the drug (NULL is the default "
        "value)."
    )
    oral = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Indicates whether the drug is known to be administered orally (1"
        " = yes, 0 = default value)."
    )
    parenteral = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Indicates whether the drug is known to be administered "
        "parenterally (1 = yes, 0 = default value)."
    )
    topical = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Indicates whether the drug is known to be administered topically"
        " (1 = yes, 0 = default value)."
    )
    black_box_warning = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Indicates that the drug has a black box warning (1 = yes, 0 = "
        "default value)."
    )
    natural_product = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Indicates whether the compound is a natural product as defined "
        "by COCONUT (https://coconut.naturalproducts.net/), the COlleCtion of "
        "Open Natural ProdUcTs. (1 = yes, 0 = default value)."
    )
    first_in_class = Column(
        Integer, index=False, unique=False, nullable=False,
        doc="Indicates whether this is known to be the first approved drug of"
        " its class (e.g., acting on a particular target). This is regardless "
        "of the indication, or the route of administration (1 = yes, 0 = no, "
        "-1 = preclinical compound ie not a drug)."
    )
    chirality = Column(
        Integer, index=False, unique=False, nullable=False,
        doc="Shows whether a drug is dosed as a racemic mixture (0), single "
        "stereoisomer (1), an achiral molecule (2), or has unknown chirality "
        "(-1)."
    )
    prodrug = Column(
        Integer, index=False, unique=False, nullable=False,
        doc="Indicates that the drug is a pro-drug. See active_molregno field"
        " in molecule hierarchy for the pharmacologically active molecule, "
        "where known (1 = yes, 0 = no, -1 = preclinical compound ie not a "
        "drug)."
    )
    inorganic_flag = Column(
        Integer, index=False, unique=False, nullable=False,
        doc="Indicates whether the molecule is inorganic i.e., containing "
        "only metal atoms and <2 carbon atoms (1 = yes, 0 = no, -1 = "
        "preclinical compound ie not a drug)."
    )
    usan_year = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="The year in which the application for a USAN/INN name was "
        "granted. (NULL is the default value)."
    )
    availability_type = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="The availability type for the drug (-2 = withdrawn, -1 = "
        "unknown, 0 = discontinued, 1 = prescription only, 2 = over the "
        "counter)."
    )
    usan_stem = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Where the drug or clinical candidate name can be matched, this "
        "indicates the USAN stem (NULL is the default value). Also described "
        "in the USAN_STEMS table."
    )
    polymer_flag = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Indicates whether a molecule is a small molecule polymer, e.g., "
        "polistyrex (1 = yes, 0 = default value)."
    )
    usan_substem = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Where the drug or clinical candidate name can be matched, this "
        "indicates the USAN substem (NULL is the default value)."
    )
    usan_stem_definition = Column(
        String(1000), index=False, unique=False, nullable=True,
        doc="Definition of the USAN stem (NULL is the default value)."
    )
    indication_class = Column(
        String(1000), index=False, unique=False, nullable=True,
        doc="Indication class(es) assigned to a drug in the USP dictionary. "
        "TO BE DEPRECATED."
    )
    withdrawn_flag = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Indicates an approved drug has been withdrawn for toxicity "
        "reasons for all indications, for all populations at all doses in at "
        "least one country (not necessarily in the US). (1 = yes, 0 = default "
        "value)."
    )
    chemical_probe = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Indicates whether the compound is a chemical probe as defined by"
        " chemicalprobes.org. (1 = yes, 0 = default value)."
    )
    orphan = Column(
        SmallInteger, nullable=False, default=0,
        doc="Indicates orphan designation, i.e. intended for use against a"
        " rare condition (1 = yes, 0 = no, -1 = preclinical compound ie not"
        " a drug)"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activities = rel(
        "Activities", back_populates="molecule_dictionary",
        doc="Relationship back to ``activities``."
    )
    chembl_id_lookup = rel(
        "ChemblIdLookup", back_populates="molecule_dictionary",
        doc="Relationship back to ``chembl_id_lookup``"
    )
    compound_records = rel(
        "CompoundRecords", back_populates="molecule_dictionary",
        doc="Relationship back to ``compound_records``."
    )
    biotherapeutics = rel(
        "Biotherapeutics", back_populates="molecule_dictionary",
        doc="Relationship back to ``biotherapeutics``."
    )
    compound_properties = rel(
        "CompoundProperties", back_populates="molecule_dictionary",
        doc="Relationship back to ``compound_properties``."
    )
    compound_structural_alerts = rel(
        "CompoundStructuralAlerts", back_populates="molecule_dictionary",
        doc="Relationship back to ``compound_structural_alerts``."
    )
    compound_structures = rel(
        "CompoundStructures", back_populates="molecule_dictionary",
        doc="Relationship back to ``compound_structures``."
    )
    drug_indication = rel(
        "DrugIndication", back_populates="molecule_dictionary",
        doc="Relationship back to ``drug_indication``."
    )
    drug_mechanism = rel(
        "DrugMechanism", back_populates="molecule_dictionary",
        doc="Relationship back to ``drug_mechanism``."
    )
    formulations = rel(
        "Formulations", back_populates="molecule_dictionary",
        doc="Relationship back to ``formulations``."
    )
    molecule_atc_classification = rel(
        "MoleculeAtcClassification", back_populates="molecule_dictionary",
        doc="Relationship back to ``molecule_atc_classification``."
    )
    molecule_frac_classification = rel(
        "MoleculeFracClassification", back_populates="molecule_dictionary",
        doc="Relationship back to ``molecule_frac_classification``."
    )
    # TODO: FLAG for checking
    molecule_hierarchy = rel(
        "MoleculeHierarchy", back_populates="molecule_dictionary",
        doc="Relationship back to ``molecule_hierarchy``.",
        primaryjoin="MoleculeHierarchy.molregno == MoleculeDictionary.molregno"
    )
    # TODO: FLAG for checking
    molecule_hierarchy_2 = rel(
        "MoleculeHierarchy", back_populates="molecule_dictionary_2",
        doc="Relationship back to ``molecule_hierarchy``.",
        primaryjoin="MoleculeHierarchy.parent_molregno == "
        "MoleculeDictionary.molregno"
    )
    # TODO: FLAG for checking
    molecule_hierarchy_3 = rel(
        "MoleculeHierarchy", back_populates="molecule_dictionary_3",
        doc="Relationship back to ``molecule_hierarchy``.",
        primaryjoin="MoleculeHierarchy.active_molregno == "
        "MoleculeDictionary.molregno"
    )
    molecule_hrac_classification = rel(
        "MoleculeHracClassification", back_populates="molecule_dictionary",
        doc="Relationship back to ``molecule_hrac_classification``."
    )
    molecule_irac_classification = rel(
        "MoleculeIracClassification", back_populates="molecule_dictionary",
        doc="Relationship back to ``molecule_irac_classification``."
    )
    molecule_synonyms = rel(
        "MoleculeSynonyms", back_populates="molecule_dictionary",
        doc="Relationship back to ``molecule_synonyms``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CompoundRecords(Base):
    """A representation of the ``compound_records`` table.

    Parameters
    ----------
    record_id : `int`
        Unique ID for a compound/record.
    molregno : `int`, optional, default: `NoneType`
        Foreign key to compounds table (compound structure). Foreign key to
        ``molecule_dictionary.molregno``.
    doc_id : `int`
        Foreign key to documents table. Foreign key to ``docs.doc_id``.
    compound_key : `str`, optional, default: `NoneType`
        Key text identifying this compound in the scientific document.
    compound_name : `str`, optional, default: `NoneType`
        Name of this compound recorded in the scientific document.
    src_id : `int`
        Foreign key to source table. Foreign key to ``source.src_id``.
    src_compound_id : `str`, optional, default: `NoneType`
        Identifier for the compound in the source database (e.g., pubchem SID).
    cidx : `str`
        The Depositor Defined Compound Identifier.
    activities : `chembl_orm.orm.Activities`, optional, default: `NoneType`
        Relationship with ``activities``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.
    docs : `chembl_orm.orm.Docs`, optional, default: `NoneType`
        Relationship with ``docs``.
    source : `chembl_orm.orm.Source`, optional, default: `NoneType`
        Relationship with ``source``.
    drug_indication : `chembl_orm.orm.DrugIndication`, optional, default: \
    `NoneType`
        Relationship with ``drug_indication``.
    drug_mechanism : `chembl_orm.orm.DrugMechanism`, optional, default: \
    `NoneType`
        Relationship with ``drug_mechanism``.
    drug_warning : `chembl_orm.orm.DrugWarning`, optional, default: \
    `NoneType`
        Relationship with ``drug_warning``.
    formulations : `chembl_orm.orm.Formulations`, optional, default: \
    `NoneType`
        Relationship with ``formulations``.
    metabolism : `chembl_orm.orm.Metabolism`, optional, default: `NoneType`
        Relationship with ``metabolism``.
    metabolism_2 : `chembl_orm.orm.Metabolism`, optional, default: `NoneType`
        Relationship with ``metabolism``.
    metabolism_3 : `chembl_orm.orm.Metabolism`, optional, default: `NoneType`
        Relationship with ``metabolism``.

    Notes
    -----
    Represents each compound extracted from scientific documents.
    """
    __tablename__ = "compound_records"

    record_id = Column(
        Integer, Sequence("record_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Unique ID for a compound/record."
    )
    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=False, nullable=True,
        doc="Foreign key to compounds table (compound structure)."
    )
    doc_id = Column(
        Integer, ForeignKey("docs.doc_id"), index=True, unique=False,
        nullable=False, doc="Foreign key to documents table."
    )
    compound_key = Column(
        String(250), index=False, unique=False, nullable=True,
        doc="Key text identifying this compound in the scientific document."
    )
    compound_name = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="Name of this compound recorded in the scientific document."
    )
    src_id = Column(
        Integer, ForeignKey("source.src_id"), index=True, unique=False,
        nullable=False, doc="Foreign key to source table."
    )
    src_compound_id = Column(
        String(150), index=False, unique=False, nullable=True,
        doc="Identifier for the compound in the source database (e.g., "
        "pubchem SID)."
    )
    cidx = Column(
        String(200), index=False, unique=False, nullable=False,
        doc="The Depositor Defined Compound Identifier."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activities = rel(
        "Activities", back_populates="compound_records",
        doc="Relationship back to ``activities``."
    )
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="compound_records",
        doc="Relationship back to ``molecule_dictionary``"
    )
    docs = rel(
        "Docs", back_populates="compound_records",
        doc="Relationship back to ``docs``"
    )
    source = rel(
        "Source", back_populates="compound_records",
        doc="Relationship back to ``source``"
    )
    drug_indication = rel(
        "DrugIndication", back_populates="compound_records",
        doc="Relationship back to ``drug_indication``."
    )
    drug_mechanism = rel(
        "DrugMechanism", back_populates="compound_records",
        doc="Relationship back to ``drug_mechanism``."
    )
    drug_warning = rel(
        "DrugWarning", back_populates="compound_records",
        doc="Relationship back to ``drug_warning``."
    )
    formulations = rel(
        "Formulations", back_populates="compound_records",
        doc="Relationship back to ``formulations``."
    )
    # TODO: FLAG for checking
    metabolism = rel(
        "Metabolism", back_populates="compound_records",
        doc="Relationship back to ``metabolism``.",
        primaryjoin="Metabolism.drug_record_id == CompoundRecords.record_id"
    )
    # TODO: FLAG for checking
    metabolism_2 = rel(
        "Metabolism", back_populates="compound_records_2",
        doc="Relationship back to ``metabolism``.",
        primaryjoin="Metabolism.substrate_record_id == "
        "CompoundRecords.record_id"
    )
    # TODO: FLAG for checking
    metabolism_3 = rel(
        "Metabolism", back_populates="compound_records_3",
        doc="Relationship back to ``metabolism``.",
        primaryjoin="Metabolism.metabolite_record_id == "
        "CompoundRecords.record_id"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DataValidityLookup(Base):
    """A representation of the ``data_validity_lookup`` table.

    Parameters
    ----------
    data_validity_comment : `str`
        Primary key. Short description of various types of errors/warnings
        applied to values in the activities table.
    description : `str`, optional, default: `NoneType`
        Definition of the terms in the data_validity_comment field.
    activities : `chembl_orm.orm.Activities`, optional, default: `NoneType`
        Relationship with ``activities``.

    Notes
    -----
    Table storing information about the data_validity_comment values used in
    the activities table.
    """
    __tablename__ = "data_validity_lookup"

    data_validity_comment = Column(
        String(30), Sequence("data_validity_comment_seq"), index=False,
        unique=False, nullable=False, primary_key=True,
        doc="Primary key. Short description of various types of "
        "errors/warnings applied to values in the activities table."
    )
    description = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Definition of the terms in the data_validity_comment field."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activities = rel(
        "Activities", back_populates="data_validity_lookup",
        doc="Relationship back to ``activities``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ActivityProperties(Base):
    """A representation of the ``activity_properties`` table.

    Parameters
    ----------
    ap_id : `int`
        Unique ID for each record.
    activity_id : `int`
        FK to ACTIVITY_ID in ACTIVITIES table. This is indexed. This is unique.
        Foreign key to ``activities.activity_id``.
    type : `str`
        The parameter or property type. This is indexed. This is unique.
    relation : `str`, optional, default: `NoneType`
        Symbol constraining the value (e.g. >, <, =).
    value : `float`, optional, default: `NoneType`
        Numberical value for the parameter or property.
    units : `str`, optional, default: `NoneType`
        Units of measurement.
    text_value : `str`, optional, default: `NoneType`
        Non-numerical value of the parameter or property.
    standard_type : `str`, optional, default: `NoneType`
        Standardised form of the TYPE.
    standard_relation : `str`, optional, default: `NoneType`
        Standardised form of the RELATION.
    standard_value : `float`, optional, default: `NoneType`
        Standardised form of the VALUE.
    standard_units : `str`, optional, default: `NoneType`
        Standardised form of the UNITS.
    standard_text_value : `str`, optional, default: `NoneType`
        Standardised form of the TEXT_VALUE.
    comments : `str`, optional, default: `NoneType`
        A Comment.
    result_flag : `bool`
        A flag to indicate, if set to 1, that this type is a dependent
        variable/result (e.g., slope) rather than an independent
        variable/parameter (0, the default).
    activities : `chembl_orm.orm.Activities`, optional, default: `NoneType`
        Relationship with ``activities``.

    Notes
    -----
    Table storing parameters and properties of Activity_IDs in the ACTIVITIES
    table - can be either independent variables that do not apply to the whole
    assay (e.g., compounds 'DOSE'), or dependent variables/results that are
    important in interpreting the activity values (e.g., 'HILL_SLOPE').
    """
    __tablename__ = "activity_properties"

    ap_id = Column(
        Integer, Sequence("ap_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Unique ID for each record."
    )
    activity_id = Column(
        Integer, ForeignKey("activities.activity_id"), index=True,
        unique=True, nullable=False,
        doc="FK to ACTIVITY_ID in ACTIVITIES table."
    )
    type = Column(
        String(250), index=True, unique=True, nullable=False,
        doc="The parameter or property type."
    )
    relation = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Symbol constraining the value (e.g. >, <, =)."
    )
    value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Numberical value for the parameter or property."
    )
    units = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Units of measurement."
    )
    text_value = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Non-numerical value of the parameter or property."
    )
    standard_type = Column(
        String(250), index=False, unique=False, nullable=True,
        doc="Standardised form of the TYPE."
    )
    standard_relation = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Standardised form of the RELATION."
    )
    standard_value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Standardised form of the VALUE."
    )
    standard_units = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Standardised form of the UNITS."
    )
    standard_text_value = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Standardised form of the TEXT_VALUE."
    )
    comments = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="A Comment."
    )
    result_flag = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="A flag to indicate, if set to 1, that this type is a dependent "
        "variable/result (e.g., slope) rather than an independent "
        "variable/parameter (0, the default)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activities = rel(
        "Activities", back_populates="activity_properties",
        doc="Relationship back to ``activities``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ActivitySmid(Base):
    """A representation of the ``activity_smid`` table.

    Parameters
    ----------
    smid : `int`
        FK to SMID in ACTIVITY_SUPP_MAP, and a FK to SMID in ACTIVITY_SUPP.
    activity_supp : `chembl_orm.orm.ActivitySupp`, optional, default: \
    `NoneType`
        Relationship with ``activity_supp``.
    activity_supp_map : `chembl_orm.orm.ActivitySuppMap`, optional, default: \
    `NoneType`
        Relationship with ``activity_supp_map``.

    Notes
    -----
    A join table between ACTIVITY_SUPP_MAP and ACTIVITY_SUPP.
    """
    __tablename__ = "activity_smid"

    smid = Column(
        Integer, Sequence("smid_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="FK to SMID in ACTIVITY_SUPP_MAP, and a FK to SMID in "
        "ACTIVITY_SUPP."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activity_supp = rel(
        "ActivitySupp", back_populates="activity_smid",
        doc="Relationship back to ``activity_supp``."
    )
    activity_supp_map = rel(
        "ActivitySuppMap", back_populates="activity_smid",
        doc="Relationship back to ``activity_supp_map``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ActivityStdsLookup(Base):
    """A representation of the ``activity_stds_lookup`` table.

    Parameters
    ----------
    std_act_id : `int`
        Primary key.
    standard_type : `str`
        The standard_type that other published_types in the activities table
        have been converted to. This is indexed. This is unique.
    definition : `str`, optional, default: `NoneType`
        A description/definition of the standard_type.
    standard_units : `str`
        The units that are applied to this standard_type and to which other
        published_units are converted. Note a standard_type may have more than
        one allowable standard_unit and therefore multiple rows in this table.
        This is indexed. This is unique.
    normal_range_min : `float`, optional, default: `NoneType`
        The lowest value for this activity type that is likely to be genuine.
        This is only an approximation, so lower genuine values may exist, but
        it may be desirable to validate these before using them. For a given
        standard_type/units, values in the activities table below this
        threshold are flagged with a data_validity_comment of 'Outside typical
        range'.
    normal_range_max : `float`, optional, default: `NoneType`
        The highest value for this activity type that is likely to be genuine.
        This is only an approximation, so higher genuine values may exist, but
        it may be desirable to validate these before using them. For a given
        standard_type/units, values in the activities table above this
        threshold are flagged with a data_validity_comment of 'Outside typical
        range'.

    Notes
    -----
    Table storing details of standardised activity types and units, with
    permitted value ranges. Used to determine the correct standard_type and
    standard_units for a range of published types/units.
    """
    __tablename__ = "activity_stds_lookup"

    std_act_id = Column(
        Integer, Sequence("std_act_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    standard_type = Column(
        String(250), index=True, unique=True, nullable=False,
        doc="The standard_type that other published_types in the activities "
        "table have been converted to."
    )
    definition = Column(
        String(500), index=False, unique=False, nullable=True,
        doc="A description/definition of the standard_type."
    )
    standard_units = Column(
        String(100), index=True, unique=True, nullable=False,
        doc="The units that are applied to this standard_type and to which "
        "other published_units are converted. Note a standard_type may have "
        "more than one allowable standard_unit and therefore multiple rows in "
        "this table."
    )
    normal_range_min = Column(
        Float, index=False, unique=False, nullable=True,
        doc="The lowest value for this activity type that is likely to be "
        "genuine. This is only an approximation, so lower genuine values may "
        "exist, but it may be desirable to validate these before using them. "
        "For a given standard_type/units, values in the activities table below"
        " this threshold are flagged with a data_validity_comment of 'Outside "
        "typical range'."
    )
    normal_range_max = Column(
        Float, index=False, unique=False, nullable=True,
        doc="The highest value for this activity type that is likely to be "
        "genuine. This is only an approximation, so higher genuine values may "
        "exist, but it may be desirable to validate these before using them. "
        "For a given standard_type/units, values in the activities table above"
        " this threshold are flagged with a data_validity_comment of 'Outside "
        "typical range'."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ActivitySupp(Base):
    """A representation of the ``activity_supp`` table.

    Parameters
    ----------
    as_id : `int`
        Unique ID for each record.
    rgid : `int`
        Record Grouping ID, used to group together related data points in this
        table. This is indexed. This is unique.
    smid : `int`, optional, default: `NoneType`
        FK to SMID in ACTIVITY_SMID. Foreign key to ``activity_smid.smid``.
    type : `str`
        Type of end-point measurement: e.g. IC50, LD50, %inhibition etc, as it
        appears in the original dataset. This is indexed. This is unique.
    relation : `str`, optional, default: `NoneType`
        Symbol constraining the activity value (e.g. >, <, =), as it appears in
        the original dataset.
    value : `float`, optional, default: `NoneType`
        Datapoint value as it appears in the original dataset.
    units : `str`, optional, default: `NoneType`
        Units of measurement as they appear in the original dataset.
    text_value : `str`, optional, default: `NoneType`
        Non-numeric value for measurement as in original dataset.
    standard_type : `str`, optional, default: `NoneType`
        Standardised form of the TYPE.
    standard_relation : `str`, optional, default: `NoneType`
        Standardised form of the RELATION.
    standard_value : `float`, optional, default: `NoneType`
        Standardised form of the VALUE.
    standard_units : `str`, optional, default: `NoneType`
        Standardised form of the UNITS.
    standard_text_value : `str`, optional, default: `NoneType`
        Standardised form of the TEXT_VALUE.
    comments : `str`, optional, default: `NoneType`
        A Comment.
    activity_smid : `chembl_orm.orm.ActivitySmid`, optional, default: \
    `NoneType`
        Relationship with ``activity_smid``.

    Notes
    -----
    Supplementary / Supporting Data for ACTIVITIES - can be linked via
    ACTIVITY_SMID and ACTIVITY_SUPP_MAP tables.
    """
    __tablename__ = "activity_supp"

    as_id = Column(
        Integer, Sequence("as_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Unique ID for each record."
    )
    rgid = Column(
        Integer, index=True, unique=True, nullable=False,
        doc="Record Grouping ID, used to group together related data points "
        "in this table."
    )
    smid = Column(
        Integer, ForeignKey("activity_smid.smid"), index=True, unique=False,
        nullable=True, doc="FK to SMID in ACTIVITY_SMID."
    )
    type = Column(
        String(250), index=True, unique=True, nullable=False,
        doc="Type of end-point measurement: e.g. IC50, LD50, %inhibition etc,"
        " as it appears in the original dataset."
    )
    relation = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Symbol constraining the activity value (e.g. >, <, =), as it "
        "appears in the original dataset."
    )
    value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Datapoint value as it appears in the original dataset."
    )
    units = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Units of measurement as they appear in the original dataset."
    )
    text_value = Column(
        String(1000), index=False, unique=False, nullable=True,
        doc="Non-numeric value for measurement as in original dataset."
    )
    standard_type = Column(
        String(250), index=False, unique=False, nullable=True,
        doc="Standardised form of the TYPE."
    )
    standard_relation = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Standardised form of the RELATION."
    )
    standard_value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Standardised form of the VALUE."
    )
    standard_units = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Standardised form of the UNITS."
    )
    standard_text_value = Column(
        String(1000), index=False, unique=False, nullable=True,
        doc="Standardised form of the TEXT_VALUE."
    )
    comments = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="A Comment."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activity_smid = rel(
        "ActivitySmid", back_populates="activity_supp",
        doc="Relationship back to ``activity_smid``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ActivitySuppMap(Base):
    """A representation of the ``activity_supp_map`` table.

    Parameters
    ----------
    actsm_id : `int`
        Primary key.
    activity_id : `int`
        FK to ACTIVITY_ID in ACTIVITIES table. Foreign key to
        ``activities.activity_id``.
    smid : `int`
        FK to SMID in ACTIVITY_SMID. Foreign key to ``activity_smid.smid``.
    activities : `chembl_orm.orm.Activities`, optional, default: `NoneType`
        Relationship with ``activities``.
    activity_smid : `chembl_orm.orm.ActivitySmid`, optional, default: \
    `NoneType`
        Relationship with ``activity_smid``.

    Notes
    -----
    Mapping table, linking supplementary / supporting data from the
    ACTIVITY_SUPP table to the main ACTIVITIES table.
    """
    __tablename__ = "activity_supp_map"

    actsm_id = Column(
        Integer, Sequence("actsm_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    activity_id = Column(
        Integer, ForeignKey("activities.activity_id"), index=True,
        unique=False, nullable=False,
        doc="FK to ACTIVITY_ID in ACTIVITIES table."
    )
    smid = Column(
        Integer, ForeignKey("activity_smid.smid"), index=True, unique=False,
        nullable=False, doc="FK to SMID in ACTIVITY_SMID."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activities = rel(
        "Activities", back_populates="activity_supp_map",
        doc="Relationship back to ``activities``"
    )
    activity_smid = rel(
        "ActivitySmid", back_populates="activity_supp_map",
        doc="Relationship back to ``activity_smid``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class AssayClassMap(Base):
    """A representation of the ``assay_class_map`` table.

    Parameters
    ----------
    ass_cls_map_id : `int`
        Primary key.
    assay_id : `int`
        Foreign key that maps to the ASSAYS table. This is indexed. This is
        unique. Foreign key to ``assays.assay_id``.
    assay_class_id : `int`
        Foreign key that maps to the ASSAY_CLASSIFICATION table. This is
        indexed. This is unique. Foreign key to
        ``assay_classification.assay_class_id``.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.
    assay_classification : `chembl_orm.orm.AssayClassification`, optional, \
    default: `NoneType`
        Relationship with ``assay_classification``.

    Notes
    -----
    Mapping table linking assays to classes in the ASSAY_CLASSIFICATION table.
    """
    __tablename__ = "assay_class_map"

    ass_cls_map_id = Column(
        Integer, Sequence("ass_cls_map_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    assay_id = Column(
        Integer, ForeignKey("assays.assay_id"), index=True, unique=True,
        nullable=False, doc="Foreign key that maps to the ASSAYS table."
    )
    assay_class_id = Column(
        Integer, ForeignKey("assay_classification.assay_class_id"),
        index=True, unique=True, nullable=False,
        doc="Foreign key that maps to the ASSAY_CLASSIFICATION table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assays = rel(
        "Assays", back_populates="assay_class_map",
        doc="Relationship back to ``assays``"
    )
    assay_classification = rel(
        "AssayClassification", back_populates="assay_class_map",
        doc="Relationship back to ``assay_classification``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class AssayClassification(Base):
    """A representation of the ``assay_classification`` table.

    Parameters
    ----------
    assay_class_id : `int`
        Primary key.
    l1 : `str`, optional, default: `NoneType`
        High level classification e.g., by anatomical/therapeutic area.
    l2 : `str`, optional, default: `NoneType`
        Mid-level classification e.g., by phenotype/biological process.
    l3 : `str`, optional, default: `NoneType`
        Fine-grained classification e.g., by assay type. This is indexed. This
        is unique.
    class_type : `str`, optional, default: `NoneType`
        The type of assay being classified e.g., in vivo efficacy.
    source : `str`, optional, default: `NoneType`
        Source from which the assay class was obtained.
    assay_class_map : `chembl_orm.orm.AssayClassMap`, optional, default: \
    `NoneType`
        Relationship with ``assay_class_map``.

    Notes
    -----
    Classification scheme for phenotypic assays e.g., by therapeutic area,
    phenotype/process and assay type. Can be used to find standard assays for a
    particular disease area or phenotype e.g., anti-obesity assays. Currently
    data are available only for in vivo efficacy assays.
    """
    __tablename__ = "assay_classification"

    assay_class_id = Column(
        Integer, Sequence("assay_class_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    l1 = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="High level classification e.g., by anatomical/therapeutic area."
    )
    l2 = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Mid-level classification e.g., by phenotype/biological process."
    )
    l3 = Column(
        String(1000), index=True, unique=True, nullable=True,
        doc="Fine-grained classification e.g., by assay type."
    )
    class_type = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="The type of assay being classified e.g., in vivo efficacy."
    )
    source = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Source from which the assay class was obtained."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assay_class_map = rel(
        "AssayClassMap", back_populates="assay_classification",
        doc="Relationship back to ``assay_class_map``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class AssayParameters(Base):
    """A representation of the ``assay_parameters`` table.

    Parameters
    ----------
    assay_param_id : `int`
        Numeric primary key.
    assay_id : `int`
        Foreign key to assays table. The assay to which this parameter belongs.
        This is indexed. This is unique. Foreign key to ``assays.assay_id``.
    type : `str`
        The type of parameter being described, according to the original data
        source. This is indexed. This is unique.
    relation : `str`, optional, default: `NoneType`
        The relation symbol for the parameter being described, according to the
        original data source.
    value : `float`, optional, default: `NoneType`
        The value of the parameter being described, according to the original
        data source. Used for numeric data.
    units : `str`, optional, default: `NoneType`
        The units for the parameter being described, according to the original
        data source.
    text_value : `str`, optional, default: `NoneType`
        The text value of the parameter being described, according to the
        original data source. Used for non-numeric/qualitative data.
    standard_type : `str`, optional, default: `NoneType`
        Standardized form of the TYPE.
    standard_relation : `str`, optional, default: `NoneType`
        Standardized form of the RELATION.
    standard_value : `float`, optional, default: `NoneType`
        Standardized form of the VALUE.
    standard_units : `str`, optional, default: `NoneType`
        Standardized form of the UNITS.
    standard_text_value : `str`, optional, default: `NoneType`
        Standardized form of the TEXT_VALUE.
    comments : `str`, optional, default: `NoneType`
        Additional comments describing the parameter.
    assays : `chembl_orm.orm.Assays`, optional, default: `NoneType`
        Relationship with ``assays``.

    Notes
    -----
    Table storing additional parameters for an assay e.g., dose, administration
    route, time point.
    """
    __tablename__ = "assay_parameters"

    assay_param_id = Column(
        Integer, Sequence("assay_param_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Numeric primary key."
    )
    assay_id = Column(
        Integer, ForeignKey("assays.assay_id"), index=True, unique=True,
        nullable=False,
        doc="Foreign key to assays table. The assay to which this parameter "
        "belongs."
    )
    type = Column(
        String(250), index=True, unique=True, nullable=False,
        doc="The type of parameter being described, according to the original"
        " data source."
    )
    relation = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="The relation symbol for the parameter being described, according"
        " to the original data source."
    )
    value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="The value of the parameter being described, according to the "
        "original data source. Used for numeric data."
    )
    units = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="The units for the parameter being described, according to the "
        "original data source."
    )
    text_value = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="The text value of the parameter being described, according to "
        "the original data source. Used for non-numeric/qualitative data."
    )
    standard_type = Column(
        String(250), index=False, unique=False, nullable=True,
        doc="Standardized form of the TYPE."
    )
    standard_relation = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Standardized form of the RELATION."
    )
    standard_value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Standardized form of the VALUE."
    )
    standard_units = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Standardized form of the UNITS."
    )
    standard_text_value = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="Standardized form of the TEXT_VALUE."
    )
    comments = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="Additional comments describing the parameter."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assays = rel(
        "Assays", back_populates="assay_parameters",
        doc="Relationship back to ``assays``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class AtcClassification(Base):
    """A representation of the ``atc_classification`` table.

    Parameters
    ----------
    who_name : `str`, optional, default: `NoneType`
        WHO/INN name for the compound.
    level1 : `str`, optional, default: `NoneType`
        First level of classification.
    level2 : `str`, optional, default: `NoneType`
        Second level of classification.
    level3 : `str`, optional, default: `NoneType`
        Third level of classification.
    level4 : `str`, optional, default: `NoneType`
        Fourth level of classification.
    level5 : `str`
        Complete ATC code for compound.
    level1_description : `str`, optional, default: `NoneType`
        Description of first level of classification.
    level2_description : `str`, optional, default: `NoneType`
        Description of second level of classification.
    level3_description : `str`, optional, default: `NoneType`
        Description of third level of classification.
    level4_description : `str`, optional, default: `NoneType`
        Description of fourth level of classification.
    defined_daily_dose : `chembl_orm.orm.DefinedDailyDose`, optional, \
    default: `NoneType`
        Relationship with ``defined_daily_dose``.
    molecule_atc_classification : `chembl_orm.orm.MoleculeAtcClassification`, \
    optional, default: `NoneType`
        Relationship with ``molecule_atc_classification``.

    Notes
    -----
    WHO ATC Classification for drugs.
    """
    __tablename__ = "atc_classification"

    who_name = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="WHO/INN name for the compound."
    )
    level1 = Column(
        String(10), index=False, unique=False, nullable=True,
        doc="First level of classification."
    )
    level2 = Column(
        String(10), index=False, unique=False, nullable=True,
        doc="Second level of classification."
    )
    level3 = Column(
        String(10), index=False, unique=False, nullable=True,
        doc="Third level of classification."
    )
    level4 = Column(
        String(10), index=False, unique=False, nullable=True,
        doc="Fourth level of classification."
    )
    level5 = Column(
        String(10), Sequence("level5_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Complete ATC code for compound."
    )
    level1_description = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Description of first level of classification."
    )
    level2_description = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Description of second level of classification."
    )
    level3_description = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Description of third level of classification."
    )
    level4_description = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Description of fourth level of classification."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    defined_daily_dose = rel(
        "DefinedDailyDose", back_populates="atc_classification",
        doc="Relationship back to ``defined_daily_dose``."
    )
    molecule_atc_classification = rel(
        "MoleculeAtcClassification", back_populates="atc_classification",
        doc="Relationship back to ``molecule_atc_classification``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BindingSites(Base):
    """A representation of the ``binding_sites`` table.

    Parameters
    ----------
    site_id : `int`
        Primary key. Unique identifier for a binding site in a given target.
    site_name : `str`, optional, default: `NoneType`
        Name/label for the binding site.
    tid : `int`, optional, default: `NoneType`
        Foreign key to target_dictionary. Target on which the binding site is
        found. Foreign key to ``target_dictionary.tid``.
    target_dictionary : `chembl_orm.orm.TargetDictionary`, optional, default: \
    `NoneType`
        Relationship with ``target_dictionary``.
    drug_mechanism : `chembl_orm.orm.DrugMechanism`, optional, default: \
    `NoneType`
        Relationship with ``drug_mechanism``.
    predicted_binding_domains : `chembl_orm.orm.PredictedBindingDomains`, \
    optional, default: `NoneType`
        Relationship with ``predicted_binding_domains``.
    site_components : `chembl_orm.orm.SiteComponents`, optional, default: \
    `NoneType`
        Relationship with ``site_components``.

    Notes
    -----
    Table storing details of binding sites for a target. A target may have
    multiple sites defined in this table.
    """
    __tablename__ = "binding_sites"

    site_id = Column(
        Integer, Sequence("site_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key. Unique identifier for a binding site in a given "
        "target."
    )
    site_name = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Name/label for the binding site."
    )
    tid = Column(
        Integer, ForeignKey("target_dictionary.tid"), index=True,
        unique=False, nullable=True,
        doc="Foreign key to target_dictionary. Target on which the binding "
        "site is found."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    target_dictionary = rel(
        "TargetDictionary", back_populates="binding_sites",
        doc="Relationship back to ``target_dictionary``"
    )
    drug_mechanism = rel(
        "DrugMechanism", back_populates="binding_sites",
        doc="Relationship back to ``drug_mechanism``."
    )
    predicted_binding_domains = rel(
        "PredictedBindingDomains", back_populates="binding_sites",
        doc="Relationship back to ``predicted_binding_domains``."
    )
    site_components = rel(
        "SiteComponents", back_populates="binding_sites",
        doc="Relationship back to ``site_components``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BioComponentSequences(Base):
    """A representation of the ``bio_component_sequences`` table.

    Parameters
    ----------
    component_id : `int`
        Primary key. Unique identifier for each of the molecular components of
        biotherapeutics in ChEMBL (e.g., antibody chains, recombinant proteins,
        synthetic peptides).
    component_type : `str`
        Type of molecular component (e.g., 'PROTEIN','DNA','RNA').
    description : `str`, optional, default: `NoneType`
        Description/name of molecular component.
    sequence : `str`, optional, default: `NoneType`
        Sequence of the biotherapeutic component.
    sequence_md5sum : `str`, optional, default: `NoneType`
        MD5 checksum of the sequence.
    tax_id : `int`, optional, default: `NoneType`
        NCBI tax ID for the species from which the sequence is derived. May be
        null for humanized monoclonal antibodies, synthetic peptides etc.
    organism : `str`, optional, default: `NoneType`
        Name of the species from which the sequence is derived.
    biotherapeutic_components : `chembl_orm.orm.BiotherapeuticComponents`, \
    optional, default: `NoneType`
        Relationship with ``biotherapeutic_components``.

    Notes
    -----
    Table storing the sequences for biotherapeutic drugs (e.g., monoclonal
    antibodies, recombinant proteins, peptides etc. For multi-chain
    biotherapeutics (e.g., mAbs) each chain is stored here as a separate
    component.
    """
    __tablename__ = "bio_component_sequences"

    component_id = Column(
        Integer, Sequence("component_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key. Unique identifier for each of the molecular "
        "components of biotherapeutics in ChEMBL (e.g., antibody chains, "
        "recombinant proteins, synthetic peptides)."
    )
    component_type = Column(
        String(50), index=False, unique=False, nullable=False,
        doc="Type of molecular component (e.g., 'PROTEIN','DNA','RNA')."
    )
    description = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Description/name of molecular component."
    )
    sequence = Column(
        Text, index=False, unique=False, nullable=True,
        doc="Sequence of the biotherapeutic component."
    )
    sequence_md5sum = Column(
        String(32), index=False, unique=False, nullable=True,
        doc="MD5 checksum of the sequence."
    )
    tax_id = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="NCBI tax ID for the species from which the sequence is derived. "
        "May be null for humanized monoclonal antibodies, synthetic peptides "
        "etc."
    )
    organism = Column(
        String(150), index=False, unique=False, nullable=True,
        doc="Name of the species from which the sequence is derived."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    biotherapeutic_components = rel(
        "BiotherapeuticComponents", back_populates="bio_component_sequences",
        doc="Relationship back to ``biotherapeutic_components``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BiotherapeuticComponents(Base):
    """A representation of the ``biotherapeutic_components`` table.

    Parameters
    ----------
    biocomp_id : `int`
        Primary key.
    molregno : `int`
        Foreign key to the biotherapeutics table, indicating which
        biotherapeutic the component is part of. This is indexed. This is
        unique. Foreign key to ``biotherapeutics.molregno``.
    component_id : `int`
        Foreign key to the bio_component_sequences table, indicating which
        component is part of the biotherapeutic. This is indexed. This is
        unique. Foreign key to ``bio_component_sequences.component_id``.
    biotherapeutics : `chembl_orm.orm.Biotherapeutics`, optional, default: \
    `NoneType`
        Relationship with ``biotherapeutics``.
    bio_component_sequences : `chembl_orm.orm.BioComponentSequences`, \
    optional, default: `NoneType`
        Relationship with ``bio_component_sequences``.

    Notes
    -----
    Links each biotherapeutic drug (in the biotherapeutics table) to its
    component sequences (in the bio_component_sequences table). A
    biotherapeutic drug can have multiple components and hence multiple rows in
    this table. Similarly, a particular component sequence can be part of more
    than one drug.
    """
    __tablename__ = "biotherapeutic_components"

    biocomp_id = Column(
        Integer, Sequence("biocomp_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    molregno = Column(
        Integer, ForeignKey("biotherapeutics.molregno"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to the biotherapeutics table, indicating which "
        "biotherapeutic the component is part of."
    )
    component_id = Column(
        Integer, ForeignKey("bio_component_sequences.component_id"),
        index=True, unique=True, nullable=False,
        doc="Foreign key to the bio_component_sequences table, indicating "
        "which component is part of the biotherapeutic."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    biotherapeutics = rel(
        "Biotherapeutics", back_populates="biotherapeutic_components",
        doc="Relationship back to ``biotherapeutics``"
    )
    bio_component_sequences = rel(
        "BioComponentSequences", back_populates="biotherapeutic_components",
        doc="Relationship back to ``bio_component_sequences``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Biotherapeutics(Base):
    """A representation of the ``biotherapeutics`` table.

    Parameters
    ----------
    molregno : `int`
        Foreign key to molecule_dictionary. Foreign key to
        ``molecule_dictionary.molregno``.
    description : `str`, optional, default: `NoneType`
        Description of the biotherapeutic.
    helm_notation : `str`, optional, default: `NoneType`
        Sequence notation generated according to the HELM standard
        (http://www.openhelm.org/home). Currently for peptides only.
    biotherapeutic_components : `chembl_orm.orm.BiotherapeuticComponents`, \
    optional, default: `NoneType`
        Relationship with ``biotherapeutic_components``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.

    Notes
    -----
    Stores sequence information/descriptions for protein therapeutics,
    including recombinant proteins, peptides and antibodies.
    """
    __tablename__ = "biotherapeutics"

    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"),
        Sequence("molregno_seq"), index=True, unique=False, nullable=False,
        primary_key=True, doc="Foreign key to molecule_dictionary."
    )
    description = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Description of the biotherapeutic."
    )
    helm_notation = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="Sequence notation generated according to the HELM standard "
        "(http://www.openhelm.org/home). Currently for peptides only."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    biotherapeutic_components = rel(
        "BiotherapeuticComponents", back_populates="biotherapeutics",
        doc="Relationship back to ``biotherapeutic_components``."
    )
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="biotherapeutics",
        doc="Relationship back to ``molecule_dictionary``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ComponentClass(Base):
    """A representation of the ``component_class`` table.

    Parameters
    ----------
    component_id : `int`
        Foreign key to component_sequences table. This is indexed. This is
        unique. Foreign key to ``component_sequences.component_id``.
    protein_class_id : `int`
        Foreign key to the protein_classification table. This is indexed. This
        is unique. Foreign key to ``protein_classification.protein_class_id``.
    comp_class_id : `int`
        Primary key.
    component_sequences : `chembl_orm.orm.ComponentSequences`, optional, \
    default: `NoneType`
        Relationship with ``component_sequences``.
    protein_classification : `chembl_orm.orm.ProteinClassification`, \
    optional, default: `NoneType`
        Relationship with ``protein_classification``.

    Notes
    -----
    Links protein components of targets to the protein_family_classification
    table. A protein can have more than one classification (e.g., Membrane
    receptor and Enzyme).
    """
    __tablename__ = "component_class"

    component_id = Column(
        Integer, ForeignKey("component_sequences.component_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to component_sequences table."
    )
    protein_class_id = Column(
        Integer, ForeignKey("protein_classification.protein_class_id"),
        index=True, unique=True, nullable=False,
        doc="Foreign key to the protein_classification table."
    )
    comp_class_id = Column(
        Integer, Sequence("comp_class_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    component_sequences = rel(
        "ComponentSequences", back_populates="component_class",
        doc="Relationship back to ``component_sequences``"
    )
    protein_classification = rel(
        "ProteinClassification", back_populates="component_class",
        doc="Relationship back to ``protein_classification``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ComponentSequences(Base):
    """A representation of the ``component_sequences`` table.

    Parameters
    ----------
    component_id : `int`
        Primary key. Unique identifier for the component.
    component_type : `str`, optional, default: `NoneType`
        Type of molecular component represented (e.g., 'PROTEIN','DNA','RNA').
    accession : `str`, optional, default: `NoneType`
        Accession for the sequence in the source database from which it was
        taken (e.g., UniProt accession for proteins). This is indexed. This is
        unique.
    sequence : `str`, optional, default: `NoneType`
        A representative sequence for the molecular component, as given in the
        source sequence database (not necessarily the exact sequence used in
        the assay).
    sequence_md5sum : `str`, optional, default: `NoneType`
        MD5 checksum of the sequence.
    description : `str`, optional, default: `NoneType`
        Description/name for the molecular component, usually taken from the
        source sequence database.
    tax_id : `int`, optional, default: `NoneType`
        NCBI tax ID for the sequence in the source database (i.e., species that
        the protein/nucleic acid sequence comes from).
    organism : `str`, optional, default: `NoneType`
        Name of the organism the sequence comes from.
    db_source : `str`, optional, default: `NoneType`
        The name of the source sequence database from which
        sequences/accessions are taken. For UniProt proteins, this field
        indicates whether the sequence is from SWISS-PROT or TREMBL.
    db_version : `str`, optional, default: `NoneType`
        The version of the source sequence database from which
        sequences/accession were last updated.
    component_class : `chembl_orm.orm.ComponentClass`, optional, default: \
    `NoneType`
        Relationship with ``component_class``.
    component_domains : `chembl_orm.orm.ComponentDomains`, optional, default: \
    `NoneType`
        Relationship with ``component_domains``.
    component_go : `chembl_orm.orm.ComponentGo`, optional, default: \
    `NoneType`
        Relationship with ``component_go``.
    component_synonyms : `chembl_orm.orm.ComponentSynonyms`, optional, \
    default: `NoneType`
        Relationship with ``component_synonyms``.
    site_components : `chembl_orm.orm.SiteComponents`, optional, default: \
    `NoneType`
        Relationship with ``site_components``.
    target_components : `chembl_orm.orm.TargetComponents`, optional, default: \
    `NoneType`
        Relationship with ``target_components``.

    Notes
    -----
    Table storing the sequences for components of molecular targets (e.g.,
    protein sequences), along with other details taken from sequence databases
    (e.g., names, accessions). Single protein targets will have a single
    protein component in this table, whereas protein complexes/protein families
    will have multiple protein components.
    """
    __tablename__ = "component_sequences"

    component_id = Column(
        Integer, Sequence("component_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key. Unique identifier for the component."
    )
    component_type = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Type of molecular component represented (e.g., "
        "'PROTEIN','DNA','RNA')."
    )
    accession = Column(
        String(25), index=True, unique=True, nullable=True,
        doc="Accession for the sequence in the source database from which it "
        "was taken (e.g., UniProt accession for proteins)."
    )
    sequence = Column(
        Text, index=False, unique=False, nullable=True,
        doc="A representative sequence for the molecular component, as given "
        "in the source sequence database (not necessarily the exact sequence "
        "used in the assay)."
    )
    sequence_md5sum = Column(
        String(32), index=False, unique=False, nullable=True,
        doc="MD5 checksum of the sequence."
    )
    description = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Description/name for the molecular component, usually taken from"
        " the source sequence database."
    )
    tax_id = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="NCBI tax ID for the sequence in the source database (i.e., "
        "species that the protein/nucleic acid sequence comes from)."
    )
    organism = Column(
        String(150), index=False, unique=False, nullable=True,
        doc="Name of the organism the sequence comes from."
    )
    db_source = Column(
        String(25), index=False, unique=False, nullable=True,
        doc="The name of the source sequence database from which "
        "sequences/accessions are taken. For UniProt proteins, this field "
        "indicates whether the sequence is from SWISS-PROT or TREMBL."
    )
    db_version = Column(
        String(10), index=False, unique=False, nullable=True,
        doc="The version of the source sequence database from which "
        "sequences/accession were last updated."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    component_class = rel(
        "ComponentClass", back_populates="component_sequences",
        doc="Relationship back to ``component_class``."
    )
    component_domains = rel(
        "ComponentDomains", back_populates="component_sequences",
        doc="Relationship back to ``component_domains``."
    )
    component_go = rel(
        "ComponentGo", back_populates="component_sequences",
        doc="Relationship back to ``component_go``."
    )
    component_synonyms = rel(
        "ComponentSynonyms", back_populates="component_sequences",
        doc="Relationship back to ``component_synonyms``."
    )
    site_components = rel(
        "SiteComponents", back_populates="component_sequences",
        doc="Relationship back to ``site_components``."
    )
    target_components = rel(
        "TargetComponents", back_populates="component_sequences",
        doc="Relationship back to ``target_components``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ProteinClassification(Base):
    """A representation of the ``protein_classification`` table.

    Parameters
    ----------
    protein_class_id : `int`
        Primary key. Unique identifier for each protein family classification.
    parent_id : `int`, optional, default: `NoneType`
        Protein_class_id for the parent of this protein family.
    pref_name : `str`, optional, default: `NoneType`
        Preferred/full name for this protein family.
    short_name : `str`, optional, default: `NoneType`
        Short/abbreviated name for this protein family (not necessarily
        unique).
    protein_class_desc : `str`
        Concatenated description of each classification for searching purposes
        etc.
    definition : `str`, optional, default: `NoneType`
        Definition of the protein family.
    class_level : `int`
        Level of the class within the hierarchy (level 1 = top level
        classification).
    component_class : `chembl_orm.orm.ComponentClass`, optional, default: \
    `NoneType`
        Relationship with ``component_class``.
    protein_class_synonyms : `chembl_orm.orm.ProteinClassSynonyms`, optional, \
    default: `NoneType`
        Relationship with ``protein_class_synonyms``.

    Notes
    -----
    Table storing the protein family classifications for protein targets in
    ChEMBL (formerly in the target_class table).
    """
    __tablename__ = "protein_classification"

    protein_class_id = Column(
        Integer, Sequence("protein_class_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key. Unique identifier for each protein family "
        "classification."
    )
    parent_id = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Protein_class_id for the parent of this protein family."
    )
    pref_name = Column(
        String(500), index=False, unique=False, nullable=True,
        doc="Preferred/full name for this protein family."
    )
    short_name = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Short/abbreviated name for this protein family (not necessarily "
        "unique)."
    )
    protein_class_desc = Column(
        String(410), index=False, unique=False, nullable=False,
        doc="Concatenated description of each classification for searching "
        "purposes etc."
    )
    definition = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="Definition of the protein family."
    )
    class_level = Column(
        Integer, index=False, unique=False, nullable=False,
        doc="Level of the class within the hierarchy (level 1 = top level "
        "classification)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    component_class = rel(
        "ComponentClass", back_populates="protein_classification",
        doc="Relationship back to ``component_class``."
    )
    protein_class_synonyms = rel(
        "ProteinClassSynonyms", back_populates="protein_classification",
        doc="Relationship back to ``protein_class_synonyms``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ComponentDomains(Base):
    """A representation of the ``component_domains`` table.

    Parameters
    ----------
    compd_id : `int`
        Primary key.
    domain_id : `int`, optional, default: `NoneType`
        Foreign key to the domains table, indicating the domain that is
        contained in the associated molecular component. This is indexed. This
        is unique. Foreign key to ``domains.domain_id``.
    component_id : `int`
        Foreign key to the component_sequences table, indicating the
        molecular_component that has the given domain. This is indexed. This is
        unique. Foreign key to ``component_sequences.component_id``.
    start_position : `int`, optional, default: `NoneType`
        Start position of the domain within the sequence given in the
        component_sequences table. This is indexed. This is unique.
    end_position : `int`, optional, default: `NoneType`
        End position of the domain within the sequence given in the
        component_sequences table.
    domains : `chembl_orm.orm.Domains`, optional, default: `NoneType`
        Relationship with ``domains``.
    component_sequences : `chembl_orm.orm.ComponentSequences`, optional, \
    default: `NoneType`
        Relationship with ``component_sequences``.

    Notes
    -----
    Links protein components of targets to the structural domains they contain
    (from the domains table). Contains information showing the start and end
    position of the domain in the component sequence.
    """
    __tablename__ = "component_domains"

    compd_id = Column(
        Integer, Sequence("compd_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    domain_id = Column(
        Integer, ForeignKey("domains.domain_id"), index=True, unique=True,
        nullable=True,
        doc="Foreign key to the domains table, indicating the domain that is "
        "contained in the associated molecular component."
    )
    component_id = Column(
        Integer, ForeignKey("component_sequences.component_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to the component_sequences table, indicating the "
        "molecular_component that has the given domain."
    )
    start_position = Column(
        Integer, index=True, unique=True, nullable=True,
        doc="Start position of the domain within the sequence given in the "
        "component_sequences table."
    )
    end_position = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="End position of the domain within the sequence given in the "
        "component_sequences table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    domains = rel(
        "Domains", back_populates="component_domains",
        doc="Relationship back to ``domains``"
    )
    component_sequences = rel(
        "ComponentSequences", back_populates="component_domains",
        doc="Relationship back to ``component_sequences``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Domains(Base):
    """A representation of the ``domains`` table.

    Parameters
    ----------
    domain_id : `int`
        Primary key. Unique identifier for each domain.
    domain_type : `str`
        Indicates the source of the domain (e.g., Pfam).
    source_domain_id : `str`
        Identifier for the domain in the source database (e.g., Pfam ID such as
        PF00001).
    domain_name : `str`, optional, default: `NoneType`
        Name given to the domain in the source database (e.g., 7tm_1).
    domain_description : `str`, optional, default: `NoneType`
        Longer name or description for the domain.
    component_domains : `chembl_orm.orm.ComponentDomains`, optional, default: \
    `NoneType`
        Relationship with ``component_domains``.
    site_components : `chembl_orm.orm.SiteComponents`, optional, default: \
    `NoneType`
        Relationship with ``site_components``.

    Notes
    -----
    Table storing a non-redundant list of domains found in protein targets
    (e.g., Pfam domains).
    """
    __tablename__ = "domains"

    domain_id = Column(
        Integer, Sequence("domain_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key. Unique identifier for each domain."
    )
    domain_type = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="Indicates the source of the domain (e.g., Pfam)."
    )
    source_domain_id = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="Identifier for the domain in the source database (e.g., Pfam ID "
        "such as PF00001)."
    )
    domain_name = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="Name given to the domain in the source database (e.g., 7tm_1)."
    )
    domain_description = Column(
        String(500), index=False, unique=False, nullable=True,
        doc="Longer name or description for the domain."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    component_domains = rel(
        "ComponentDomains", back_populates="domains",
        doc="Relationship back to ``component_domains``."
    )
    site_components = rel(
        "SiteComponents", back_populates="domains",
        doc="Relationship back to ``site_components``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ComponentGo(Base):
    """A representation of the ``component_go`` table.

    Parameters
    ----------
    comp_go_id : `int`
        Primary key.
    component_id : `int`
        Foreign key to COMPONENT_SEQUENCES table. The protein component this GO
        term applies to. This is indexed. This is unique. Foreign key to
        ``component_sequences.component_id``.
    go_id : `str`
        Foreign key to the GO_CLASSIFICATION table. The GO term that this
        protein is mapped to. This is indexed. This is unique. Foreign key to
        ``go_classification.go_id``.
    component_sequences : `chembl_orm.orm.ComponentSequences`, optional, \
    default: `NoneType`
        Relationship with ``component_sequences``.
    go_classification : `chembl_orm.orm.GoClassification`, optional, default: \
    `NoneType`
        Relationship with ``go_classification``.

    Notes
    -----
    Table mapping protein components in the COMPONENT_SEQUENCES table to the GO
    slim terms stored in the GO_CLASSIFICATION table.
    """
    __tablename__ = "component_go"

    comp_go_id = Column(
        Integer, Sequence("comp_go_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    component_id = Column(
        Integer, ForeignKey("component_sequences.component_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to COMPONENT_SEQUENCES table. The protein component "
        "this GO term applies to."
    )
    go_id = Column(
        String(10), ForeignKey("go_classification.go_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to the GO_CLASSIFICATION table. The GO term that "
        "this protein is mapped to."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    component_sequences = rel(
        "ComponentSequences", back_populates="component_go",
        doc="Relationship back to ``component_sequences``"
    )
    go_classification = rel(
        "GoClassification", back_populates="component_go",
        doc="Relationship back to ``go_classification``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GoClassification(Base):
    """A representation of the ``go_classification`` table.

    Parameters
    ----------
    go_id : `str`
        Primary key. Gene Ontology identifier for the GO slim term.
    parent_go_id : `str`, optional, default: `NoneType`
        Gene Ontology identifier for the parent of this GO term in the ChEMBL
        Drug Target GO slim.
    pref_name : `str`, optional, default: `NoneType`
        Gene Ontology name.
    class_level : `int`, optional, default: `NoneType`
        Indicates the level of the term in the slim (L1 = highest).
    aspect : `str`, optional, default: `NoneType`
        Indicates which aspect of the Gene Ontology the term belongs to (F =
        molecular function, P = biological process, C = cellular component).
    path : `str`, optional, default: `NoneType`
        Indicates the full path to this term in the GO slim.
    component_go : `chembl_orm.orm.ComponentGo`, optional, default: \
    `NoneType`
        Relationship with ``component_go``.

    Notes
    -----
    Table storing the ChEMBL Drug Target GO slim
    (http://www.geneontology.org/ontology/subsets/goslim_chembl.obo).
    """
    __tablename__ = "go_classification"

    go_id = Column(
        String(10), Sequence("go_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key. Gene Ontology identifier for the GO slim term."
    )
    parent_go_id = Column(
        String(10), index=False, unique=False, nullable=True,
        doc="Gene Ontology identifier for the parent of this GO term in the "
        "ChEMBL Drug Target GO slim."
    )
    pref_name = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Gene Ontology name."
    )
    class_level = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Indicates the level of the term in the slim (L1 = highest)."
    )
    aspect = Column(
        String(1), index=False, unique=False, nullable=True,
        doc="Indicates which aspect of the Gene Ontology the term belongs to "
        "(F = molecular function, P = biological process, C = cellular "
        "component)."
    )
    path = Column(
        String(1000), index=False, unique=False, nullable=True,
        doc="Indicates the full path to this term in the GO slim."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    component_go = rel(
        "ComponentGo", back_populates="go_classification",
        doc="Relationship back to ``component_go``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ComponentSynonyms(Base):
    """A representation of the ``component_synonyms`` table.

    Parameters
    ----------
    compsyn_id : `int`
        Primary key.
    component_id : `int`
        Foreign key to the component_sequences table. The component to which
        this synonym applies. This is indexed. This is unique. Foreign key to
        ``component_sequences.component_id``.
    component_synonym : `str`, optional, default: `NoneType`
        The synonym for the component. This is indexed. This is unique.
    syn_type : `str`, optional, default: `NoneType`
        The type or origin of the synonym (e.g., GENE_SYMBOL). This is indexed.
        This is unique.
    component_sequences : `chembl_orm.orm.ComponentSequences`, optional, \
    default: `NoneType`
        Relationship with ``component_sequences``.

    Notes
    -----
    Table storing synonyms for the components of molecular targets (e.g.,
    names, acronyms, gene symbols etc.) Please note: EC numbers are also
    currently included in this table although they are not strictly synonyms
    and can apply to multiple proteins.
    """
    __tablename__ = "component_synonyms"

    compsyn_id = Column(
        Integer, Sequence("compsyn_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    component_id = Column(
        Integer, ForeignKey("component_sequences.component_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to the component_sequences table. The component to "
        "which this synonym applies."
    )
    component_synonym = Column(
        String(500), index=True, unique=True, nullable=True,
        doc="The synonym for the component."
    )
    syn_type = Column(
        String(20), index=True, unique=True, nullable=True,
        doc="The type or origin of the synonym (e.g., GENE_SYMBOL)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    component_sequences = rel(
        "ComponentSequences", back_populates="component_synonyms",
        doc="Relationship back to ``component_sequences``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CompoundProperties(Base):
    """A representation of the ``compound_properties`` table.

    Parameters
    ----------
    molregno : `int`
        Foreign key to compounds table (compound structure). Foreign key to
        ``molecule_dictionary.molregno``.
    mw_freebase : `float`, optional, default: `NoneType`
        Molecular weight of parent compound.
    alogp : `float`, optional, default: `NoneType`
        Calculated ALogP.
    hba : `int`, optional, default: `NoneType`
        Number hydrogen bond acceptors.
    hbd : `int`, optional, default: `NoneType`
        Number hydrogen bond donors.
    psa : `float`, optional, default: `NoneType`
        Polar surface area.
    rtb : `float`, optional, default: `NoneType`
        Number rotatable bonds.
    ro3_pass : `str`, optional, default: `NoneType`
        Indicates whether the compound passes the rule-of-three (mw < 300, logP
        < 3 etc).
    num_ro5_violations : `int`, optional, default: `NoneType`
        Number of violations of Lipinski's rule-of-five, using HBA and HBD
        definitions.
    cx_most_apka : `float`, optional, default: `NoneType`
        The most acidic pKa calculated using ChemAxon v17.29.0.
    cx_most_bpka : `float`, optional, default: `NoneType`
        The most basic pKa calculated using ChemAxon v17.29.0.
    cx_logp : `float`, optional, default: `NoneType`
        The calculated octanol/water partition coefficient using ChemAxon
        v17.29.0.
    cx_logd : `float`, optional, default: `NoneType`
        The calculated octanol/water distribution coefficient at pH7.4 using
        ChemAxon v17.29.0.
    molecular_species : `str`, optional, default: `NoneType`
        Indicates whether the compound is an acid/base/neutral.
    full_mwt : `float`, optional, default: `NoneType`
        Molecular weight of the full compound including any salts.
    aromatic_rings : `int`, optional, default: `NoneType`
        Number of aromatic rings.
    heavy_atoms : `int`, optional, default: `NoneType`
        Number of heavy (non-hydrogen) atoms.
    qed_weighted : `float`, optional, default: `NoneType`
        Weighted quantitative estimate of drug likeness (as defined by
        Bickerton et al., Nature Chem 2012).
    mw_monoisotopic : `float`, optional, default: `NoneType`
        Monoisotopic parent molecular weight.
    full_molformula : `str`, optional, default: `NoneType`
        Molecular formula for the full compound (including any salt).
    hba_lipinski : `int`, optional, default: `NoneType`
        Number of hydrogen bond acceptors calculated according to Lipinski's
        original rules (i.e., N + O count)).
    hbd_lipinski : `int`, optional, default: `NoneType`
        Number of hydrogen bond donors calculated according to Lipinski's
        original rules (i.e., NH + OH count).
    num_lipinski_ro5_violations : `int`, optional, default: `NoneType`
        Number of violations of Lipinski's rule of five using HBA_LIPINSKI and
        HBD_LIPINSKI counts.
    np_likeness_score : `float`, optional, default: `NoneType`
        Natural Product-likeness Score: Peter Ertl, Silvio Roggo, and Ansgar
        Schuffenhauer Journal of Chemical Information and Modeling, 48, 68-74
        (2008).
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.

    Notes
    -----
    Table storing calculated physicochemical properties for compounds, now
    calculated with RDKit and ChemAxon software (note all but FULL_MWT and
    FULL_MOLFORMULA are calculated on the parent structure).
    """
    __tablename__ = "compound_properties"

    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"),
        Sequence("molregno_seq"), index=True, unique=False, nullable=False,
        primary_key=True,
        doc="Foreign key to compounds table (compound structure)."
    )
    mw_freebase = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Molecular weight of parent compound."
    )
    alogp = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Calculated ALogP."
    )
    hba = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Number hydrogen bond acceptors."
    )
    hbd = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Number hydrogen bond donors."
    )
    psa = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Polar surface area."
    )
    rtb = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Number rotatable bonds."
    )
    ro3_pass = Column(
        String(3), index=False, unique=False, nullable=True,
        doc="Indicates whether the compound passes the rule-of-three (mw < "
        "300, logP < 3 etc)."
    )
    num_ro5_violations = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Number of violations of Lipinski's rule-of-five, using HBA and "
        "HBD definitions."
    )
    cx_most_apka = Column(
        Float, index=False, unique=False, nullable=True,
        doc="The most acidic pKa calculated using ChemAxon v17.29.0."
    )
    cx_most_bpka = Column(
        Float, index=False, unique=False, nullable=True,
        doc="The most basic pKa calculated using ChemAxon v17.29.0."
    )
    cx_logp = Column(
        Float, index=False, unique=False, nullable=True,
        doc="The calculated octanol/water partition coefficient using "
        "ChemAxon v17.29.0."
    )
    cx_logd = Column(
        Float, index=False, unique=False, nullable=True,
        doc="The calculated octanol/water distribution coefficient at pH7.4 "
        "using ChemAxon v17.29.0."
    )
    molecular_species = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Indicates whether the compound is an acid/base/neutral."
    )
    full_mwt = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Molecular weight of the full compound including any salts."
    )
    aromatic_rings = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Number of aromatic rings."
    )
    heavy_atoms = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Number of heavy (non-hydrogen) atoms."
    )
    qed_weighted = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Weighted quantitative estimate of drug likeness (as defined by "
        "Bickerton et al., Nature Chem 2012)."
    )
    mw_monoisotopic = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Monoisotopic parent molecular weight."
    )
    full_molformula = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Molecular formula for the full compound (including any salt)."
    )
    hba_lipinski = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Number of hydrogen bond acceptors calculated according to "
        "Lipinski's original rules (i.e., N + O count))."
    )
    hbd_lipinski = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Number of hydrogen bond donors calculated according to "
        "Lipinski's original rules (i.e., NH + OH count)."
    )
    num_lipinski_ro5_violations = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Number of violations of Lipinski's rule of five using "
        "HBA_LIPINSKI and HBD_LIPINSKI counts."
    )
    np_likeness_score = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Natural Product-likeness Score: Peter Ertl, Silvio Roggo, and "
        "Ansgar Schuffenhauer Journal of Chemical Information and Modeling, "
        "48, 68-74 (2008)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="compound_properties",
        doc="Relationship back to ``molecule_dictionary``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CompoundStructuralAlerts(Base):
    """A representation of the ``compound_structural_alerts`` table.

    Parameters
    ----------
    cpd_str_alert_id : `int`
        Primary key.
    molregno : `int`
        Foreign key to the molecule_dictionary. The compound for which the
        structural alert has been found. This is indexed. This is unique.
        Foreign key to ``molecule_dictionary.molregno``.
    alert_id : `int`
        Foreign key to the structural_alerts table. The particular alert that
        has been identified in this compound. This is indexed. This is unique.
        Foreign key to ``structural_alerts.alert_id``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.
    structural_alerts : `chembl_orm.orm.StructuralAlerts`, optional, default: \
    `NoneType`
        Relationship with ``structural_alerts``.

    Notes
    -----
    Table showing which structural alerts (as defined in the STRUCTURAL_ALERTS
    table) are found in a particular ChEMBL compound. It should be noted some
    alerts/alert sets are more permissive than others and may flag a large
    number of compounds. Results should be interpreted with care, depending on
    the use-case, and not treated as a blanket filter (e.g., around 50% of
    approved drugs have 1 or more alerts from these sets).
    """
    __tablename__ = "compound_structural_alerts"

    cpd_str_alert_id = Column(
        Integer, Sequence("cpd_str_alert_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to the molecule_dictionary. The compound for which "
        "the structural alert has been found."
    )
    alert_id = Column(
        Integer, ForeignKey("structural_alerts.alert_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to the structural_alerts table. The particular alert"
        " that has been identified in this compound."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="compound_structural_alerts",
        doc="Relationship back to ``molecule_dictionary``"
    )
    structural_alerts = rel(
        "StructuralAlerts", back_populates="compound_structural_alerts",
        doc="Relationship back to ``structural_alerts``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class StructuralAlerts(Base):
    """A representation of the ``structural_alerts`` table.

    Parameters
    ----------
    alert_id : `int`
        Primary key. Unique identifier for the structural alert.
    alert_set_id : `int`
        Foreign key to structural_alert_sets table indicating which set this
        particular alert comes from. This is indexed. This is unique. Foreign
        key to ``structural_alert_sets.alert_set_id``.
    alert_name : `str`
        A name for the structural alert. This is indexed. This is unique.
    smarts : `str`
        SMARTS defining the structural feature that is considered to be an
        alert. This is indexed. This is unique.
    compound_structural_alerts : `chembl_orm.orm.CompoundStructuralAlerts`, \
    optional, default: `NoneType`
        Relationship with ``compound_structural_alerts``.
    structural_alert_sets : `chembl_orm.orm.StructuralAlertSets`, optional, \
    default: `NoneType`
        Relationship with ``structural_alert_sets``.

    Notes
    -----
    Table storing a list of structural features (encoded as SMARTS) that are
    potentially undesirable in drug discovery context. It should be noted some
    alerts/alert sets are more permissive than others and may flag a large
    number of compounds. Results should be interpreted with care, depending on
    the use-case, and not treated as a blanket filter (e.g., around 50% of
    approved drugs have 1 or more alerts from these sets).
    """
    __tablename__ = "structural_alerts"

    alert_id = Column(
        Integer, Sequence("alert_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key. Unique identifier for the structural alert."
    )
    alert_set_id = Column(
        Integer, ForeignKey("structural_alert_sets.alert_set_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to structural_alert_sets table indicating which set "
        "this particular alert comes from."
    )
    alert_name = Column(
        String(100), index=True, unique=True, nullable=False,
        doc="A name for the structural alert."
    )
    smarts = Column(
        String(4000), index=True, unique=True, nullable=False,
        doc="SMARTS defining the structural feature that is considered to be "
        "an alert."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    compound_structural_alerts = rel(
        "CompoundStructuralAlerts", back_populates="structural_alerts",
        doc="Relationship back to ``compound_structural_alerts``."
    )
    structural_alert_sets = rel(
        "StructuralAlertSets", back_populates="structural_alerts",
        doc="Relationship back to ``structural_alert_sets``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class StructuralAlertSets(Base):
    """A representation of the ``structural_alert_sets`` table.

    Parameters
    ----------
    alert_set_id : `int`
        Unique ID for the structural alert set.
    set_name : `str`
        Name (or origin) of the structural alert set. This is indexed. This is
        unique.
    priority : `int`
        Priority assigned to the structural alert set for display on the ChEMBL
        interface (priorities >=4 are shown by default).
    structural_alerts : `chembl_orm.orm.StructuralAlerts`, optional, default: \
    `NoneType`
        Relationship with ``structural_alerts``.

    Notes
    -----
    Table showing list of sets of structural alerts that have been included in
    COMPOUND_STRUCTURAL_ALERT table. It should be noted some alerts/alert sets
    are more permissive than others and may flag a large number of compounds.
    Results should be interpreted with care, depending on the use-case, and not
    treated as a blanket filter (e.g., around 50% of approved drugs have 1 or
    more alerts from these sets).
    """
    __tablename__ = "structural_alert_sets"

    alert_set_id = Column(
        Integer, Sequence("alert_set_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Unique ID for the structural alert set."
    )
    set_name = Column(
        String(100), index=True, unique=True, nullable=False,
        doc="Name (or origin) of the structural alert set."
    )
    priority = Column(
        Integer, index=False, unique=False, nullable=False,
        doc="Priority assigned to the structural alert set for display on the"
        " ChEMBL interface (priorities >=4 are shown by default)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    structural_alerts = rel(
        "StructuralAlerts", back_populates="structural_alert_sets",
        doc="Relationship back to ``structural_alerts``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CompoundStructures(Base):
    """A representation of the ``compound_structures`` table.

    Parameters
    ----------
    molregno : `int`
        Internal Primary Key for the compound structure and foreign key to
        molecule_dictionary table. Foreign key to
        ``molecule_dictionary.molregno``.
    molfile : `str`, optional, default: `NoneType`
        MDL Connection table representation of compound.
    standard_inchi : `str`, optional, default: `NoneType`
        IUPAC standard InChI for the compound. This is indexed. This is unique.
    standard_inchi_key : `str`
        IUPAC standard InChI key for the compound. This is indexed. This is
        unique.
    canonical_smiles : `str`, optional, default: `NoneType`
        Canonical smiles, generated using RDKit.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.

    Notes
    -----
    Table storing various structure representations (e.g., Molfile, InChI) for
    each compound.
    """
    __tablename__ = "compound_structures"

    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"),
        Sequence("molregno_seq"), index=True, unique=False, nullable=False,
        primary_key=True,
        doc="Internal Primary Key for the compound structure and foreign key "
        "to molecule_dictionary table."
    )
    molfile = Column(
        Text, index=False, unique=False, nullable=True,
        doc="MDL Connection table representation of compound."
    )
    standard_inchi = Column(
        String(4000), index=True, unique=True, nullable=True,
        doc="IUPAC standard InChI for the compound."
    )
    standard_inchi_key = Column(
        String(27), index=True, unique=True, nullable=False,
        doc="IUPAC standard InChI key for the compound."
    )
    canonical_smiles = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="Canonical smiles, generated using RDKit."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="compound_structures",
        doc="Relationship back to ``molecule_dictionary``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DefinedDailyDose(Base):
    """A representation of the ``defined_daily_dose`` table.

    Parameters
    ----------
    atc_code : `str`
        ATC code for the compound (foreign key to ATC_CLASSIFICATION table).
        Foreign key to ``atc_classification.level5``.
    ddd_units : `str`, optional, default: `NoneType`
        Units of defined daily dose.
    ddd_admr : `str`, optional, default: `NoneType`
        Administration route for dose.
    ddd_comment : `str`, optional, default: `NoneType`
        Comment.
    ddd_id : `int`
        Internal primary key.
    ddd_value : `float`, optional, default: `NoneType`
        Value of defined daily dose.
    atc_classification : `chembl_orm.orm.AtcClassification`, optional, \
    default: `NoneType`
        Relationship with ``atc_classification``.

    Notes
    -----
    WHO DDD (defined daily dose) information.
    """
    __tablename__ = "defined_daily_dose"

    atc_code = Column(
        String(10), ForeignKey("atc_classification.level5"), index=True,
        unique=False, nullable=False,
        doc="ATC code for the compound (foreign key to ATC_CLASSIFICATION "
        "table)."
    )
    ddd_units = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Units of defined daily dose."
    )
    ddd_admr = Column(
        String(1000), index=False, unique=False, nullable=True,
        doc="Administration route for dose."
    )
    ddd_comment = Column(
        String(2000), index=False, unique=False, nullable=True, doc="Comment."
    )
    ddd_id = Column(
        Integer, Sequence("ddd_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Internal primary key."
    )
    ddd_value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Value of defined daily dose."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    atc_classification = rel(
        "AtcClassification", back_populates="defined_daily_dose",
        doc="Relationship back to ``atc_classification``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DrugIndication(Base):
    """A representation of the ``drug_indication`` table.

    Parameters
    ----------
    drugind_id : `int`
        Primary key.
    record_id : `int`
        Foreign key to compound_records table. Links to the drug record to
        which this indication applies. This is indexed. This is unique. Foreign
        key to ``compound_records.record_id``.
    molregno : `int`, optional, default: `NoneType`
        Molregno for the drug (foreign key to the molecule_dictionary and
        compound_records tables). Foreign key to
        ``molecule_dictionary.molregno``.
    max_phase_for_ind : `int`, optional, default: `NoneType`
        Maximum phase of development that the drug is known to have reached for
        this particular indication (4 = Approved, 3 = Phase 3 Clinical Trials,
        2 = Phase 2 Clinical Trials, 1 = Phase 1 Clinical Trials, 0.5 = Early
        Phase 1 Clinical Trials, -1 = Clinical Phase unknown for drug or
        clinical candidate drug ie where ChEMBL cannot assign a clinical
        phase).
    mesh_id : `str`
        Medical Subject Headings (MeSH) disease identifier corresponding to the
        indication. This is indexed. This is unique.
    mesh_heading : `str`
        Medical Subject Heading term for the MeSH disease ID.
    efo_id : `str`, optional, default: `NoneType`
        Experimental Factor Ontology (EFO) disease identifier corresponding to
        the indication. This is indexed. This is unique.
    efo_term : `str`, optional, default: `NoneType`
        Experimental Factor Ontology term for the EFO ID.
    compound_records : `chembl_orm.orm.CompoundRecords`, optional, default: \
    `NoneType`
        Relationship with ``compound_records``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.
    indication_refs : `chembl_orm.orm.IndicationRefs`, optional, default: \
    `NoneType`
        Relationship with ``indication_refs``.
    indication_index : `chembl_orm.orm.TermIndexMap`
        Relationship back to the ``term_index_map`` indication index table.

    Notes
    -----
    Table storing indications for drugs, and clinical candidate drugs, from a
    variety of sources (e.g., ATC, DailyMed, ClinicalTrials.gov).
    """
    __tablename__ = "drug_indication"

    drugind_id = Column(
        Integer, Sequence("drugind_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    record_id = Column(
        Integer, ForeignKey("compound_records.record_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to compound_records table. Links to the drug record "
        "to which this indication applies."
    )
    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=False, nullable=True,
        doc="Molregno for the drug (foreign key to the molecule_dictionary "
        "and compound_records tables)."
    )
    max_phase_for_ind = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Maximum phase of development that the drug is known to have "
        "reached for this particular indication (4 = Approved, 3 = Phase 3 "
        "Clinical Trials, 2 = Phase 2 Clinical Trials, 1 = Phase 1 Clinical "
        "Trials, 0.5 = Early Phase 1 Clinical Trials, -1 = Clinical Phase "
        "unknown for drug or clinical candidate drug ie where ChEMBL cannot "
        "assign a clinical phase)."
    )
    mesh_id = Column(
        String(20), index=True, unique=True, nullable=False,
        doc="Medical Subject Headings (MeSH) disease identifier corresponding"
        " to the indication."
    )
    mesh_heading = Column(
        String(200), index=False, unique=False, nullable=False,
        doc="Medical Subject Heading term for the MeSH disease ID."
    )
    efo_id = Column(
        String(20), index=True, unique=True, nullable=True,
        doc="Experimental Factor Ontology (EFO) disease identifier "
        "corresponding to the indication."
    )
    efo_term = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Experimental Factor Ontology term for the EFO ID."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    compound_records = rel(
        "CompoundRecords", back_populates="drug_indication",
        doc="Relationship back to ``compound_records``"
    )
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="drug_indication",
        doc="Relationship back to ``molecule_dictionary``"
    )
    indication_refs = rel(
        "IndicationRefs", back_populates="drug_indication",
        doc="Relationship back to ``indication_refs``."
    )
    indication_index = rel(
        "TermIndexMap",
        back_populates="indication",
        primaryjoin='DrugIndication.drugind_id == TermIndexMap.term_id',
        foreign_keys='DrugIndication.drugind_id',
        doc="Relationship back to the ``term_index_mapping`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DrugMechanism(Base):
    """A representation of the ``drug_mechanism`` table.

    Parameters
    ----------
    mec_id : `int`
        Primary key for each drug mechanism of action.
    record_id : `int`
        Record_id for the drug (foreign key to compound_records table). Foreign
        key to ``compound_records.record_id``.
    molregno : `int`, optional, default: `NoneType`
        Molregno for the drug (foreign key to molecule_dictionary table).
        Foreign key to ``molecule_dictionary.molregno``.
    mechanism_of_action : `str`, optional, default: `NoneType`
        Description of the mechanism of action e.g., 'Phosphodiesterase 5
        inhibitor'.
    tid : `int`, optional, default: `NoneType`
        Target associated with this mechanism of action (foreign key to
        target_dictionary table). Foreign key to ``target_dictionary.tid``.
    site_id : `int`, optional, default: `NoneType`
        Binding site for the drug within the target (where known) - foreign key
        to binding_sites table. Foreign key to ``binding_sites.site_id``.
    action_type : `str`, optional, default: `NoneType`
        Type of action of the drug on the target e.g., agonist/antagonist etc
        (foreign key to action_type table). Foreign key to
        ``action_type.action_type``.
    direct_interaction : `bool`, optional, default: `NoneType`
        Flag to show whether the molecule is believed to interact directly with
        the target (1 = yes, 0 = no).
    molecular_mechanism : `bool`, optional, default: `NoneType`
        Flag to show whether the mechanism of action describes the molecular
        target of the drug, rather than a higher-level physiological mechanism
        e.g., vasodilator (1 = yes, 0 = no).
    disease_efficacy : `bool`, optional, default: `NoneType`
        Flag to show whether the target assigned is believed to play a role in
        the efficacy of the drug in the indication(s) for which it is approved
        (1 = yes, 0 = no).
    mechanism_comment : `str`, optional, default: `NoneType`
        Additional comments regarding the mechanism of action.
    selectivity_comment : `str`, optional, default: `NoneType`
        Additional comments regarding the selectivity of the drug.
    binding_site_comment : `str`, optional, default: `NoneType`
        Additional comments regarding the binding site of the drug.
    variant_id : `int`, optional, default: `NoneType`
        Foreign key to variant_sequences table. Indicates the mutant/variant
        version of the target used in the assay (where known/applicable).
        Foreign key to ``variant_sequences.variant_id``.
    compound_records : `chembl_orm.orm.CompoundRecords`, optional, default: \
    `NoneType`
        Relationship with ``compound_records``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.
    target_dictionary : `chembl_orm.orm.TargetDictionary`, optional, default: \
    `NoneType`
        Relationship with ``target_dictionary``.
    binding_sites : `chembl_orm.orm.BindingSites`, optional, default: \
    `NoneType`
        Relationship with ``binding_sites``.
    action_type_rel : `chembl_orm.orm.ActionType`, optional, default: \
    `NoneType`
        Relationship with ``action_type``.
    variant_sequences : `chembl_orm.orm.VariantSequences`, optional, default: \
    `NoneType`
        Relationship with ``variant_sequences``.
    mechanism_refs : `chembl_orm.orm.MechanismRefs`, optional, default: \
    `NoneType`
        Relationship with ``mechanism_refs``.

    Notes
    -----
    Table storing mechanism of action information for drugs, and clinical
    candidate drugs, from a variety of sources (e.g., ATC, FDA,
    ClinicalTrials.gov).
    """
    __tablename__ = "drug_mechanism"

    mec_id = Column(
        Integer, Sequence("mec_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key for each drug mechanism of action."
    )
    record_id = Column(
        Integer, ForeignKey("compound_records.record_id"), index=True,
        unique=False, nullable=False,
        doc="Record_id for the drug (foreign key to compound_records table)."
    )
    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=False, nullable=True,
        doc="Molregno for the drug (foreign key to molecule_dictionary table)."
    )
    mechanism_of_action = Column(
        String(250), index=False, unique=False, nullable=True,
        doc="Description of the mechanism of action e.g., 'Phosphodiesterase "
        "5 inhibitor'."
    )
    tid = Column(
        Integer, ForeignKey("target_dictionary.tid"), index=True,
        unique=False, nullable=True,
        doc="Target associated with this mechanism of action (foreign key to "
        "target_dictionary table)."
    )
    site_id = Column(
        Integer, ForeignKey("binding_sites.site_id"), index=True,
        unique=False, nullable=True,
        doc="Binding site for the drug within the target (where known) - "
        "foreign key to binding_sites table."
    )
    action_type = Column(
        String(50), ForeignKey("action_type.action_type"), index=True,
        unique=False, nullable=True,
        doc="Type of action of the drug on the target e.g., "
        "agonist/antagonist etc (foreign key to action_type table)."
    )
    direct_interaction = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Flag to show whether the molecule is believed to interact "
        "directly with the target (1 = yes, 0 = no)."
    )
    molecular_mechanism = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Flag to show whether the mechanism of action describes the "
        "molecular target of the drug, rather than a higher-level "
        "physiological mechanism e.g., vasodilator (1 = yes, 0 = no)."
    )
    disease_efficacy = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Flag to show whether the target assigned is believed to play a "
        "role in the efficacy of the drug in the indication(s) for which it is"
        " approved (1 = yes, 0 = no)."
    )
    mechanism_comment = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Additional comments regarding the mechanism of action."
    )
    selectivity_comment = Column(
        String(1000), index=False, unique=False, nullable=True,
        doc="Additional comments regarding the selectivity of the drug."
    )
    binding_site_comment = Column(
        String(1000), index=False, unique=False, nullable=True,
        doc="Additional comments regarding the binding site of the drug."
    )
    variant_id = Column(
        Integer, ForeignKey("variant_sequences.variant_id"), index=True,
        unique=False, nullable=True,
        doc="Foreign key to variant_sequences table. Indicates the "
        "mutant/variant version of the target used in the assay (where "
        "known/applicable)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    compound_records = rel(
        "CompoundRecords", back_populates="drug_mechanism",
        doc="Relationship back to ``compound_records``"
    )
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="drug_mechanism",
        doc="Relationship back to ``molecule_dictionary``"
    )
    target_dictionary = rel(
        "TargetDictionary", back_populates="drug_mechanism",
        doc="Relationship back to ``target_dictionary``"
    )
    binding_sites = rel(
        "BindingSites", back_populates="drug_mechanism",
        doc="Relationship back to ``binding_sites``"
    )
    action_type_rel = rel(
        "ActionType", back_populates="drug_mechanism",
        doc="Relationship back to ``action_type``"
    )
    variant_sequences = rel(
        "VariantSequences", back_populates="drug_mechanism",
        doc="Relationship back to ``variant_sequences``"
    )
    mechanism_refs = rel(
        "MechanismRefs", back_populates="drug_mechanism",
        doc="Relationship back to ``mechanism_refs``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DrugWarning(Base):
    """A representation of the ``drug_warning`` table.

    Parameters
    ----------
    warning_id : `int`
        Primary key for the drug warning.
    record_id : `int`, optional, default: `NoneType`
        Foreign key to the compound_records table. Foreign key to
        ``compound_records.record_id``.
    molregno : `int`, optional, default: `NoneType`
        Foreign key to molecule_dictionary table.
    warning_type : `str`, optional, default: `NoneType`
        Description of the drug warning type (e.g., withdrawn vs black box
        warning).
    warning_class : `str`, optional, default: `NoneType`
        High-level class of the drug warning.
    warning_description : `str`, optional, default: `NoneType`
        Description of the drug warning.
    warning_country : `str`, optional, default: `NoneType`
        List of countries/regions associated with the drug warning.
    warning_year : `int`, optional, default: `NoneType`
        Year the drug was first shown the warning.
    efo_term : `str`, optional, default: `NoneType`
        Term for Experimental Factor Ontology (EFO).
    efo_id : `str`, optional, default: `NoneType`
        Identifier for Experimental Factor Ontology (EFO).
    efo_id_for_warning_class : `str`, optional, default: `NoneType`
        Warning Class Identifier for Experimental Factor Ontology (EFO).
    compound_records : `chembl_orm.orm.CompoundRecords`, optional, default: \
    `NoneType`
        Relationship with ``compound_records``.
    warning_refs : `chembl_orm.orm.WarningRefs`, optional, default: \
    `NoneType`
        Relationship with ``warning_refs``.

    Notes
    -----
    Table storing safety-related information for drugs and clinical candidates.
    """
    __tablename__ = "drug_warning"

    warning_id = Column(
        Integer, Sequence("warning_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key for the drug warning."
    )
    record_id = Column(
        Integer, ForeignKey("compound_records.record_id"), index=True,
        unique=False, nullable=True,
        doc="Foreign key to the compound_records table."
    )
    molregno = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Foreign key to molecule_dictionary table."
    )
    warning_type = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="Description of the drug warning type (e.g., withdrawn vs black "
        "box warning)."
    )
    warning_class = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="High-level class of the drug warning."
    )
    warning_description = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="Description of the drug warning."
    )
    warning_country = Column(
        String(1000), index=False, unique=False, nullable=True,
        doc="List of countries/regions associated with the drug warning."
    )
    warning_year = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Year the drug was first shown the warning."
    )
    efo_term = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Term for Experimental Factor Ontology (EFO)."
    )
    efo_id = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="Identifier for Experimental Factor Ontology (EFO)."
    )
    efo_id_for_warning_class = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="Warning Class Identifier for Experimental Factor Ontology (EFO)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    compound_records = rel(
        "CompoundRecords", back_populates="drug_warning",
        doc="Relationship back to ``compound_records``"
    )
    warning_refs = rel(
        "WarningRefs", back_populates="drug_warning",
        doc="Relationship back to ``warning_refs``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Formulations(Base):
    """A representation of the ``formulations`` table.

    Parameters
    ----------
    product_id : `str`
        Unique identifier of the product. FK to PRODUCTS. This is indexed. This
        is unique. Foreign key to ``products.product_id``.
    ingredient : `str`, optional, default: `NoneType`
        Name of the approved ingredient within the product.
    strength : `str`, optional, default: `NoneType`
        Dose strength.
    record_id : `int`
        Foreign key to the compound_records table. This is indexed. This is
        unique. Foreign key to ``compound_records.record_id``.
    molregno : `int`, optional, default: `NoneType`
        Unique identifier of the ingredient FK to MOLECULE_DICTIONARY. Foreign
        key to ``molecule_dictionary.molregno``.
    formulation_id : `int`
        Primary key.
    products : `chembl_orm.orm.Products`, optional, default: `NoneType`
        Relationship with ``products``.
    compound_records : `chembl_orm.orm.CompoundRecords`, optional, default: \
    `NoneType`
        Relationship with ``compound_records``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.

    Notes
    -----
    Table linking individual ingredients in approved products (and their
    strengths) to entries in the molecule dictionary. Where products are
    mixtures of active ingredients they will have multiple entries in this
    table.
    """
    __tablename__ = "formulations"

    product_id = Column(
        String(30), ForeignKey("products.product_id"), index=True,
        unique=True, nullable=False,
        doc="Unique identifier of the product. FK to PRODUCTS."
    )
    ingredient = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Name of the approved ingredient within the product."
    )
    strength = Column(
        String(300), index=False, unique=False, nullable=True,
        doc="Dose strength."
    )
    record_id = Column(
        Integer, ForeignKey("compound_records.record_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to the compound_records table."
    )
    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=False, nullable=True,
        doc="Unique identifier of the ingredient FK to MOLECULE_DICTIONARY."
    )
    formulation_id = Column(
        Integer, Sequence("formulation_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    products = rel(
        "Products", back_populates="formulations",
        doc="Relationship back to ``products``"
    )
    compound_records = rel(
        "CompoundRecords", back_populates="formulations",
        doc="Relationship back to ``compound_records``"
    )
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="formulations",
        doc="Relationship back to ``molecule_dictionary``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Products(Base):
    """A representation of the ``products`` table.

    Parameters
    ----------
    dosage_form : `str`, optional, default: `NoneType`
        The dosage form of the product (e.g., tablet, capsule etc).
    route : `str`, optional, default: `NoneType`
        The administration route of the product (e.g., oral, injection etc).
    trade_name : `str`, optional, default: `NoneType`
        The trade name for the product.
    approval_date : `datetime.datetime`, optional, default: `NoneType`
        The FDA approval date for the product (not necessarily first approval
        of the active ingredient).
    ad_type : `str`, optional, default: `NoneType`
        RX = prescription, OTC = over the counter, DISCN = discontinued.
    oral : `bool`, optional, default: `NoneType`
        Flag to show whether product is orally delivered.
    topical : `bool`, optional, default: `NoneType`
        Flag to show whether product is topically delivered.
    parenteral : `bool`, optional, default: `NoneType`
        Flag to show whether product is parenterally delivered.
    black_box_warning : `bool`, optional, default: `NoneType`
        Flag to show whether the product label has a black box warning.
    applicant_full_name : `str`, optional, default: `NoneType`
        Name of the company applying for FDA approval.
    innovator_company : `bool`, optional, default: `NoneType`
        Flag to show whether the applicant is the innovator of the product.
    product_id : `str`
        FDA application number for the product.
    nda_type : `str`, optional, default: `NoneType`
        New Drug Application Type. The type of new drug application approval.
        New Drug Applications (NDA or innovator)  are ”N”.   Abbreviated New
        Drug Applications (ANDA or generic) are “A”.
    formulations : `chembl_orm.orm.Formulations`, optional, default: \
    `NoneType`
        Relationship with ``formulations``.
    product_patents : `chembl_orm.orm.ProductPatents`, optional, default: \
    `NoneType`
        Relationship with ``product_patents``.

    Notes
    -----
    Table containing information about approved drug products (mainly from the
    FDA Orange Book), such as trade name, administration route, approval date.
    Ingredients in each product are linked to the molecule dictionary via the
    formulations table.
    """
    __tablename__ = "products"

    dosage_form = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="The dosage form of the product (e.g., tablet, capsule etc)."
    )
    route = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="The administration route of the product (e.g., oral, injection "
        "etc)."
    )
    trade_name = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="The trade name for the product."
    )
    approval_date = Column(
        DateTime, index=False, unique=False, nullable=True,
        doc="The FDA approval date for the product (not necessarily first "
        "approval of the active ingredient)."
    )
    ad_type = Column(
        String(5), index=False, unique=False, nullable=True,
        doc="RX = prescription, OTC = over the counter, DISCN = discontinued."
    )
    oral = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Flag to show whether product is orally delivered."
    )
    topical = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Flag to show whether product is topically delivered."
    )
    parenteral = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Flag to show whether product is parenterally delivered."
    )
    black_box_warning = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Flag to show whether the product label has a black box warning."
    )
    applicant_full_name = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Name of the company applying for FDA approval."
    )
    innovator_company = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Flag to show whether the applicant is the innovator of the "
        "product."
    )
    product_id = Column(
        String(30), Sequence("product_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="FDA application number for the product."
    )
    nda_type = Column(
        String(10), index=False, unique=False, nullable=True,
        doc="New Drug Application Type. The type of new drug application "
        "approval.  New Drug Applications (NDA or innovator)  are ”N”.   "
        "Abbreviated New Drug Applications (ANDA or generic) are “A”."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    formulations = rel(
        "Formulations", back_populates="products",
        doc="Relationship back to ``formulations``."
    )
    product_patents = rel(
        "ProductPatents", back_populates="products",
        doc="Relationship back to ``product_patents``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FracClassification(Base):
    """A representation of the ``frac_classification`` table.

    Parameters
    ----------
    frac_class_id : `int`
        Unique numeric primary key for each level5 code.
    active_ingredient : `str`
        Name of active ingredient (fungicide) classified by FRAC.
    level1 : `str`
        Mechanism of action code assigned by FRAC.
    level1_description : `str`
        Description of mechanism of action.
    level2 : `str`
        Target site code assigned by FRAC.
    level2_description : `str`, optional, default: `NoneType`
        Description of target provided by FRAC.
    level3 : `str`
        Group number assigned by FRAC.
    level3_description : `str`, optional, default: `NoneType`
        Description of group provided by FRAC.
    level4 : `str`
        Number denoting the chemical group (number not assigned by FRAC).
    level4_description : `str`, optional, default: `NoneType`
        Chemical group name provided by FRAC.
    level5 : `str`
        A unique code assigned to each ingredient (based on the level 1-4 FRAC
        classification, but not assigned by IRAC). This is indexed. This is
        unique.
    frac_code : `str`
        The official FRAC classification code for the ingredient.
    molecule_frac_classification : \
    `chembl_orm.orm.MoleculeFracClassification`, optional, default: \
    `NoneType`
        Relationship with ``molecule_frac_classification``.

    Notes
    -----
    Table showing classification of fungicide mechanism of action according to
    the Fungicide Resistance Action Committee (FRAC): http://www.frac.info/publ
    ication/anhang/FRAC%20Code%20List%202013-final.pdf.
    """
    __tablename__ = "frac_classification"

    frac_class_id = Column(
        Integer, Sequence("frac_class_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Unique numeric primary key for each level5 code."
    )
    active_ingredient = Column(
        String(500), index=False, unique=False, nullable=False,
        doc="Name of active ingredient (fungicide) classified by FRAC."
    )
    level1 = Column(
        String(2), index=False, unique=False, nullable=False,
        doc="Mechanism of action code assigned by FRAC."
    )
    level1_description = Column(
        String(2000), index=False, unique=False, nullable=False,
        doc="Description of mechanism of action."
    )
    level2 = Column(
        String(2), index=False, unique=False, nullable=False,
        doc="Target site code assigned by FRAC."
    )
    level2_description = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Description of target provided by FRAC."
    )
    level3 = Column(
        String(6), index=False, unique=False, nullable=False,
        doc="Group number assigned by FRAC."
    )
    level3_description = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Description of group provided by FRAC."
    )
    level4 = Column(
        String(7), index=False, unique=False, nullable=False,
        doc="Number denoting the chemical group (number not assigned by FRAC)."
    )
    level4_description = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Chemical group name provided by FRAC."
    )
    level5 = Column(
        String(8), index=True, unique=True, nullable=False,
        doc="A unique code assigned to each ingredient (based on the level "
        "1-4 FRAC classification, but not assigned by IRAC)."
    )
    frac_code = Column(
        String(4), index=False, unique=False, nullable=False,
        doc="The official FRAC classification code for the ingredient."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    molecule_frac_classification = rel(
        "MoleculeFracClassification", back_populates="frac_classification",
        doc="Relationship back to ``molecule_frac_classification``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HracClassification(Base):
    """A representation of the ``hrac_classification`` table.

    Parameters
    ----------
    hrac_class_id : `int`
        Unique numeric primary key for each level3 code.
    active_ingredient : `str`
        Name of active ingredient (herbicide) classified by HRAC.
    level1 : `str`
        HRAC group code - denoting mechanism of action of herbicide.
    level1_description : `str`
        Description of mechanism of action provided by HRAC.
    level2 : `str`
        Indicates a chemical family within a particular HRAC group (number not
        assigned by HRAC).
    level2_description : `str`, optional, default: `NoneType`
        Description of chemical family provided by HRAC.
    level3 : `str`
        A unique code assigned to each ingredient (based on the level 1 and 2
        HRAC classification, but not assigned by HRAC). This is indexed. This
        is unique.
    hrac_code : `str`
        The official HRAC classification code for the ingredient.
    molecule_hrac_classification : \
    `chembl_orm.orm.MoleculeHracClassification`, optional, default: \
    `NoneType`
        Relationship with ``molecule_hrac_classification``.

    Notes
    -----
    Table showing classification of herbicide mechanism of action according to
    the Herbicide Resistance Action Committee (HRAC): http://www.hracglobal.com
    /Education/ClassificationofHerbicideSiteofAction.aspx.
    """
    __tablename__ = "hrac_classification"

    hrac_class_id = Column(
        Integer, Sequence("hrac_class_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Unique numeric primary key for each level3 code."
    )
    active_ingredient = Column(
        String(500), index=False, unique=False, nullable=False,
        doc="Name of active ingredient (herbicide) classified by HRAC."
    )
    level1 = Column(
        String(2), index=False, unique=False, nullable=False,
        doc="HRAC group code - denoting mechanism of action of herbicide."
    )
    level1_description = Column(
        String(2000), index=False, unique=False, nullable=False,
        doc="Description of mechanism of action provided by HRAC."
    )
    level2 = Column(
        String(3), index=False, unique=False, nullable=False,
        doc="Indicates a chemical family within a particular HRAC group "
        "(number not assigned by HRAC)."
    )
    level2_description = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Description of chemical family provided by HRAC."
    )
    level3 = Column(
        String(5), index=True, unique=True, nullable=False,
        doc="A unique code assigned to each ingredient (based on the level 1 "
        "and 2 HRAC classification, but not assigned by HRAC)."
    )
    hrac_code = Column(
        String(2), index=False, unique=False, nullable=False,
        doc="The official HRAC classification code for the ingredient."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    molecule_hrac_classification = rel(
        "MoleculeHracClassification", back_populates="hrac_classification",
        doc="Relationship back to ``molecule_hrac_classification``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class IndicationRefs(Base):
    """A representation of the ``indication_refs`` table.

    Parameters
    ----------
    indref_id : `int`
        Primary key.
    drugind_id : `int`
        Foreign key to the DRUG_INDICATION table, indicating the drug-
        indication link that this reference applies to. This is indexed. This
        is unique. Foreign key to ``drug_indication.drugind_id``.
    ref_type : `str`
        Type/source of reference. This is indexed. This is unique.
    ref_id : `str`
        Identifier for the reference in the source. This is indexed. This is
        unique.
    ref_url : `str`
        Full URL linking to the reference.
    drug_indication : `chembl_orm.orm.DrugIndication`, optional, default: \
    `NoneType`
        Relationship with ``drug_indication``.

    Notes
    -----
    Table storing references indicating the source of drug indication
    information.
    """
    __tablename__ = "indication_refs"

    indref_id = Column(
        Integer, Sequence("indref_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    drugind_id = Column(
        Integer, ForeignKey("drug_indication.drugind_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to the DRUG_INDICATION table, indicating the drug-"
        "indication link that this reference applies to."
    )
    ref_type = Column(
        String(50), index=True, unique=True, nullable=False,
        doc="Type/source of reference."
    )
    ref_id = Column(
        String(4000), index=True, unique=True, nullable=False,
        doc="Identifier for the reference in the source."
    )
    ref_url = Column(
        String(4000), index=False, unique=False, nullable=False,
        doc="Full URL linking to the reference."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    drug_indication = rel(
        "DrugIndication", back_populates="indication_refs",
        doc="Relationship back to ``drug_indication``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class IracClassification(Base):
    """A representation of the ``irac_classification`` table.

    Parameters
    ----------
    irac_class_id : `int`
        Unique numeric primary key for each level4 code.
    active_ingredient : `str`
        Name of active ingredient (insecticide) classified by IRAC.
    level1 : `str`
        Class of action e.g., nerve action, energy metabolism (code not
        assigned by IRAC).
    level1_description : `str`
        Description of class of action, as provided by IRAC.
    level2 : `str`
        IRAC main group code denoting primary site/mechanism of action.
    level2_description : `str`
        Description of site/mechanism of action provided by IRAC.
    level3 : `str`
        IRAC sub-group code denoting chemical class of insecticide.
    level3_description : `str`
        Description of chemical class or exemplifying ingredient provided by
        IRAC.
    level4 : `str`
        A unique code assigned to each ingredient (based on the level 1, 2 and
        3 IRAC classification, but not assigned by IRAC). This is indexed. This
        is unique.
    irac_code : `str`
        The official IRAC classification code for the ingredient.
    molecule_irac_classification : \
    `chembl_orm.orm.MoleculeIracClassification`, optional, default: \
    `NoneType`
        Relationship with ``molecule_irac_classification``.

    Notes
    -----
    Table showing classification of insecticide mechanism of action according
    to the Insecticide Resistance Action Committee (IRAC): http://www.irac-
    online.org/documents/moa-classification/?ext=pdf.
    """
    __tablename__ = "irac_classification"

    irac_class_id = Column(
        Integer, Sequence("irac_class_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Unique numeric primary key for each level4 code."
    )
    active_ingredient = Column(
        String(500), index=False, unique=False, nullable=False,
        doc="Name of active ingredient (insecticide) classified by IRAC."
    )
    level1 = Column(
        String(1), index=False, unique=False, nullable=False,
        doc="Class of action e.g., nerve action, energy metabolism (code not "
        "assigned by IRAC)."
    )
    level1_description = Column(
        String(2000), index=False, unique=False, nullable=False,
        doc="Description of class of action, as provided by IRAC."
    )
    level2 = Column(
        String(3), index=False, unique=False, nullable=False,
        doc="IRAC main group code denoting primary site/mechanism of action."
    )
    level2_description = Column(
        String(2000), index=False, unique=False, nullable=False,
        doc="Description of site/mechanism of action provided by IRAC."
    )
    level3 = Column(
        String(6), index=False, unique=False, nullable=False,
        doc="IRAC sub-group code denoting chemical class of insecticide."
    )
    level3_description = Column(
        String(2000), index=False, unique=False, nullable=False,
        doc="Description of chemical class or exemplifying ingredient "
        "provided by IRAC."
    )
    level4 = Column(
        String(8), index=True, unique=True, nullable=False,
        doc="A unique code assigned to each ingredient (based on the level 1,"
        " 2 and 3 IRAC classification, but not assigned by IRAC)."
    )
    irac_code = Column(
        String(3), index=False, unique=False, nullable=False,
        doc="The official IRAC classification code for the ingredient."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    molecule_irac_classification = rel(
        "MoleculeIracClassification", back_populates="irac_classification",
        doc="Relationship back to ``molecule_irac_classification``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class LigandEff(Base):
    """A representation of the ``ligand_eff`` table.

    Parameters
    ----------
    activity_id : `int`
        Link key to activities table. Foreign key to
        ``activities.activity_id``.
    bei : `float`, optional, default: `NoneType`
        Binding Efficiency Index = p(XC50) * 1000/MW_freebase.
    sei : `float`, optional, default: `NoneType`
        Surface Efficiency Index = p(XC50) * 100/PSA.
    le : `float`, optional, default: `NoneType`
        Ligand Efficiency = deltaG/heavy_atoms.
    lle : `float`, optional, default: `NoneType`
        Lipophilic Ligand Efficiency = -logKi - ALogP. [from Leeson NRDD 2007].
    activities : `chembl_orm.orm.Activities`, optional, default: `NoneType`
        Relationship with ``activities``.

    Notes
    -----
    Contains BEI (Binding Efficiency Index) and SEI (Surface Binding Efficiency
    Index) for each activity_id where such data can be calculated.
    """
    __tablename__ = "ligand_eff"

    activity_id = Column(
        Integer, ForeignKey("activities.activity_id"),
        Sequence("activity_id_seq"), index=True, unique=False, nullable=False,
        primary_key=True, doc="Link key to activities table."
    )
    bei = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Binding Efficiency Index = p(XC50) * 1000/MW_freebase."
    )
    sei = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Surface Efficiency Index = p(XC50) * 100/PSA."
    )
    le = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Ligand Efficiency = deltaG/heavy_atoms."
    )
    lle = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Lipophilic Ligand Efficiency = -logKi - ALogP. [from Leeson NRDD "
        "2007]."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activities = rel(
        "Activities", back_populates="ligand_eff",
        doc="Relationship back to ``activities``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MechanismRefs(Base):
    """A representation of the ``mechanism_refs`` table.

    Parameters
    ----------
    mecref_id : `int`
        Primary key.
    mec_id : `int`
        Foreign key to drug_mechanism table - indicating the mechanism to which
        the references refer. This is indexed. This is unique. Foreign key to
        ``drug_mechanism.mec_id``.
    ref_type : `str`
        Type/source of reference (e.g., 'PubMed','DailyMed'). This is indexed.
        This is unique.
    ref_id : `str`, optional, default: `NoneType`
        Identifier for the reference in the source (e.g., PubMed ID or DailyMed
        setid). This is indexed. This is unique.
    ref_url : `str`, optional, default: `NoneType`
        Full URL linking to the reference.
    drug_mechanism : `chembl_orm.orm.DrugMechanism`, optional, default: \
    `NoneType`
        Relationship with ``drug_mechanism``.

    Notes
    -----
    Table storing references for information in the drug_mechanism table.
    """
    __tablename__ = "mechanism_refs"

    mecref_id = Column(
        Integer, Sequence("mecref_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    mec_id = Column(
        Integer, ForeignKey("drug_mechanism.mec_id"), index=True, unique=True,
        nullable=False,
        doc="Foreign key to drug_mechanism table - indicating the mechanism "
        "to which the references refer."
    )
    ref_type = Column(
        String(50), index=True, unique=True, nullable=False,
        doc="Type/source of reference (e.g., 'PubMed','DailyMed')."
    )
    ref_id = Column(
        String(200), index=True, unique=True, nullable=True,
        doc="Identifier for the reference in the source (e.g., PubMed ID or "
        "DailyMed setid)."
    )
    ref_url = Column(
        String(400), index=False, unique=False, nullable=True,
        doc="Full URL linking to the reference."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    drug_mechanism = rel(
        "DrugMechanism", back_populates="mechanism_refs",
        doc="Relationship back to ``drug_mechanism``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Metabolism(Base):
    """A representation of the ``metabolism`` table.

    Parameters
    ----------
    met_id : `int`
        Primary key.
    drug_record_id : `int`, optional, default: `NoneType`
        Foreign key to compound_records. Record representing the drug or other
        compound for which metabolism is being studied (may not be the same as
        the substrate being measured). This is indexed. This is unique. Foreign
        key to ``compound_records.record_id``.
    substrate_record_id : `int`, optional, default: `NoneType`
        Foreign key to compound_records. Record representing the compound that
        is the subject of metabolism. This is indexed. This is unique. Foreign
        key to ``compound_records.record_id``.
    metabolite_record_id : `int`, optional, default: `NoneType`
        Foreign key to compound_records. Record representing the compound that
        is the result of metabolism. This is indexed. This is unique. Foreign
        key to ``compound_records.record_id``.
    pathway_id : `int`, optional, default: `NoneType`
        Identifier for the metabolic scheme/pathway (may be multiple pathways
        from one source document). This is indexed. This is unique.
    pathway_key : `str`, optional, default: `NoneType`
        Link to original source indicating where the pathway information was
        found (e.g., Figure 1, page 23).
    enzyme_name : `str`, optional, default: `NoneType`
        Name of the enzyme responsible for the metabolic conversion. This is
        indexed. This is unique.
    enzyme_tid : `int`, optional, default: `NoneType`
        Foreign key to target_dictionary. TID for the enzyme responsible for
        the metabolic conversion. This is indexed. This is unique. Foreign key
        to ``target_dictionary.tid``.
    met_conversion : `str`, optional, default: `NoneType`
        Description of the metabolic conversion.
    organism : `str`, optional, default: `NoneType`
        Organism in which this metabolic reaction occurs.
    tax_id : `int`, optional, default: `NoneType`
        NCBI Tax ID for the organism in which this metabolic reaction occurs.
        This is indexed. This is unique.
    met_comment : `str`, optional, default: `NoneType`
        Additional information regarding the metabolism (e.g., organ system,
        conditions under which observed, activity of metabolites).
    compound_records : `chembl_orm.orm.CompoundRecords`, optional, default: \
    `NoneType`
        Relationship with ``compound_records``.
    compound_records_2 : `chembl_orm.orm.CompoundRecords`, optional, default: \
    `NoneType`
        Relationship with ``compound_records``.
    compound_records_3 : `chembl_orm.orm.CompoundRecords`, optional, default: \
    `NoneType`
        Relationship with ``compound_records``.
    target_dictionary : `chembl_orm.orm.TargetDictionary`, optional, default: \
    `NoneType`
        Relationship with ``target_dictionary``.
    metabolism_refs : `chembl_orm.orm.MetabolismRefs`, optional, default: \
    `NoneType`
        Relationship with ``metabolism_refs``.

    Notes
    -----
    Table storing drug metabolic pathways, manually curated from a variety of
    sources.
    """
    __tablename__ = "metabolism"

    met_id = Column(
        Integer, Sequence("met_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    drug_record_id = Column(
        Integer, ForeignKey("compound_records.record_id"), index=True,
        unique=True, nullable=True,
        doc="Foreign key to compound_records. Record representing the drug or"
        " other compound for which metabolism is being studied (may not be the"
        " same as the substrate being measured)."
    )
    substrate_record_id = Column(
        Integer, ForeignKey("compound_records.record_id"), index=True,
        unique=True, nullable=True,
        doc="Foreign key to compound_records. Record representing the "
        "compound that is the subject of metabolism."
    )
    metabolite_record_id = Column(
        Integer, ForeignKey("compound_records.record_id"), index=True,
        unique=True, nullable=True,
        doc="Foreign key to compound_records. Record representing the "
        "compound that is the result of metabolism."
    )
    pathway_id = Column(
        Integer, index=True, unique=True, nullable=True,
        doc="Identifier for the metabolic scheme/pathway (may be multiple "
        "pathways from one source document)."
    )
    pathway_key = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Link to original source indicating where the pathway information"
        " was found (e.g., Figure 1, page 23)."
    )
    enzyme_name = Column(
        String(200), index=True, unique=True, nullable=True,
        doc="Name of the enzyme responsible for the metabolic conversion."
    )
    enzyme_tid = Column(
        Integer, ForeignKey("target_dictionary.tid"), index=True, unique=True,
        nullable=True,
        doc="Foreign key to target_dictionary. TID for the enzyme responsible"
        " for the metabolic conversion."
    )
    met_conversion = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Description of the metabolic conversion."
    )
    organism = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Organism in which this metabolic reaction occurs."
    )
    tax_id = Column(
        Integer, index=True, unique=True, nullable=True,
        doc="NCBI Tax ID for the organism in which this metabolic reaction "
        "occurs."
    )
    met_comment = Column(
        String(1000), index=False, unique=False, nullable=True,
        doc="Additional information regarding the metabolism (e.g., organ "
        "system, conditions under which observed, activity of metabolites)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # TODO: FLAG for checking
    compound_records = rel(
        "CompoundRecords", back_populates="metabolism",
        doc="Relationship back to ``compound_records``",
        primaryjoin="Metabolism.drug_record_id == CompoundRecords.record_id"
    )
    # TODO: FLAG for checking
    compound_records_2 = rel(
        "CompoundRecords", back_populates="metabolism_2",
        doc="Relationship back to ``compound_records``",
        primaryjoin="Metabolism.substrate_record_id == "
        "CompoundRecords.record_id"
    )
    # TODO: FLAG for checking
    compound_records_3 = rel(
        "CompoundRecords", back_populates="metabolism_3",
        doc="Relationship back to ``compound_records``",
        primaryjoin="Metabolism.metabolite_record_id == "
        "CompoundRecords.record_id"
    )
    target_dictionary = rel(
        "TargetDictionary", back_populates="metabolism",
        doc="Relationship back to ``target_dictionary``"
    )
    metabolism_refs = rel(
        "MetabolismRefs", back_populates="metabolism",
        doc="Relationship back to ``metabolism_refs``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MetabolismRefs(Base):
    """A representation of the ``metabolism_refs`` table.

    Parameters
    ----------
    metref_id : `int`
        Primary key.
    met_id : `int`
        Foreign key to record_metabolism table - indicating the metabolism
        information to which the references refer. This is indexed. This is
        unique. Foreign key to ``metabolism.met_id``.
    ref_type : `str`
        Type/source of reference (e.g., 'PubMed','DailyMed'). This is indexed.
        This is unique.
    ref_id : `str`, optional, default: `NoneType`
        Identifier for the reference in the source (e.g., PubMed ID or DailyMed
        setid). This is indexed. This is unique.
    ref_url : `str`, optional, default: `NoneType`
        Full URL linking to the reference.
    metabolism : `chembl_orm.orm.Metabolism`, optional, default: `NoneType`
        Relationship with ``metabolism``.

    Notes
    -----
    Table storing references for metabolic pathways, indicating the source of
    the data.
    """
    __tablename__ = "metabolism_refs"

    metref_id = Column(
        Integer, Sequence("metref_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    met_id = Column(
        Integer, ForeignKey("metabolism.met_id"), index=True, unique=True,
        nullable=False,
        doc="Foreign key to record_metabolism table - indicating the "
        "metabolism information to which the references refer."
    )
    ref_type = Column(
        String(50), index=True, unique=True, nullable=False,
        doc="Type/source of reference (e.g., 'PubMed','DailyMed')."
    )
    ref_id = Column(
        String(200), index=True, unique=True, nullable=True,
        doc="Identifier for the reference in the source (e.g., PubMed ID or "
        "DailyMed setid)."
    )
    ref_url = Column(
        String(400), index=False, unique=False, nullable=True,
        doc="Full URL linking to the reference."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    metabolism = rel(
        "Metabolism", back_populates="metabolism_refs",
        doc="Relationship back to ``metabolism``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MoleculeAtcClassification(Base):
    """A representation of the ``molecule_atc_classification`` table.

    Parameters
    ----------
    mol_atc_id : `int`
        Primary key.
    level5 : `str`
        ATC code (foreign key to atc_classification table). Foreign key to
        ``atc_classification.level5``.
    molregno : `int`
        Drug to which the ATC code applies (foreign key to molecule_dictionary
        table). Foreign key to ``molecule_dictionary.molregno``.
    atc_classification : `chembl_orm.orm.AtcClassification`, optional, \
    default: `NoneType`
        Relationship with ``atc_classification``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.

    Notes
    -----
    Table mapping drugs in the molecule_dictionary to ATC codes in the
    atc_classification table.
    """
    __tablename__ = "molecule_atc_classification"

    mol_atc_id = Column(
        Integer, Sequence("mol_atc_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    level5 = Column(
        String(10), ForeignKey("atc_classification.level5"), index=True,
        unique=False, nullable=False,
        doc="ATC code (foreign key to atc_classification table)."
    )
    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=False, nullable=False,
        doc="Drug to which the ATC code applies (foreign key to "
        "molecule_dictionary table)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    atc_classification = rel(
        "AtcClassification", back_populates="molecule_atc_classification",
        doc="Relationship back to ``atc_classification``"
    )
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="molecule_atc_classification",
        doc="Relationship back to ``molecule_dictionary``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MoleculeFracClassification(Base):
    """A representation of the ``molecule_frac_classification`` table.

    Parameters
    ----------
    mol_frac_id : `int`
        Primary key.
    frac_class_id : `int`
        Foreign key to frac_classification table showing the mechanism of
        action classification of the compound. This is indexed. This is unique.
        Foreign key to ``frac_classification.frac_class_id``.
    molregno : `int`
        Foreign key to molecule_dictionary, showing the compound to which the
        classification applies. This is indexed. This is unique. Foreign key to
        ``molecule_dictionary.molregno``.
    frac_classification : `chembl_orm.orm.FracClassification`, optional, \
    default: `NoneType`
        Relationship with ``frac_classification``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.

    Notes
    -----
    Table showing Fungicide Resistance Action Committee (FRAC) mechanism of
    action classification for known crop protection fungicides.
    """
    __tablename__ = "molecule_frac_classification"

    mol_frac_id = Column(
        Integer, Sequence("mol_frac_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    frac_class_id = Column(
        Integer, ForeignKey("frac_classification.frac_class_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to frac_classification table showing the mechanism "
        "of action classification of the compound."
    )
    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to molecule_dictionary, showing the compound to "
        "which the classification applies."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    frac_classification = rel(
        "FracClassification", back_populates="molecule_frac_classification",
        doc="Relationship back to ``frac_classification``"
    )
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="molecule_frac_classification",
        doc="Relationship back to ``molecule_dictionary``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MoleculeHierarchy(Base):
    """A representation of the ``molecule_hierarchy`` table.

    Parameters
    ----------
    molregno : `int`
        Foreign key to compounds table. This field holds a list of all of the
        ChEMBL compounds with associated data (e.g., activity information,
        approved drugs). Parent compounds that are generated only by removing
        salts, and which do not themselves have any associated data will not
        appear here. Foreign key to ``molecule_dictionary.molregno``.
    parent_molregno : `int`, optional, default: `NoneType`
        Represents parent compound of molregno in first field (i.e., generated
        by removing salts). Where molregno and parent_molregno are same, the
        initial ChEMBL compound did not contain a salt component, or else could
        not be further processed for various reasons (e.g., inorganic mixture).
        Compounds which are only generated by removing salts will appear in
        this field only. Those which, themselves, have any associated data
        (e.g., activity data) or are launched drugs will also appear in the
        molregno field. Foreign key to ``molecule_dictionary.molregno``.
    active_molregno : `int`, optional, default: `NoneType`
        Where a compound is a pro-drug, this represents the active metabolite
        of the 'dosed' compound given by parent_molregno. Where parent_molregno
        and active_molregno are the same, the compound is not currently known
        to be a pro-drug. Foreign key to ``molecule_dictionary.molregno``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.
    molecule_dictionary_2 : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.
    molecule_dictionary_3 : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.

    Notes
    -----
    Table storing relationships between parents, salts and active metabolites
    (for pro-drugs).
    """
    __tablename__ = "molecule_hierarchy"

    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"),
        Sequence("molregno_seq"), index=True, unique=False, nullable=False,
        primary_key=True,
        doc="Foreign key to compounds table. This field holds a list of all "
        "of the ChEMBL compounds with associated data (e.g., activity "
        "information, approved drugs). Parent compounds that are generated "
        "only by removing salts, and which do not themselves have any "
        "associated data will not appear here."
    )
    parent_molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=False, nullable=True,
        doc="Represents parent compound of molregno in first field (i.e., "
        "generated by removing salts). Where molregno and parent_molregno are "
        "same, the initial ChEMBL compound did not contain a salt component, "
        "or else could not be further processed for various reasons (e.g., "
        "inorganic mixture). Compounds which are only generated by removing "
        "salts will appear in this field only. Those which, themselves, have "
        "any associated data (e.g., activity data) or are launched drugs will "
        "also appear in the molregno field."
    )
    active_molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=False, nullable=True,
        doc="Where a compound is a pro-drug, this represents the active "
        "metabolite of the 'dosed' compound given by parent_molregno. Where "
        "parent_molregno and active_molregno are the same, the compound is not"
        " currently known to be a pro-drug."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # TODO: FLAG for checking
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="molecule_hierarchy",
        doc="Relationship back to ``molecule_dictionary``",
        primaryjoin="MoleculeHierarchy.molregno == MoleculeDictionary.molregno"
    )
    # TODO: FLAG for checking
    molecule_dictionary_2 = rel(
        "MoleculeDictionary", back_populates="molecule_hierarchy_2",
        doc="Relationship back to ``molecule_dictionary``",
        primaryjoin="MoleculeHierarchy.parent_molregno == "
        "MoleculeDictionary.molregno"
    )
    # TODO: FLAG for checking
    molecule_dictionary_3 = rel(
        "MoleculeDictionary", back_populates="molecule_hierarchy_3",
        doc="Relationship back to ``molecule_dictionary``",
        primaryjoin="MoleculeHierarchy.active_molregno == "
        "MoleculeDictionary.molregno"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MoleculeHracClassification(Base):
    """A representation of the ``molecule_hrac_classification`` table.

    Parameters
    ----------
    mol_hrac_id : `int`
        Primary key.
    hrac_class_id : `int`
        Foreign key to hrac_classification table showing the classification for
        the compound. This is indexed. This is unique. Foreign key to
        ``hrac_classification.hrac_class_id``.
    molregno : `int`
        Foreign key to molecule_dictionary, showing the compound to which this
        classification applies. This is indexed. This is unique. Foreign key to
        ``molecule_dictionary.molregno``.
    hrac_classification : `chembl_orm.orm.HracClassification`, optional, \
    default: `NoneType`
        Relationship with ``hrac_classification``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.

    Notes
    -----
    Table showing Herbicide Resistance Action Committee (HRAC) mechanism of
    action classification for known herbicidal compounds.
    """
    __tablename__ = "molecule_hrac_classification"

    mol_hrac_id = Column(
        Integer, Sequence("mol_hrac_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    hrac_class_id = Column(
        Integer, ForeignKey("hrac_classification.hrac_class_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to hrac_classification table showing the "
        "classification for the compound."
    )
    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to molecule_dictionary, showing the compound to "
        "which this classification applies."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    hrac_classification = rel(
        "HracClassification", back_populates="molecule_hrac_classification",
        doc="Relationship back to ``hrac_classification``"
    )
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="molecule_hrac_classification",
        doc="Relationship back to ``molecule_dictionary``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MoleculeIracClassification(Base):
    """A representation of the ``molecule_irac_classification`` table.

    Parameters
    ----------
    mol_irac_id : `int`
        Primary key.
    irac_class_id : `int`
        Foreign key to the irac_classification table showing the mechanism of
        action classification for the compound. This is indexed. This is
        unique. Foreign key to ``irac_classification.irac_class_id``.
    molregno : `int`
        Foreign key to the molecule_dictionary table, showing the compound to
        which the classification applies. This is indexed. This is unique.
        Foreign key to ``molecule_dictionary.molregno``.
    irac_classification : `chembl_orm.orm.IracClassification`, optional, \
    default: `NoneType`
        Relationship with ``irac_classification``.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.

    Notes
    -----
    Table showing Insecticide Resistance Action Committee (IRAC) mechanism of
    action classification for known crop protection insecticides.
    """
    __tablename__ = "molecule_irac_classification"

    mol_irac_id = Column(
        Integer, Sequence("mol_irac_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    irac_class_id = Column(
        Integer, ForeignKey("irac_classification.irac_class_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to the irac_classification table showing the "
        "mechanism of action classification for the compound."
    )
    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to the molecule_dictionary table, showing the "
        "compound to which the classification applies."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    irac_classification = rel(
        "IracClassification", back_populates="molecule_irac_classification",
        doc="Relationship back to ``irac_classification``"
    )
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="molecule_irac_classification",
        doc="Relationship back to ``molecule_dictionary``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MoleculeSynonyms(Base):
    """A representation of the ``molecule_synonyms`` table.

    Parameters
    ----------
    molregno : `int`
        Foreign key to molecule_dictionary. This is indexed. This is unique.
        Foreign key to ``molecule_dictionary.molregno``.
    syn_type : `str`
        Type of name/synonym (e.g., TRADE_NAME, RESEARCH_CODE, USAN). This is
        indexed. This is unique.
    molsyn_id : `int`
        Primary key.
    res_stem_id : `int`, optional, default: `NoneType`
        Foreign key to the research_stem table. Where a synonym is a research
        code, this links to further information about the company associated
        with that code. Foreign key to ``research_stem.res_stem_id``.
    synonyms : `str`, optional, default: `NoneType`
        Synonym for the compound. This is indexed. This is unique.
    molecule_dictionary : `chembl_orm.orm.MoleculeDictionary`, optional, \
    default: `NoneType`
        Relationship with ``molecule_dictionary``.
    research_stem : `chembl_orm.orm.ResearchStem`, optional, default: \
    `NoneType`
        Relationship with ``research_stem``.

    Notes
    -----
    Stores synonyms for a compound (e.g., common names, trade names, research
    codes etc).
    """
    __tablename__ = "molecule_synonyms"

    molregno = Column(
        Integer, ForeignKey("molecule_dictionary.molregno"), index=True,
        unique=True, nullable=False, doc="Foreign key to molecule_dictionary."
    )
    syn_type = Column(
        String(50), index=True, unique=True, nullable=False,
        doc="Type of name/synonym (e.g., TRADE_NAME, RESEARCH_CODE, USAN)."
    )
    molsyn_id = Column(
        Integer, Sequence("molsyn_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    res_stem_id = Column(
        Integer, ForeignKey("research_stem.res_stem_id"), index=True,
        unique=False, nullable=True,
        doc="Foreign key to the research_stem table. Where a synonym is a "
        "research code, this links to further information about the company "
        "associated with that code."
    )
    synonyms = Column(
        String(200), index=True, unique=True, nullable=True,
        doc="Synonym for the compound."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    molecule_dictionary = rel(
        "MoleculeDictionary", back_populates="molecule_synonyms",
        doc="Relationship back to ``molecule_dictionary``"
    )
    research_stem = rel(
        "ResearchStem", back_populates="molecule_synonyms",
        doc="Relationship back to ``research_stem``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ResearchStem(Base):
    """A representation of the ``research_stem`` table.

    Parameters
    ----------
    res_stem_id : `int`
        Primary key. Unique ID for each research code stem.
    research_stem : `str`, optional, default: `NoneType`
        The actual stem/prefix used in the research code. This is indexed. This
        is unique.
    molecule_synonyms : `chembl_orm.orm.MoleculeSynonyms`, optional, default: \
    `NoneType`
        Relationship with ``molecule_synonyms``.
    research_companies : `chembl_orm.orm.ResearchCompanies`, optional, \
    default: `NoneType`
        Relationship with ``research_companies``.

    Notes
    -----
    Table storing a list of stems/prefixes used in research codes.
    """
    __tablename__ = "research_stem"

    res_stem_id = Column(
        Integer, Sequence("res_stem_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key. Unique ID for each research code stem."
    )
    research_stem = Column(
        String(20), index=True, unique=True, nullable=True,
        doc="The actual stem/prefix used in the research code."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    molecule_synonyms = rel(
        "MoleculeSynonyms", back_populates="research_stem",
        doc="Relationship back to ``molecule_synonyms``."
    )
    research_companies = rel(
        "ResearchCompanies", back_populates="research_stem",
        doc="Relationship back to ``research_companies``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class OrganismClass(Base):
    """A representation of the ``organism_class`` table.

    Parameters
    ----------
    oc_id : `int`
        Internal primary key.
    tax_id : `int`, optional, default: `NoneType`
        NCBI taxonomy ID for the organism (corresponding to tax_ids in
        target_dictionary table). This is indexed. This is unique.
    l1 : `str`, optional, default: `NoneType`
        Highest level classification (e.g., Eukaryotes, Bacteria, Fungi etc).
    l2 : `str`, optional, default: `NoneType`
        Second level classification.
    l3 : `str`, optional, default: `NoneType`
        Third level classification.

    Notes
    -----
    Simple organism classification (essentially a cut-down version of the NCBI
    taxonomy for organisms in ChEMBL target_dictionary table), allowing
    browsing of ChEMBL data by taxonomic groups.
    """
    __tablename__ = "organism_class"

    oc_id = Column(
        Integer, Sequence("oc_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Internal primary key."
    )
    tax_id = Column(
        Integer, index=True, unique=True, nullable=True,
        doc="NCBI taxonomy ID for the organism (corresponding to tax_ids in "
        "target_dictionary table)."
    )
    l1 = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Highest level classification (e.g., Eukaryotes, Bacteria, Fungi "
        "etc)."
    )
    l2 = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Second level classification."
    )
    l3 = Column(
        String(200), index=False, unique=False, nullable=True,
        doc="Third level classification."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PatentUseCodes(Base):
    """A representation of the ``patent_use_codes`` table.

    Parameters
    ----------
    patent_use_code : `str`
        Primary key. Patent use code from FDA Orange Book.
    definition : `str`
        Definition for the patent use code, from FDA Orange Book.
    product_patents : `chembl_orm.orm.ProductPatents`, optional, default: \
    `NoneType`
        Relationship with ``product_patents``.

    Notes
    -----
    Table from FDA Orange Book, showing definitions of different patent use
    codes (as used in the product_patents table).
    """
    __tablename__ = "patent_use_codes"

    patent_use_code = Column(
        String(8), Sequence("patent_use_code_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key. Patent use code from FDA Orange Book."
    )
    definition = Column(
        String(500), index=False, unique=False, nullable=False,
        doc="Definition for the patent use code, from FDA Orange Book."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    product_patents = rel(
        "ProductPatents", back_populates="patent_use_codes",
        doc="Relationship back to ``product_patents``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PredictedBindingDomains(Base):
    """A representation of the ``predicted_binding_domains`` table.

    Parameters
    ----------
    predbind_id : `int`
        Primary key.
    activity_id : `int`, optional, default: `NoneType`
        Foreign key to the activities table, indicating the
        compound/assay(+target) combination for which this prediction is made.
        Foreign key to ``activities.activity_id``.
    site_id : `int`, optional, default: `NoneType`
        Foreign key to the binding_sites table, indicating the binding site
        (domain) that the compound is predicted to bind to. Foreign key to
        ``binding_sites.site_id``.
    prediction_method : `str`, optional, default: `NoneType`
        The method used to assign the binding domain (e.g., 'Single domain'
        where the protein has only 1 domain, 'Multi domain' where the protein
        has multiple domains, but only 1 is known to bind small molecules in
        other proteins).
    confidence : `str`, optional, default: `NoneType`
        The level of confidence assigned to the prediction (high where the
        protein has only 1 domain, medium where the compound has multiple
        domains, but only 1 known small molecule-binding domain).
    activities : `chembl_orm.orm.Activities`, optional, default: `NoneType`
        Relationship with ``activities``.
    binding_sites : `chembl_orm.orm.BindingSites`, optional, default: \
    `NoneType`
        Relationship with ``binding_sites``.

    Notes
    -----
    Table storing information on the likely binding domain of compounds in the
    activities table (based on analysis of the domain structure of the target.
    Note these are predictions, not experimentally determined. See Kruger F,
    Rostom R and Overington JP (2012), BMC Bioinformatics, 13(S17), S11 for
    more details.
    """
    __tablename__ = "predicted_binding_domains"

    predbind_id = Column(
        Integer, Sequence("predbind_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    activity_id = Column(
        Integer, ForeignKey("activities.activity_id"), index=True,
        unique=False, nullable=True,
        doc="Foreign key to the activities table, indicating the "
        "compound/assay(+target) combination for which this prediction is "
        "made."
    )
    site_id = Column(
        Integer, ForeignKey("binding_sites.site_id"), index=True,
        unique=False, nullable=True,
        doc="Foreign key to the binding_sites table, indicating the binding "
        "site (domain) that the compound is predicted to bind to."
    )
    prediction_method = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="The method used to assign the binding domain (e.g., 'Single "
        "domain' where the protein has only 1 domain, 'Multi domain' where the"
        " protein has multiple domains, but only 1 is known to bind small "
        "molecules in other proteins)."
    )
    confidence = Column(
        String(10), index=False, unique=False, nullable=True,
        doc="The level of confidence assigned to the prediction (high where "
        "the protein has only 1 domain, medium where the compound has multiple"
        " domains, but only 1 known small molecule-binding domain)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    activities = rel(
        "Activities", back_populates="predicted_binding_domains",
        doc="Relationship back to ``activities``"
    )
    binding_sites = rel(
        "BindingSites", back_populates="predicted_binding_domains",
        doc="Relationship back to ``binding_sites``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ProductPatents(Base):
    """A representation of the ``product_patents`` table.

    Parameters
    ----------
    prod_pat_id : `int`
        Primary key.
    product_id : `str`
        Foreign key to products table - FDA application number for the product.
        This is indexed. This is unique. Foreign key to
        ``products.product_id``.
    patent_no : `str`
        Patent numbers as submitted by the applicant holder for patents covered
        by the statutory provisions. This is indexed. This is unique.
    patent_expire_date : `datetime.datetime`
        Date the patent expires as submitted by the applicant holder including
        applicable extensions. This is indexed. This is unique.
    drug_substance_flag : `bool`
        Patents submitted on FDA Form 3542 and listed after August 18, 2003 may
        have a drug substance flag set to 1, indicating the sponsor submitted
        the patent as claiming the drug substance.
    drug_product_flag : `bool`
        Patents submitted on FDA Form 3542 and listed after August 18, 2003 may
        have a drug product flag set to 1, indicating the sponsor submitted the
        patent as claiming the drug product.
    patent_use_code : `str`, optional, default: `NoneType`
        Code to designate a use patent that covers the approved indication or
        use of a drug product. This is indexed. This is unique. Foreign key to
        ``patent_use_codes.patent_use_code``.
    delist_flag : `bool`
        Sponsor has requested patent be delisted if set to 1.  This patent has
        remained listed because, under Section 505(j)(5)(D)(i) of the Act, a
        first applicant may retain eligibility for 180-day exclusivity based on
        a paragraph IV certification to this patent for a certain period.
        Applicants under Section 505(b)(2) are not required to certify to
        patents where this flag is set to 1.
    submission_date : `datetime.datetime`, optional, default: `NoneType`
        The date on which the FDA receives patent information from the new drug
        application (NDA) holder. Format is Mmm d, yyyy.
    products : `chembl_orm.orm.Products`, optional, default: `NoneType`
        Relationship with ``products``.
    patent_use_codes : `chembl_orm.orm.PatentUseCodes`, optional, default: \
    `NoneType`
        Relationship with ``patent_use_codes``.

    Notes
    -----
    Table from FDA Orange Book, showing patents associated with drug products.
    """
    __tablename__ = "product_patents"

    prod_pat_id = Column(
        Integer, Sequence("prod_pat_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    product_id = Column(
        String(30), ForeignKey("products.product_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to products table - FDA application number for the "
        "product."
    )
    patent_no = Column(
        String(20), index=True, unique=True, nullable=False,
        doc="Patent numbers as submitted by the applicant holder for patents "
        "covered by the statutory provisions."
    )
    patent_expire_date = Column(
        DateTime, index=True, unique=True, nullable=False,
        doc="Date the patent expires as submitted by the applicant holder "
        "including applicable extensions."
    )
    drug_substance_flag = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Patents submitted on FDA Form 3542 and listed after August 18, "
        "2003 may have a drug substance flag set to 1, indicating the sponsor "
        "submitted the patent as claiming the drug substance."
    )
    drug_product_flag = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Patents submitted on FDA Form 3542 and listed after August 18, "
        "2003 may have a drug product flag set to 1, indicating the sponsor "
        "submitted the patent as claiming the drug product."
    )
    patent_use_code = Column(
        String(10), ForeignKey("patent_use_codes.patent_use_code"),
        index=True, unique=True, nullable=True,
        doc="Code to designate a use patent that covers the approved "
        "indication or use of a drug product."
    )
    delist_flag = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Sponsor has requested patent be delisted if set to 1.  This "
        "patent has remained listed because, under Section 505(j)(5)(D)(i) of "
        "the Act, a first applicant may retain eligibility for 180-day "
        "exclusivity based on a paragraph IV certification to this patent for "
        "a certain period.  Applicants under Section 505(b)(2) are not "
        "required to certify to patents where this flag is set to 1."
    )
    submission_date = Column(
        DateTime, index=False, unique=False, nullable=True,
        doc="The date on which the FDA receives patent information from the "
        "new drug application (NDA) holder. Format is Mmm d, yyyy."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    products = rel(
        "Products", back_populates="product_patents",
        doc="Relationship back to ``products``"
    )
    patent_use_codes = rel(
        "PatentUseCodes", back_populates="product_patents",
        doc="Relationship back to ``patent_use_codes``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ProteinClassSynonyms(Base):
    """A representation of the ``protein_class_synonyms`` table.

    Parameters
    ----------
    protclasssyn_id : `int`
        Primary key.
    protein_class_id : `int`
        Foreign key to the PROTEIN_CLASSIFICATION table. The protein_class to
        which this synonym applies. This is indexed. This is unique. Foreign
        key to ``protein_classification.protein_class_id``.
    protein_class_synonym : `str`, optional, default: `NoneType`
        The synonym for the protein class. This is indexed. This is unique.
    syn_type : `str`, optional, default: `NoneType`
        The type or origin of the synonym (e.g., ChEMBL, Concept Wiki, UMLS).
        This is indexed. This is unique.
    protein_classification : `chembl_orm.orm.ProteinClassification`, \
    optional, default: `NoneType`
        Relationship with ``protein_classification``.

    Notes
    -----
    Table storing synonyms for the protein family classifications (from various
    sources including MeSH, ConceptWiki and UMLS).
    """
    __tablename__ = "protein_class_synonyms"

    protclasssyn_id = Column(
        Integer, Sequence("protclasssyn_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    protein_class_id = Column(
        Integer, ForeignKey("protein_classification.protein_class_id"),
        index=True, unique=True, nullable=False,
        doc="Foreign key to the PROTEIN_CLASSIFICATION table. The "
        "protein_class to which this synonym applies."
    )
    protein_class_synonym = Column(
        String(1000), index=True, unique=True, nullable=True,
        doc="The synonym for the protein class."
    )
    syn_type = Column(
        String(20), index=True, unique=True, nullable=True,
        doc="The type or origin of the synonym (e.g., ChEMBL, Concept Wiki, "
        "UMLS)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    protein_classification = rel(
        "ProteinClassification", back_populates="protein_class_synonyms",
        doc="Relationship back to ``protein_classification``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ResearchCompanies(Base):
    """A representation of the ``research_companies`` table.

    Parameters
    ----------
    co_stem_id : `int`
        Primary key.
    res_stem_id : `int`, optional, default: `NoneType`
        Foreign key to research_stem table. This is indexed. This is unique.
        Foreign key to ``research_stem.res_stem_id``.
    company : `str`, optional, default: `NoneType`
        Name of current company associated with this research code stem. This
        is indexed. This is unique.
    country : `str`, optional, default: `NoneType`
        Country in which the company uses this research code stem.
    previous_company : `str`, optional, default: `NoneType`
        Previous name of the company associated with this research code stem
        (e.g., if the company has undergone acquisitions/mergers).
    research_stem : `chembl_orm.orm.ResearchStem`, optional, default: \
    `NoneType`
        Relationship with ``research_stem``.

    Notes
    -----
    Table storing a list of pharmaceutical companies (including current and
    former names) corresponding to each research code stem in the research_stem
    table. A stem can sometimes be used by more than one company.
    """
    __tablename__ = "research_companies"

    co_stem_id = Column(
        Integer, Sequence("co_stem_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    res_stem_id = Column(
        Integer, ForeignKey("research_stem.res_stem_id"), index=True,
        unique=True, nullable=True, doc="Foreign key to research_stem table."
    )
    company = Column(
        String(100), index=True, unique=True, nullable=True,
        doc="Name of current company associated with this research code stem."
    )
    country = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Country in which the company uses this research code stem."
    )
    previous_company = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Previous name of the company associated with this research code "
        "stem (e.g., if the company has undergone acquisitions/mergers)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    research_stem = rel(
        "ResearchStem", back_populates="research_companies",
        doc="Relationship back to ``research_stem``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SiteComponents(Base):
    """A representation of the ``site_components`` table.

    Parameters
    ----------
    sitecomp_id : `int`
        Primary key.
    site_id : `int`
        Foreign key to binding_sites table. This is indexed. This is unique.
        Foreign key to ``binding_sites.site_id``.
    component_id : `int`, optional, default: `NoneType`
        Foreign key to the component_sequences table, indicating which
        molecular component of the target is involved in the binding site. This
        is indexed. This is unique. Foreign key to
        ``component_sequences.component_id``.
    domain_id : `int`, optional, default: `NoneType`
        Foreign key to the domains table, indicating which domain of the given
        molecular component is involved in the binding site (where not known,
        the domain_id may be null). This is indexed. This is unique. Foreign
        key to ``domains.domain_id``.
    site_residues : `str`, optional, default: `NoneType`
        List of residues from the given molecular component that make up the
        binding site (where not know, will be null).
    binding_sites : `chembl_orm.orm.BindingSites`, optional, default: \
    `NoneType`
        Relationship with ``binding_sites``.
    component_sequences : `chembl_orm.orm.ComponentSequences`, optional, \
    default: `NoneType`
        Relationship with ``component_sequences``.
    domains : `chembl_orm.orm.Domains`, optional, default: `NoneType`
        Relationship with ``domains``.

    Notes
    -----
    Table defining the location of the binding sites in the binding_sites
    table. A binding site could be defined in terms of which protein subunits
    (components) are involved, the domains within those subunits to which the
    compound binds, and possibly even the precise residues involved. For a
    target where the binding site is at the interface of two protein subunits
    or two domains, there will be two site_components describing each of these
    subunits/domains.
    """
    __tablename__ = "site_components"

    sitecomp_id = Column(
        Integer, Sequence("sitecomp_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    site_id = Column(
        Integer, ForeignKey("binding_sites.site_id"), index=True, unique=True,
        nullable=False, doc="Foreign key to binding_sites table."
    )
    component_id = Column(
        Integer, ForeignKey("component_sequences.component_id"), index=True,
        unique=True, nullable=True,
        doc="Foreign key to the component_sequences table, indicating which "
        "molecular component of the target is involved in the binding site."
    )
    domain_id = Column(
        Integer, ForeignKey("domains.domain_id"), index=True, unique=True,
        nullable=True,
        doc="Foreign key to the domains table, indicating which domain of the"
        " given molecular component is involved in the binding site (where not"
        " known, the domain_id may be null)."
    )
    site_residues = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="List of residues from the given molecular component that make up"
        " the binding site (where not know, will be null)."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    binding_sites = rel(
        "BindingSites", back_populates="site_components",
        doc="Relationship back to ``binding_sites``"
    )
    component_sequences = rel(
        "ComponentSequences", back_populates="site_components",
        doc="Relationship back to ``component_sequences``"
    )
    domains = rel(
        "Domains", back_populates="site_components",
        doc="Relationship back to ``domains``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TargetComponents(Base):
    """A representation of the ``target_components`` table.

    Parameters
    ----------
    tid : `int`
        Foreign key to the target_dictionary, indicating the target to which
        the components belong. This is indexed. This is unique. Foreign key to
        ``target_dictionary.tid``.
    component_id : `int`
        Foreign key to the component_sequences table, indicating which
        components belong to the target. This is indexed. This is unique.
        Foreign key to ``component_sequences.component_id``.
    targcomp_id : `int`
        Primary key.
    homologue : `int`
        Indicates that the given component is a homologue of the correct
        component (e.g., from a different species) when set to 1. This may be
        the case if the sequence for the correct protein/nucleic acid cannot be
        found in sequence databases. A value of 2 indicates that the sequence
        given is a representative of a species group, e.g., an E. coli protein
        to represent the target of a broad-spectrum antibiotic.
    target_dictionary : `chembl_orm.orm.TargetDictionary`, optional, default: \
    `NoneType`
        Relationship with ``target_dictionary``.
    component_sequences : `chembl_orm.orm.ComponentSequences`, optional, \
    default: `NoneType`
        Relationship with ``component_sequences``.

    Notes
    -----
    Links molecular target from the target_dictionary to the components they
    consist of (in the component_sequences table). For a protein complex or
    protein family target, for example, there will be multiple protein
    components in the component_sequences table.
    """
    __tablename__ = "target_components"

    tid = Column(
        Integer, ForeignKey("target_dictionary.tid"), index=True, unique=True,
        nullable=False,
        doc="Foreign key to the target_dictionary, indicating the target to "
        "which the components belong."
    )
    component_id = Column(
        Integer, ForeignKey("component_sequences.component_id"), index=True,
        unique=True, nullable=False,
        doc="Foreign key to the component_sequences table, indicating which "
        "components belong to the target."
    )
    targcomp_id = Column(
        Integer, Sequence("targcomp_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )
    homologue = Column(
        Integer, index=False, unique=False, nullable=False,
        doc="Indicates that the given component is a homologue of the correct"
        " component (e.g., from a different species) when set to 1. This may "
        "be the case if the sequence for the correct protein/nucleic acid "
        "cannot be found in sequence databases. A value of 2 indicates that "
        "the sequence given is a representative of a species group, e.g., an "
        "E. coli protein to represent the target of a broad-spectrum "
        "antibiotic."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    target_dictionary = rel(
        "TargetDictionary", back_populates="target_components",
        doc="Relationship back to ``target_dictionary``"
    )
    component_sequences = rel(
        "ComponentSequences", back_populates="target_components",
        doc="Relationship back to ``component_sequences``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TargetRelations(Base):
    """A representation of the ``target_relations`` table.

    Parameters
    ----------
    tid : `int`
        Identifier for target of interest (foreign key to target_dictionary
        table). Foreign key to ``target_dictionary.tid``.
    relationship : `str`
        Relationship between two targets (e.g., SUBSET OF, SUPERSET OF,
        OVERLAPS WITH).
    related_tid : `int`
        Identifier for the target that is related to the target of interest
        (foreign key to target_dicitionary table). Foreign key to
        ``target_dictionary.tid``.
    targrel_id : `int`
        Primary key.
    target_dictionary : `chembl_orm.orm.TargetDictionary`, optional, default: \
    `NoneType`
        Relationship with ``target_dictionary``.
    target_dictionary_2 : `chembl_orm.orm.TargetDictionary`, optional, \
    default: `NoneType`
        Relationship with ``target_dictionary``.

    Notes
    -----
    Table showing relationships between different protein targets based on
    overlapping protein components (e.g., relationship between a protein
    complex and the individual subunits).
    """
    __tablename__ = "target_relations"

    tid = Column(
        Integer, ForeignKey("target_dictionary.tid"), index=True,
        unique=False, nullable=False,
        doc="Identifier for target of interest (foreign key to "
        "target_dictionary table)."
    )
    relationship = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="Relationship between two targets (e.g., SUBSET OF, SUPERSET OF, "
        "OVERLAPS WITH)."
    )
    related_tid = Column(
        Integer, ForeignKey("target_dictionary.tid"), index=True,
        unique=False, nullable=False,
        doc="Identifier for the target that is related to the target of "
        "interest (foreign key to target_dicitionary table)."
    )
    targrel_id = Column(
        Integer, Sequence("targrel_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Primary key."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # TODO: FLAG for checking
    target_dictionary = rel(
        "TargetDictionary", back_populates="target_relations",
        doc="Relationship back to ``target_dictionary``",
        primaryjoin="TargetRelations.tid == TargetDictionary.tid"
    )
    # TODO: FLAG for checking
    target_dictionary_2 = rel(
        "TargetDictionary", back_populates="target_relations_2",
        doc="Relationship back to ``target_dictionary``",
        primaryjoin="TargetRelations.related_tid == TargetDictionary.tid"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UsanStems(Base):
    """A representation of the ``usan_stems`` table.

    Parameters
    ----------
    usan_stem_id : `int`
        Numeric primary key.
    stem : `str`
        Stem defined for use in United States Adopted Names. This is indexed.
        This is unique.
    subgroup : `str`, optional, default: `NoneType`
        More specific subgroup of the stem defined for use in United States
        Adopted Names. This is indexed. This is unique.
    annotation : `str`, optional, default: `NoneType`
        Meaning of the stem (e.g., the class of compound it applies to).
    stem_class : `str`, optional, default: `NoneType`
        Indicates whether stem is used as a prefix/infix/suffix/combined prefix
        and suffix.
    major_class : `str`, optional, default: `NoneType`
        Protein family targeted by compounds of this class (e.g., GPCR/Ion
        channel/Protease) where known/applicable.

    Notes
    -----
    Table storing definitions for stems used in USANs (United States Adopted
    Names).
    """
    __tablename__ = "usan_stems"

    usan_stem_id = Column(
        Integer, Sequence("usan_stem_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Numeric primary key."
    )
    stem = Column(
        String(100), index=True, unique=True, nullable=False,
        doc="Stem defined for use in United States Adopted Names."
    )
    subgroup = Column(
        String(100), index=True, unique=True, nullable=True,
        doc="More specific subgroup of the stem defined for use in United "
        "States Adopted Names."
    )
    annotation = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Meaning of the stem (e.g., the class of compound it applies to)."
    )
    stem_class = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Indicates whether stem is used as a prefix/infix/suffix/combined"
        " prefix and suffix."
    )
    major_class = Column(
        String(100), index=False, unique=False, nullable=True,
        doc="Protein family targeted by compounds of this class (e.g., "
        "GPCR/Ion channel/Protease) where known/applicable."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Version(Base):
    """A representation of the ``version`` table.

    Parameters
    ----------
    name : `str`
        Name of release version.
    creation_date : `datetime.datetime`, optional, default: `NoneType`
        Date database created.
    comments : `str`, optional, default: `NoneType`
        Description of release version.

    Notes
    -----
    Table showing release version and creation date for the database.
    """
    __tablename__ = "version"

    name = Column(
        String(20), Sequence("name_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="Name of release version."
    )
    creation_date = Column(
        DateTime, index=False, unique=False, nullable=True,
        doc="Date database created."
    )
    comments = Column(
        String(2000), index=False, unique=False, nullable=True,
        doc="Description of release version."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class WarningRefs(Base):
    """A representation of the ``warning_refs`` table.

    Parameters
    ----------
    warnref_id : `int`
        Primary key for the warning reference.
    warning_id : `int`, optional, default: `NoneType`
        Foreign key to the drug_warning table. Foreign key to
        ``drug_warning.warning_id``.
    ref_type : `str`, optional, default: `NoneType`
        Type/source of reference.
    ref_id : `str`, optional, default: `NoneType`
        Identifier for the reference in the source.
    ref_url : `str`, optional, default: `NoneType`
        Full URL linking to the reference.
    drug_warning : `chembl_orm.orm.DrugWarning`, optional, default: \
    `NoneType`
        Relationship with ``drug_warning``.

    Notes
    -----
    Table storing references indicating the source of drug warning information.
    """
    __tablename__ = "warning_refs"

    warnref_id = Column(
        Integer, Sequence("warnref_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Primary key for the warning reference."
    )
    warning_id = Column(
        Integer, ForeignKey("drug_warning.warning_id"), index=True,
        unique=False, nullable=True,
        doc="Foreign key to the drug_warning table."
    )
    ref_type = Column(
        String(50), index=False, unique=False, nullable=True,
        doc="Type/source of reference."
    )
    ref_id = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="Identifier for the reference in the source."
    )
    ref_url = Column(
        String(4000), index=False, unique=False, nullable=True,
        doc="Full URL linking to the reference."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    drug_warning = rel(
        "DrugWarning", back_populates="warning_refs",
        doc="Relationship back to ``drug_warning``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TermIndexLookup(Base, orm_mixin.TermIndexLookupMixin):
    """A representation of the ``term_index_lookup`` table.

    Parameters
    ----------
    token_id : `int`
        Auto-incremented primary key.
    token_str : `str`
        The actual lookup string of the token.
    token_count `int`
        The total count of the token in the dataset.
    token_prob : `int`
        The prbability that the token is encountered in the dataset.
    token_index_map : `TermIndexMap`
        The relationship back to the ``token_index_map`` table.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TermIndexMap(Base, orm_mixin.TermIndexMapMixinInt):
    """A representation of the ``term_index_mapping`` table.

    Parameters
    ----------
    mapping_id : `int`
        Auto-incremented primary key.
    token_id : `int`
        A mapping back to a specific token.
    start_pos : `int`
        The start position of the word represented by the token in the term
        string.
    end_pos : `int`
        The end position of the word represented by the token in the term
        string.
    rank_order : `int`
        The rank order position of the word represented by the token in the
        term string.
    term_id : `int`
        The ID for the term identifier. This matches to the ``drugind_id`` of
        the ``drug_indication`` table.
    token_index_lookup : `TermIndexLookup`
        The relationship back to the ``token_index_lookup`` table.
    indication : `chembl_orm.orm.DrugIndication`
        The relationship back to the ``drug_indication`` table.
    """
    indication = rel(
        "DrugIndication",
        back_populates="indication_index",
        primaryjoin='DrugIndication.drugind_id == TermIndexMap.term_id',
        foreign_keys='DrugIndication.drugind_id',
        doc="Relationship back to the ``drug_indication`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)
