"""Some general queries for common data from ChEMBL
"""
from sqlalchemy.orm import aliased
from sqlalchemy import or_, and_
from sqlalchemy.orm.exc import NoResultFound
from chembl_orm import orm, INT_VERSION, examples
from umls_tools.admin import index
import re
import warnings
# import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ChemblQuery(object):
    """A ChEMBL query object that provides a general interface to higher level
    queries of ChEMBL.

    Parameters
    ----------
    sessionmaker : `sqlalchemy.Sessionmaker`
        An SQLAlchemy sessionmaker
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, sessionmaker):
        self.sessionmaker = sessionmaker
        self.session = None
        self.db_version = None
        self.salts = []
        self.index_search = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Initialise the session and check the version.
        """
        self.salts = examples.get_data("salts")
        self.session = self.sessionmaker()
        self.db_version = self.get_version()
        if self.valid_orm() is False:
            warnings.warn(
                "The ORM and the database are different versions: "
                f"{INT_VERSION} vs. {self.db_version}"
            )
        nterms = self.session.query(orm.DrugIndication.molregno).count()

        # There are EFO and MeSH terms but I am not multiplying by two as they
        # are highly similar
        self.index_search = index.BaseSearchIndex(self.session, orm, nterms)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the session and check the version.
        """
        self.session.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def valid_orm(self):
        """Check if the ChEMBL ORM version is valid for the ChEMBL database
        being queried.

        Returns
        -------
        is_valid : `bool`
            `True` if it is valid `False` if not.

        Notes
        -----
        Validity is based on the version number of the database matching the
        version number of the ChEMBL ORM.
        """
        return INT_VERSION == self.db_version

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_version(self):
        """Get the ChEMBL version number from the database.

        Returns
        -------
        chembl_version : `int`
            The ChEMBL version number.
        """
        try:
            return int(
                re.sub(
                    "ChEMBL_",
                    "",
                    self.session.query(orm.Version.name).one()[0]
                )
            )
        except NoResultFound as e:
            raise ValueError("Unable to get ChEMBL version number") from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_drugs_for_indication(self, indication):
        """Get the drugs that map to a supplied indication.

        Parameters
        ----------
        indication : `str`
            The drug indication to search for.

        Returns
        -------
        matching_drugs : `list` of `list`
            The elements are ordered from highest score to lowest and each
            sub-list  (matching term) has the following elements:

            0. The match score source_coverage * target_coverage *
               sum(log10(total index terms/token freq)).
            1. The length of the match.
            2. The length of the whole matching term
            3. A set of tuples of the containing the matching positions of
               individual tokens.
            4. The token mapping rows in the database (SQLAlchemy objects)

        Notes
        -----
        The indication argument is processed in the same way the indications
        that built the index table are processed. The best matches are
        returned.

        This requires the index tables to be built.

        See also
        --------
        chembl_orm.index.build_chembl_index
        """
        return self.index_search.search_term(indication)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def map_drug_name(self, drug_name, exhaustive=True):
        """Attempt to map a drug name to a ChEMBL compound.

        Parameters
        ----------
        drug_name : `str`
            The drug name to map into a ChEMBL compound ID.
        exhaustive : `bool`, optional, default: `True`
            Perform an exhaustive search (see notes).

        Returns
        -------
        mappings : `list` or `tuple`
            The tuple will contain the following fields:

            1. The Drug synonym name that matched the supplied drug name.
            2. The synonym type (i.e. USAN or INN or research code).
            3. The compound preferred name in the ``molecule_dictionary``
               table.
            4. The compound ChEMBL ID.
            5. The molecule type, i.e. small molecule.
            6. The drug indication class.

        exhaustive : `bool`
            An indicator if the exhaustive search strategy was used to yield
            the match. Even if the exhaustive parameter was set that does not
            necessarily mean that it had to be used to get the match into
            ChEMBL.

        Notes
        -----
        If a match to the drug name is not found and an exhaustive search is
        employed, then salt names will be stripped from the drug name and
        searched against ChEMBL.
        """
        query = self.session.query(
            orm.MoleculeSynonyms.synonyms,
            orm.MoleculeSynonyms.syn_type,
            orm.MoleculeDictionary.pref_name.label("compound_pref_name"),
            orm.MoleculeDictionary.chembl_id.label("compound_chembl_id"),
            orm.MoleculeDictionary.molecule_type,
            orm.MoleculeDictionary.indication_class
        ).select_from(
            orm.MoleculeSynonyms
        ).join(
            orm.MoleculeDictionary,
            orm.MoleculeSynonyms.molregno == orm.MoleculeDictionary.molregno
        ).filter(
            or_(
                orm.MoleculeSynonyms.synonyms.like(drug_name),
                orm.MoleculeDictionary.pref_name.like(drug_name)
            )
        )

        # Do the query
        mappings = [i for i in query]

        if len(mappings) > 0:
            return mappings, False

        mappings = []
        if exhaustive is False:
            return mappings, False

        exhaustive = False
        for i in self.salts:
            new_drug_name = i.pattern.sub('', drug_name).strip()

            if new_drug_name != drug_name:
                mappings, exhaustive = self.map_drug_name(
                    new_drug_name, exhaustive=False
                )
                if len(mappings) > 0:
                    exhaustive = True
                    break
        return mappings, exhaustive

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def find_drug_targets(self, chembl_id):
        """Attempt to get a drug target for a compound chembl ID.

        Parameters
        ----------
        chembl_id :`str`
            The chembl ID to get the efficacy targets for

        Returns
        -------
        efficacy_targets : `list`
            The efficacy targets for the compund ChEMBL ID or any of it's
            active parent molecules.

        Notes
        -----
        The difference between this and
        ``chembl_orm.queries.ChemblQuery.get_drug_targets`` is that if no drug
        target is found for the ChEMBL ID then we check for any drug targets
        for the parent molecule or the active molecule, this uses the chembl
        molecule_hierarchy table.
        """

        # First attempt to find drug targets for the chembl_id
        drug_targets = self.get_drug_targets(chembl_id)

        # if we have found some then we return them
        if len(drug_targets) > 0:
            drug_targets = [(i, chembl_id, 'direct') for i in drug_targets]
            return drug_targets

        # If we get here then no drug targets have been found so we have a look
        # if any parent molecules have any drug targets
        parents = self.get_parent_molecules(chembl_id)

        for p in parents:
            drug_targets.extend(
                [
                    (i, chembl_id, 'parent') for i in self.get_drug_targets(
                        p.chembl_id
                    )
                ]
            )

        if len(drug_targets) > 0:
            return drug_targets

        # If we get here then no drug targets have been found with the parent
        # compounds so we have a look if any active molecules have any drug
        # targets
        active = self.get_active_molecules(chembl_id)

        for a in active:
            drug_targets.extend(
                [
                    (i, chembl_id, 'active') for i in self.get_drug_targets(
                        a.chembl_id
                    )
                ]
            )

        return drug_targets

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_drug_targets(self, chembl_id):
        """Get all the efficacy drug target components for a chembl ID.

        Parameters
        ----------
        chembl_id : `str`
            The compund chembl ID to get the efficacy targets for.

        Returns
        -------
        efficacy_targets : `tuple`
            Matching efficacy targets.
        """

        query = self.session.query(
            orm.MoleculeDictionary.pref_name.label("compound_pref_name"),
            orm.MoleculeDictionary.chembl_id.label("compound_chembl_id"),
            orm.MoleculeDictionary.molecule_type,
            orm.MoleculeDictionary.indication_class,
            orm.DrugMechanism.mechanism_of_action,
            orm.TargetDictionary.chembl_id.label("target_chembl_id"),
            orm.TargetDictionary.target_type,
            orm.TargetDictionary.pref_name.label("target_pref_name"),
            orm.TargetDictionary.organism,
            orm.DrugMechanism.action_type,
            orm.ComponentSequences.accession.label("target_accession"),
            orm.ComponentSequences.description.label("target_desc"),
        ).select_from(
            orm.MoleculeDictionary
        ).join(
            orm.DrugMechanism,
            orm.DrugMechanism.molregno == orm.MoleculeDictionary.molregno
        ).join(
            orm.TargetDictionary,
            orm.TargetDictionary.tid == orm.DrugMechanism.tid
        ).join(
            orm.TargetComponents,
            orm.TargetComponents.tid == orm.DrugMechanism.tid
        ).join(
            orm.ComponentSequences,
            orm.ComponentSequences.component_id == \
            orm.TargetComponents.component_id
        ).filter(
            orm.MoleculeDictionary.chembl_id == chembl_id
        ).group_by(
            orm.MoleculeDictionary.chembl_id,
            orm.TargetDictionary.chembl_id,
            orm.ComponentSequences.accession
        )

        return query.all()  # [i for i in query]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_parent_molecules(self, chembl_id):
        """Find parent molecules for a compound chembl ID.

        Parameters
        ----------
        chembl_id : `str`
            The chembl ID to get the efficacy targets for

        Returns
        -------
        parent : `list` of `chembl_orm.orm.MoleculeDictionary`
            Any parent molecules.

        Notes
        -----
        This queries the ``molecule_hierarchy`` table for active molecules
        matching the ``chembl_id`` and returns the parent.
        """
        md_comp, md_par = (
            aliased(orm.MoleculeDictionary),
            aliased(orm.MoleculeDictionary)
        )

        query = self.session.query(md_par).select_from(
            md_comp
        ).join(
            orm.MoleculeHierarchy,
            orm.MoleculeHierarchy.active_molregno == md_comp.molregno
        ).join(
            md_par,
            md_par.molregno == orm.MoleculeHierarchy.parent_molregno
        ).filter(
            and_(
                md_comp.chembl_id == chembl_id,
                orm.MoleculeHierarchy.parent_molregno !=
                orm.MoleculeHierarchy.active_molregno
            )
        )
        return query.all()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_active_molecules(self, chembl_id):
        """Find active molecules for a chembl ID.

        Parameters
        ----------
        chembl_id : `str`
            The chembl ID to get the efficacy targets for

        Returns
        -------
        active : `list` of `chembl_orm.orm.MoleculeDictionary`
            Any active molecules.

        Notes
        -----
        This queries the ``molecule_hierarchy`` table for parent molecules
        matching the chembl_id and returns the active molecule.
        """
        md_comp, md_par = (
            aliased(orm.MoleculeDictionary),
            aliased(orm.MoleculeDictionary)
        )

        query = self.session.query(md_par).select_from(
            md_comp
        ).join(
            orm.MoleculeHierarchy,
            orm.MoleculeHierarchy.parent_molregno == md_comp.molregno
        ).join(
            md_par,
            md_par.molregno == orm.MoleculeHierarchy.active_molregno
        ).filter(
            and_(
                md_comp.chembl_id == chembl_id,
                orm.MoleculeHierarchy.parent_molregno !=
                orm.MoleculeHierarchy.active_molregno
            )
        )
        return query.all()
