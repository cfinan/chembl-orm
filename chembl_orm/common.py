"""Some common variables and functions
"""
from chembl_orm import __version__
import re
import os


DEFAULT_PREFIX = "db."
"""The SQLAlchemy database ChEMBL command prefixes in the config file (`str`)
"""
DEFAULT_CONFIG = os.path.join(os.environ['HOME'], '.db.cnf')
"""The default database config file location (`str`)
"""
ORM_VERSION = int(re.sub(r'\..*$', '', __version__))
"""The ORM major version number expressed as an integer (`int`)
"""
