# Import examples into the main package
from .example_data import (
    examples
)
import re
__version__ = '34.0.0a0'
INT_VERSION = int(re.sub(r'\..*$', '', __version__))
