===============
Database schema
===============

Below is a diagram of the currently implemented schema taken from the `ChEMBL downloads <https://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/latest/>`_ page. The table and column descriptions below and in the ORM API documentation are taken from the schema documentation file also available from the download page.

.. image:: ./_static/chembl-schema.png
   :width: 500
   :alt: ChEMBL schema
   :align: center

.. include:: ./data_dict/chembl-schema-tables.rst
