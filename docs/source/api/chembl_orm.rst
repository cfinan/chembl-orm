``chembl_orm`` package
======================

``chembl_orm.common`` module
----------------------------

.. automodule:: chembl_orm.common
   :members:
   :undoc-members:
   :show-inheritance:

``chembl_orm.parse_schema`` module
----------------------------------

.. autofunction:: chembl_orm.parse_schema.parse_chembl_schema

``chembl_orm.index`` module
---------------------------

.. autofunction:: chembl_orm.index.build_chembl_index

``chembl_orm.queries`` module
-----------------------------

.. autoclass:: chembl_orm.queries.ChemblQuery
   :members:

..
   ORM API documentation references are dynamically produced at documentation
   build time, this includes a module title
.. include:: ../data_dict/chembl-db-orm.rst
