==============
Python Scripts
==============

.. _chembl_index:

``chembl-index``
----------------

.. argparse::
   :module: chembl_orm.index
   :func: _init_cmd_args
   :prog: chembl-index

Example usage
~~~~~~~~~~~~~

To index a ChEMBL data that is under the section heading ``chembl_latest`` in the database config file and give progress updates.

.. code-block::

    chembl-index -vv chembl_latest

.. _parse_schema:

``chembl-parse-schema``
-----------------------

.. argparse::
   :module: chembl_orm.parse_schema
   :func: _init_cmd_args
   :prog: chembl-parse-schema

Example usage
~~~~~~~~~~~~~

To parse the schema documentation file and write the column info and table info files to their use the default locations.

.. code-block::

   chembl-parse-schema schema_documentation.txt

To write to  custom column info and table info locations.

.. code-block::

   chembl-parse-schema --out-table-info ~/chembl_latest_table.txt --out-column-info ~/chembl_latest_column.txt schema_documentation.txt


Output files
~~~~~~~~~~~~

The output columns for the table info and column info files are appropriate for use with the ``orm-create-src`` script within `sqlalchemy-config <https://gitlab.com/cfinan/sqlalchemy-config>`_.

Table info file
...............

.. include:: ../data_dict/table-info.rst

Column info file
................

.. include:: ../data_dict/column-info.rst

Known Issues
~~~~~~~~~~~~

None reported.
