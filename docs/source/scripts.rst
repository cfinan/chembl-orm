======================
Command-line endpoints
======================

.. _cmd_line:

Below is a list of all the command line endpoints installed with the chembl_orm. Many of these are mentioned in their respective context throughout the documentation but they are listed here all in a single place.

.. toctree::
   :maxdepth: 2
   :caption: Python scripts:

   scripts/python_scripts
